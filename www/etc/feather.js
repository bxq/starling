var pImgFeed;
var lRunning;
var lPrevious;
var iCountDown;

var sWndLoc = window.location.pathname;
var sBaseDir = sWndLoc.substring( 0, sWndLoc.lastIndexOf('/'));
localStorage.apihost=sBaseDir+"/index.php";

function genClock() {
	return("<div id=now>"+nowAsTxt()+"</div>");
}

function genPayload(bScrub) {
	var pData = {};
	var pReq = {};
	var pTst = {};
	var pFld = document.getElementById("tstdoc");
	if ( pFld.value != '') {
		pTst['doc']=pFld.value;
		pFld = document.getElementById("tstname");
		pTst['name']=pFld.value;
		pFld = document.getElementById("tstvuc");
		pTst['vuc']=pFld.value;
		pFld = document.getElementById("tstdtn");
		pTst['dtn']=pFld.value;
	}
	pFld = document.getElementById("tstgit");
	pTst['git']=pFld.value;
	pFld = document.getElementById("tstgbn");
	pTst['gbn']=pFld.value;
//	pFld = document.getElementById("tstgrd");
//	pTst['grd']=pFld.value;
	pFld = document.getElementById("tstlgs");
	if ( pFld.value != '') pTst['lgs']=pFld.value;
	pFld = document.getElementById("tstfrm");
	if ( pFld.checked) pTst['frm']="selenium";
	pFld = document.getElementById("tstmsid");
	pReq['starling_uid']=pFld.value;
	pFld = document.getElementById("tstpass");
	pReq['starling_pwd'] = bScrub ? pFld.value.replace(/./g,'*') : pFld.value;
	pReq['tst']=pTst;
	pData['req']=pReq;
//	var sJSON = JSON.stringify(pData,null,2);
//	return(sJSON);
	return(pData);
}

function validate() {
	var sErr="data entry is incomplete";
	var bOK=true;
//	...
	var pFld = document.getElementById("tstdoc");
	if (pFld.value != "") { // bOK=false;
		pFld = document.getElementById("tstname");
		if (pFld.value == "") bOK=false;
		pFld = document.getElementById("tstvuc");
		if (Number(pFld.value) < 1) bOK=false;
	}
	var pFld = document.getElementById("tstmsid");
	if (pFld.value == "") bOK=false;
	pFld = document.getElementById("tstpass");
	if (pFld.value == "") bOK=false;
	pFld = document.getElementById("tstgit");
	if (pFld.value == "") bOK=false;
	pFld = document.getElementById("tstgbn");
	if (pFld.value == "") bOK=false;
//	pFld = document.getElementById("tstgrd");
//	tstgrd not required, so no check
	pFld = document.getElementById("tstlgs");
	if ( pFld.value != '') {
		if (Number(pFld.value) < 1) bOK=false;
	}
//	...
	if (bOK) sErr=null;
	return(sErr);
}

function ping() {
	var sRet = null;
	var pRsp = opReq('ping',0,null,null);
	if ('msg' in pRsp) {
		if ( pRsp['msg'] != 'pong') {
			if (false) console.log('DBG=>>> ping failure: '+pRsp['msg']);
		} else sRet = pRsp['msg'];
	} // else console.log('DBG=>>> ping failure');
	return(sRet);
}

function auth(sMSID,sPassword) {
	var sRet = null;
	var pReq = {'starling_uid':sMSID,'starling_pwd':sPassword};
	var pRsp = opReq('auth',0,pReq,null);
	if ('msg' in pRsp) {
		var sMsg = pRsp['msg'];
		if ( sMsg != 'unauthorized for API') {
			sRet = sMsg;
		} else console.log('ERR=>>> auth failure: '+sMsg);
	} else console.log('ERR=>>> auth failure: no response');
	return(sRet);
}

function launchConsole(btn) {
	return;
}

async function launchTest(btn,url) {
//	await sleep(100);
	var sInvalid = validate();
	if (sInvalid == null) {
		var pCon = document.getElementById("txtcon");
		var pData = genPayload(false);
	//	console.log('DBG=>>> go: validation OK');
		var sTkn=null;
		var bOK=false;
		if ( ping() != 'pong') {
			sTkn = auth(pData['req']['starling_uid'],pData['req']['starling_pwd']);
			if ( sTkn != null) bOK=true;
		} else bOK=true;
		if (bOK) {
			pCon.readOnly = false;
		//	pCon.value += sTkn;
			pCon.scrollTop = pCon.scrollHeight;
			pCon.readOnly = true;
			pReq = pData['req']['tst'];
			try {
				var pRsp = opReq('liftoff',0,pReq,null);
			//	var sRsp = JSON.stringify(pRsp,null,2);
				var sRsp = "no message in response";
				if ('msg' in pRsp) {
					var sTst = ('id' in pRsp) ? pRsp['id'].toString() : '?';
					sRsp = '\n'+pRsp['msg'];
					if ( sTst != '?') {
						var pDbg = {'req':{'cmd':'liftoff'}};
						Object.entries(pReq).forEach(([key,value]) => { pDbg['req'][key] = value })
						sRsp += '\nLiftOff request payload:\n'+JSON.stringify(pDbg,null,2)+'\n';
						sRsp += '\nTest ID: '+sTst+'\nConsole: '+url+'?tst='+sTst;
						sRsp += '\nUpon successful test completion, these URLs will become available:';
						if ('rpt' in pRsp) sRsp += '\nReport: '+pRsp['rpt'];
						if ('arcrpt' in pRsp) sRsp += '\nReport ZIP: '+pRsp['arcrpt'];
						if ('arctst' in pRsp) sRsp += '\nSnapshot/Export ZIP: '+pRsp['arctst'];
					}
					pCon.readOnly = false;
					pCon.value += sRsp;
					pCon.scrollTop = pCon.scrollHeight;
					pCon.readOnly = true;
				}
			//	console.log('DBG=>>> go - rsp: '+sRsp);
			} catch (pErr) { console.log("ERR=>>> launch request failure"); }
		} else console.log('DBG=>>> go: failure in auth request');
	} else console.log('ERR=>>> launch invalidation');
	btn.innerText="Launch";
	btn.disabled=false;
	return;
}

function go(btn) {
	if ( btn.innerText == "Launch") {
		btn.innerText="...";
		launchTest(btn,document.URL.replace("feather.html","console.html"));
	} else if ( btn.innerText == "Console") {
		btn.innerText="...";
		launchConsole(btn);
	}
	return;
}

function genPwdFld(sCtl,sTxt,iPad) {
	var sGen = "<input type=password id="+sCtl+" name="+sCtl+">";
	var sRet = "<td align=right><img src=img/glass.png width=16><label for="+sCtl+"><strong>"+sTxt+"</strong></label></td><td colspan="+String(iPad)+">"+sGen+"</td>\n";
	return(sRet);
}

function genIntFld(sCtl,sTxt,iPad,val) {
	var sDefault = (val >= 0) ? " value="+val.toString() : "";
	var sGen = "<input type=number id="+sCtl+" name="+sCtl+sDefault+">";
	var sRet = "<td align=right><label for="+sCtl+"><strong>"+sTxt+"</strong></label></td><td colspan="+String(iPad)+">"+sGen+"</td>\n";
	return(sRet);
}

function genChkFld(sCtl,sTxt,iPad) {
	var sRet  = "<td colspan="+iPad+" align=right>";
	sRet += "<input type=checkbox id="+sCtl+" name="+sCtl+"><label for="+sCtl+">"+sTxt+"</label>";
	sRet += "</td>";
	return(sRet);
}

function genTxtFld(sCtl,sTxt,iPad) {
	var sGen = "<input type=text id="+sCtl+" name="+sCtl+">";
	var sRet = "<td align=right><label for="+sCtl+"><strong>"+sTxt+"</strong></label></td><td colspan="+String(iPad)+">"+sGen+"</td>\n";
	return(sRet);
}

function genBtnAct( sLabel, sOnClick) {
	sStl = "width:240px;background-color:#886800;color:#282828;border:none;";
	sClk = "this.disabled=true;"+sOnClick;
	return("<button style=\""+sStl+"\" onclick=\""+sClk+"\"><strong>"+sLabel+"</strong></button>");
}

function genEntry() {
	var sRet = "<div id=entry>\n";
	sRet += "<table align=center>\n";
	sRet += "<form>\n";
	sRet += "<tr>"+genTxtFld("tstmsid","User ID:",2)+genPwdFld("tstpass","Password:",2)+"</tr>";
	sRet += "<tr>"+genTxtFld("tstgit","Git repository:",5)+"</tr>";
	sRet += "<tr>"+genTxtFld("tstgbn","Git branch:",5)+"</tr>";
//	sRet += "<tr>"+genTxtFld("tstgrd","Directory (git):",5)+"</tr>";
	sRet += "<tr><td align=left colspan=6><br><i>Optional</i></td></tr>\n";
	sRet += "<tr><td align=center colspan=6><hr></td></tr>\n";
	sRet += "<tr>"+genTxtFld("tstname","Test Name:",5)+"</tr>";
	sRet += "<tr>";
	sRet += ""+genTxtFld("tstdoc","Plan (JMX):",3)+"";
	sRet += ""+genChkFld("tstfrm","Selenium",2)+"";
	sRet += "</tr>";
	sRet += "<tr>";
	sRet += genIntFld("tstlgs","LGs:",-1,1);
	sRet += genIntFld("tstvuc","Threads (per LG):",1,2);
	sRet += genIntFld("tstdtn","Duration (ms):",1,120000);
	sRet += "</tr>";
	sRet += "</form>\n";
	sRet += "<tr><td align=center colspan=6><br></td></tr>\n";
	sRet += "<tr><td align=center colspan=6>"+genBtnAct("Launch","go(this);")+"</td></tr>\n";
	sRet += "</table>\n";
	sRet += "</div>\n";
	return(sRet);
}

function updateView(pBody) {
//	console.log('DBG=>>> updateView: invoked');
	var pNow = document.getElementById("now");
	if ( pNow != null) {
		pNow.innerHTML = nowAsTxt();
	}
}

function genView() {
	var sRet  = "<table align=center>\n";
	sRet += "<tr><td><h1>Starling</h1></td><td align=center></td><td align=right>";
	sRet += genClock();
	sRet += "</td></tr>";
	sRet += "<tr>";
	sRet += "<td align=center colspan=3>";
	sRet += "<table align=center><tr><td align=center>\n"+genEntry()+"</td></tr></table>\n";
	sRet += "</td>";
	sRet += "</tr>";
	sRet += "<tr><td align=left colspan=3><i>Info:</i></td></tr>";
	sRet += "<tr><td align=center colspan=3><textarea id=txtcon rows=40 cols=200 readonly></textarea></td></tr>";
	sRet += "</table>";
	return(sRet);
}

function ctlInit() {
//	console.log('DBG=>>> ctlInit: invoked');
	var pBody = document.getElementById( "ecmakitEnabledBody");
	if ( pBody != null) {
		pBody.innerHTML = genView();
	}
//	alert(document.cookie.match(/PHPSESSID=[^;]+/));
}

function viewRefresh( callback) {
	var pBody = document.getElementById( "ecmakitEnabledBody");
	if ( pBody != null) {
		updateView(pBody);
	}
}
