LPX = 64
TGT = dev
PGM := $(or $(PGM),"starling")
HST = starlingdev
DML = "$(HOME)/www"
DRN = 127.0.0.1:32000
DRP = $(PGM)/$(TGT)
IMG = starling

all: docker
	@echo Build complete

clean:
	@rm -fv .DS_Store

docker:
	docker build --no-cache -t $(IMG) .
	docker image prune -f
	@mkdir -pv $(HOME)/var/$(PGM)/etc $(HOME)/var/$(PGM)/var $(HOME)/var/$(PGM)/pub

push:
	@docker login -u $(shell cat $(HOME)/etc/msid.json | jq .msid | tr -d '"') -p "$(shell cat $(HOME)/etc/msid.json | jq .password | tr -d '\"')" $(DRN)
	@docker tag $(IMG):latest $(DRN)/$(DRP)/$(IMG):latest
	@docker push $(DRN)/$(DRP)/$(IMG):latest

syncwww:
	@rsync -rltDOi --exclude='*.swp' ./www/ $(DML)/
