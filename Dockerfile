FROM	rhel8:latest
ARG	USR=starling
ARG	HDL=/home/starling
ARG	SHRVER=4.15.2-11
ARG	JMETER_VERSION=5.5
ARG	ZOOKINI_VER=102
USER	root
RUN	microdnf update && microdnf reinstall -y tzdata \
		&& microdnf install -y procps-ng sudo tar rsync curl findutils iputils diffutils patch git openssl jq vim gnupg logrotate yum \
		&& rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm \
		&& microdnf install -y unzip zip bzip2 nc java-1.8.0-openjdk-headless \
		&& echo '[fluent-bit]' > /etc/yum.repos.d/fluentbit.repo \
		&& echo 'name = Fluent Bit' >> /etc/yum.repos.d/fluentbit.repo \
		&& echo 'baseurl = https://packages.fluentbit.io/centos/$releasever/$basearch/' >> /etc/yum.repos.d/fluentbit.repo \
		&& echo 'gpgcheck=1' >> /etc/yum.repos.d/fluentbit.repo \
		&& echo 'gpgkey=https://packages.fluentbit.io/fluentbit.key' >> /etc/yum.repos.d/fluentbit.repo \
		&& echo 'repo_gpgcheck=1' >> /etc/yum.repos.d/fluentbit.repo \
		&& echo 'enabled=1' >> /etc/yum.repos.d/fluentbit.repo \
		&& microdnf install -y fluent-bit \
		&& rm -fv /etc/fluent-bit/* \
		&& curl -sLk -O "https://repo.mysql.com/mysql80-community-release-el8-1.noarch.rpm" \
		&& rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022 \
		&& yum localinstall -y ./mysql80-community-release-el8-1.noarch.rpm && yum update -y \
		&& yum install -y mysql-community-client \
		&& microdnf install -y python39 python39-devel python39-setuptools python39-pip \
		&& microdnf clean all \
      && curl -sLk -O https://git.albertleadata.org/rk4n3/nixlib/-/wikis/eet && chmod 755 eet && mv -v eet /usr/local/bin/ \
      && groupadd -g 14242 ${USR} \
      && useradd -u 104242 -g 14242 -d ${HDL} -s /bin/bash -c "${USR}" -m ${USR} \
		&& /bin/bash -c "mkdir -pv ${HDL}/{.ssh,bin,lib,etc,var,tmp,log,arc}" \
		&& echo "${USR} ALL=(ALL) NOPASSWD: SETENV: ALL" >> /etc/sudoers.d/${USR} \
		&& chmod 0440 /etc/sudoers.d/${USR} \
      && chown -R ${USR}:${USR} ${HDL} && chmod -R g+w,g+s ${HDL} \
      && pip3 install --upgrade pip \
      && pip3 install --upgrade chardet \
      && pip3 install certifi \
      && pip3 install idna \
      && pip3 install urllib3 \
      && pip3 install influxdb \
      && pip3 install kazoo \
      && pip3 install kafka-python \
      && pip3 install locust \
		&& curl -sLk -O https://dlcdn.apache.org//jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz \
      && tar -xzf apache-jmeter-${JMETER_VERSION}.tgz -C ${HDL} \
      && ln -s ${HDL}/apache-jmeter-${JMETER_VERSION} ${HDL}/jmeter \
      && rm -rf ${HDL}/jmeter/docs ${HDL}/jmeter/printable_docs ${HDL}/jmeter/backups/* \
      && rm apache-jmeter-${JMETER_VERSION}.tgz \
      && curl -sLk -O https://git.albertleadata.org/rk4n3/zookini/-/wikis/zookini-102.tgz \
      && tar -xzf zookini-${ZOOKINI_VER}.tgz -C ${HDL} \
      && rm -rfv zookini-${ZOOKINI_VER}.tgz ${HDL}/lib/requests
ADD	--chown=${USR}:${USR} bin/rc.svc ${HDL}/bin/
ENV	HOME=${HDL}
ENV	PATH=${HDL}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV	LD_LIBRARY_PATH=${HDL}/lib
ENV	PYTHONPATH=${HDL}/lib
USER	starling
WORKDIR ${HDL}
ENTRYPOINT rc.svc docker
