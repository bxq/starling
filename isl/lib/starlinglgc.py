#!/usr/bin/python
from __future__ import print_function
from zookini.plugins.botplugin import BotPlugin,BotPluginT1
from zookini.zookini import rpcAuth,rpcAPI
import zookini.plugins.botplugin

class StarlingPlugin(zookini.plugins.botplugin.BotPluginT1):
	def __init__( self, iBotIdx=0):
		super(StarlingPlugin,self).__init__(iBotIdx)

	def pollUpstream( self, sChild):
		pRet = {}
	#	print( "%s=>>> %s" % ('DBG','StarlingPlugin::pollUpstream invoked'))
		sTkn = rpcAuth()
		pTst = rpcAPI( {'rpctkn':sTkn,'cmd':'tstinf','id':sChild})
		pPln = rpcAPI( {'rpctkn':sTkn,'cmd':'plninf','id':str(pTst['pln'])})
		pApp = rpcAPI( {'rpctkn':sTkn,'cmd':'appinf','id':str(pPln['app'])})
		pRet['cmdb'] = pApp['cmdb']
		pRet['eid'] = pTst['eid']
		pRet['status'] = pTst['status']
		pRet['job'] = pTst['jobname']
		pRet['frm'] = pPln['frm']
		pRet['git'] = pPln['git']
		pRet['gbn'] = pPln['gbn']
		pRet['doc'] = pPln['doc']
		pRet['lgs'] = pTst['lgs']
		pRet['vusr'] = pTst['vusr']
		pRet['cpu'] = pTst['cpu']
		pRet['mms'] = pTst['mms']
		pRet['mmx'] = pTst['mmx']
		if 'xqt' in pTst:
			pRet['xqt'] = pTst['xqt']
		if int(pRet['status']) < 0 or int(pRet['status']) > 1:
			pRet['botstg'] = pRet['status']
		return pRet

	def genFinalization( self, sWDL, sCtx):
		sPDL = self.sHome+'/pub'
		sRet = ['#!/bin/bash']
		sRet.append('nohup /home/starling/bin/botppc '+sCtx+' 1>>'+self.sHome+'/log/botppc.log 2>&1 &')
		return sRet

	def genFinalization_legacy( self, sWDL, sCtx):
		sPDL = self.sHome+'/pub'
		sRet = ['#!/bin/bash']
		sRet.append('cd '+sWDL)
		sRet.append('LGS=$(echo [0-9]*)')
		sRet.append('if [ "${LGS}" != "" ]; then')
		sRet.append('	find . -type d -name src | sed "s/^/rm -rf /" | bash')
		sRet.append('	find . -type d -name jmeter | sed "s/^/rm -rf /" | bash')
		sRet.append('	find . -type d -name gatling | sed "s/^/rm -rf /" | bash')
		sRet.append('	RPT=$(cat 001/rpt.txt)')
		sRet.append('	tar cf ${RPT}.tar -C 001 ${RPT} ; bzip2 ${RPT}.tar ; mv ${RPT}.tar.bz2 ${RPT}.tbz')
		sRet.append('	zip -r '+sPDL+'/${RPT}.zip *')
		sRet.append('	cp '+sPDL+'/${RPT}.zip .')
		sRet.append('	if [ -x 001/rawpub ]; then { nohup 001/rawpub 1>>'+self.sHome+'/log/rawpub.log 2>&1 & } ; fi')
		sRet.append('	sleep 2 ; mv 001/${RPT} '+sPDL+'/')
		sRet.append('	cd ..')
		sRet.append('	sync')
		sRet.append('	rm -rf '+sCtx)
		sRet.append('fi')
		return sRet
