#!/bin/bash
#USER=$(whoami)
USER=starling
HOME=$(eval echo "~${USER}")
HDL=${HOME}
#HDL=/opt/app-root
RDL="var/starling/test"
WDL=${HDL}/${RDL}
P4M="xaas"
UFN=131072
# Report grinder ...
PGM=rptwww
PFN=${HOME}/tmp/${PGM}.pid
LFN=${HOME}/log/${PGM}.log

export PYTHONPATH=${HDL}/lib

function detectService {
	KEY="${1}"
	P2K=$(ps -heo 'pid,args' | grep "${KEY}" | grep -v grep | sed 's/^ *//' | cut -d' ' -f1 | head -1)
	if [ $((${P2K})) -gt 1 ]; then echo ${P2K} ; fi
}

function startFluentbit {
	nohup /opt/fluent-bit/bin/fluent-bit -c /etc/fluent-bit/fluent-bit.conf 1>/dev/null 2>&1 &
}

function stopFluentbit {
	echo "NFO=>>> stopping fluent-bit unsupported"
}

function startZookeeper {
	SPN=$(detectService "java -Dzookeeper")
	if [ "${SPN}" == "" ]; then
		ZKD=/usr/zookeeper
		ZKH=/var/lib/zookeeper
		cd ${ZKH}
		${ZKD}/bin/zkServer.sh start
	else echo -e "\nNFO=>>> zookeeper process (${SPN}) detected, skipping start\n" ; fi
}

function stopZookeeper {
	SPN=$(detectService "java -Dzookeeper")
	if [ "${SPN}" != "" ]; then
		echo -n "Killing PID ${SPN} ..."
		kill ${SPN}
		echo "OK"
	else echo -e "\nNFO=>>> no Zookeeper process detected\n" ; fi
}

function startATF {
	KHN=$(grep "^starling.console.host" ${HDL}/.starlingrc | cut -d'=' -f2- | sed "s/^  *//" | sed "s/  *$//")
	OHN=$(grep "^starling.opsbridge.host" ${HDL}/.starlingrc | cut -d'=' -f2- | sed "s/^  *//" | sed "s/  *$//")
	if [ "${KHN}" != "" -a "${OHN}" != "" ]; then
		screen -S atf -d -m ${HDL}/bin/atff ${KHN} ${OHN}
	else echo -e "\nERR=>>> console and opsbridge hosts required" ; fi
}

function stopATF {
	SPN=$(detectService "bin/atff")
	if [ "${SPN}" != "" ]; then
		echo -n "Killing PID ${SPN} ... "
		kill ${SPN}
		echo "OK"
	else echo -e "\nNFO=>>> no ATF Broker process detected" ; fi
}

function startFCGI {
	nohup spawn-fcgi -p 8000 -n ${HOME}/bin/${PGM} 1>${LFN} 2>&1 &
	echo $! > ${PFN}
}

function stopFCGI {
	if [ -r ${PFN} ]; then
		kill $(cat ${PFN})
	else echo -e "\nNo PID file detected\n" ; fi
	rm -f ${PFN}
}

function startNginx {
	local SVC=nginx
	sudo rm -f /var/lib/${SVC}/${SVC}.pid
	/usr/sbin/${SVC} -t
	if [ $? -eq 0 ]; then
		sudo sh -c 'nohup /usr/sbin/php-fpm --nodaemonize 1>/dev/null 2>&1 &'
		sudo rm -f ${SVC}.pid /var/log/${SVC}/*.log
		sudo sh -c 'cd /var/lib/nginx ; nohup /usr/sbin/nginx 1>>/var/log/nginx/console-access.log 2>>/var/log/nginx/console-error.log &'
	else echo "ERR=>>> there's something wrong with the nginx config file" ; fi
}

function stopNginx {
	if [ -r /var/lib/nginx/nginx.pid ]; then sudo kill $(cat /var/lib/nginx/nginx.pid) ; fi
	sudo rm -f /var/lib/nginx/nginx.pid
}

function startGrinder {
	startFCGI
#	nohup nginx -c ${HOME}/etc/nginx/nginx.conf -g 'daemon off;' 1>${HOME}/log/nginx-console.log 2>&1 &
}

function stopGrinder {
#	SPN=$(detectService "master process nginx")
#	if [ "${SPN}" != "" ]; then
#		echo -n "Killing PID ${SPN} ... "
#		kill ${SPN}
#		echo "OK"
#	else echo -e "\nNFO=>>> no grinder process detected" ; fi
	stopFCGI
}

function startBotLGC {
	mkdir -pv ${HDL}/log ${WDL}
	cd ${WDL}
	nohup ${HDL}/bin/rc.lgc 1>${HDL}/log/lgc.log 2>&1 &
}

function stopBotLGC {
	BPN=$(detectService "lcbot")
	if [ "${BPN}" != "" ]; then
		echo -n "Killing PID ${BPN} ... "
		kill ${BPN}
		echo "OK"
	fi
}

function startReaper {
	if [ -f ${HDL}/tmp/islrpr-*.lck ]; then rm -f ${HDL}/tmp/islrpr-*.lck ; fi
	islrpr start
}

function stopReaper {
	if [ -f ${HDL}/tmp/islrpr-*.lck ]; then
		kill $(cat ./tmp/islrpr-*.lck)
		rm -f ${HDL}/tmp/islrpr-*.lck
	fi
}

function startServices {
	startFluentbit
	CVS=$(grep "^starling.lgs.p4m" ${HDL}/.starlingrc | cut -d'=' -f2 | sed "s/ *//" | head -1)
	if [ "${CVS}" != "" ]; then P4M=${CVS} ; fi
	if [ "${P4M}" == "xaas" ]; then
		startZookeeper
		sleep 2
		startBotLGC
	fi
#	This is only for co-located synthetic ATF broker
#	startATF
	startGrinder
	startNginx
	startReaper
}

function stopServices {
	stopReaper
	stopNginx
	stopGrinder
#	This is only for co-located synthetic ATF broker
#	stopATF
	stopBotLGC
	sleep 2
	stopZookeeper
	stopFluentbit
}

function showUsage {
	echo -e "\nUsage: rc.starling [start|stop|restart|bounce|usage]\n"
}

CMD=usage
if [ "${1}" != "" ]; then CMD=${1} ; fi
case ${CMD} in
	'start') startServices $@ ;;
	'stop') stopServices $@ ;;
	'restart') stopServices $@ ; sleep 2 ; startServices $@ ;;
	'bounce') stopFCGI $@ ; sleep 1 ; startFCGI $@ ;;
	*) showUsage ;;
esac
