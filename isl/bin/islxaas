#!/bin/bash
# Ensure correct operation via SSH or cron
export USER=starling
export HOME=/home/${USER}
export PATH=${HOME}/bin:/usr/local/bin:/usr/bin:/bin
# Script variables
OLF=${HOME}/log/islxaas-console-$$.log
CLF=${OLF}
TFN=${HOME}/tmp/islxaas-$$.txt

#LGX=$((0))
DLY="8"
#TST=""
#PLN=""

function dbCxn {
	local DBH=$(grep "^starling.db.host" ${HOME}/.starlingrc | cut -d= -f2- | sed "s/ //g")
	local DBU=$(grep "^starling.db.user" ${HOME}/.starlingrc | cut -d= -f2- | sed "s/ //g")
	local DBP=$(grep "^starling.db.pass" ${HOME}/.starlingrc | cut -d= -f2- | sed "s/ //g")
	local DBN=$(grep "^starling.db.name" ${HOME}/.starlingrc | cut -d= -f2- | sed "s/ //g")
	echo "-h ${DBH} -u ${DBU} --password=${DBP} ${DBN}"
}

function stageFootprint_legacy {
	local DBC=$(dbCxn)
	local CTL=0
	local TPC=""
	if [ ${LGX} -eq 0 ]; then CTL=1 ; fi
#	local QRY="select a.id,p.id,p.lgs,p.tnt,p.git,p.gbn,a.job from pln p join app a on a.id=p.app where p.id=${PLN};"
	local QRY="select a.id,p.id,p.lgs,p.tnt,p.git,p.gbn,a.job from tst t join pln p on p.id=t.pln join app a on a.id=p.app where t.id=${TST};"
	local QVS=$(echo "${QRY}" | mysql -s -B ${DBC} | sed "s/,/;/g" | sed "s/	/,/g")
	local APP=$(echo "${QVS}" | cut -d',' -f1)
	local PLN=$(echo "${QVS}" | cut -d',' -f2)
	local LGS=$(echo "${QVS}" | cut -d',' -f3)
	local LGC=$((${LGS}))
	local TNT=$(echo "${QVS}" | cut -d',' -f4)
	local GIT=$(echo "${QVS}" | cut -d',' -f5)
	local GBN=$(echo "${QVS}" | cut -d',' -f6)
	local JOB=$(echo "${QVS}" | cut -d',' -f7)
	if [ "${JOB}" != "" -a "${GBN}" != "" ]; then
		local JDL=$HOME/var/starling/${LGX}/${JOB}
		rm -rf ${JDL}
		mkdir -p ${JDL}
		cd ${JDL}
		local LFB=${JDL}/log/lg
		local CLF=${LFB}-0.log
	#	Acquire from revision control
		if [ "${CTL}" == "1" ]; then git clone ${GIT} 1>>${OLF} 2>&1 ; else git clone ${GIT} ; fi
		cd $(ls | head -1)
	#	Now that we're in snapshot, init logging
		mkdir -p ../bin ../lib ../etc ../log ../jmeter ../target/jmeter/lib/ext
		if [ "${CTL}" == "1" ]; then
			touch ${CLF} ${LFB}-1.log ${LFB}-2.log
			(tail -n 1000 -f ${CLF} ${LFB}-1.log ${LFB}-2.log 2>&1 & echo "$!" >${TFN}) | LD_LIBRARY_PATH=${HOME}/lib ${HOME}/bin/zkcon --host=localhost:2181 --ons=starling --ctx=test ${TST} &
			sleep 1
			TPC=$(cat ${TFN}) ; rm -f ${TFN}
			cat ${OLF} >> ${CLF}
		#	...
			echo "DBG=>>> $(pwd) => cloned: $(ls | head -1)" >> ${CLF}
			echo "DBG=>>> $(pwd) => checking out: ${GBN}" >> ${CLF}
			git checkout ${GBN} 1>> ${CLF} 2>&1
			echo "DBG=>>> initialized footprint:" >> ${CLF}
			git status 1>> ${CLF} 2>&1
			echo "" >> ${CLF}
			find . -type f | grep -v "\.git/" >> ${CLF}
		else
			git checkout ${GBN}
			git status
		fi
	#	...
		if [ ${LGX} -eq 0 ]; then
			rsync -ai ./bin/ ../bin/ 1>>${CLF} 2>&1
			if [ -d ./lib ]; then rsync -ai ./lib/ ../lib/ 1>>${CLF} 2>&1 ; fi
			rsync -ai ./etc/ ../etc/ 1>>${CLF} 2>&1
			rsync -ai $HOME/jmeter/ ../target/jmeter/ 1>>${CLF} 2>&1
			rsync -ai ./target/jmeter/ ../target/jmeter/ 1>>${CLF} 2>&1
			rsync -ai ${HOME}/etc/*.jks ../target/jmeter/bin/ 1>>${CLF} 2>&1
			cd ..
			if [ ${LGC} -gt 1 ]; then
				local LG1="$(grep '^starling.ngn.lg1' ${HOME}/.starlingrc | cut -d= -f2- | sed 's/ //g')"
				local LG2="$(grep '^starling.ngn.lg2' ${HOME}/.starlingrc | cut -d= -f2- | sed 's/ //g')"
				echo "DBG=>>> spinning up load generator 1 (${LG1}) ..." >> ${CLF}
				ssh ${LG1} "nohup /home/starling/bin/islxaas 1 ${TST} 1>>${LFB}-1.log 2>&1 &"
				echo "DBG=>>> spinning up load generator 2 (${LG2})..." >> ${CLF}
				ssh ${LG2} "nohup /home/starling/bin/islxaas 2 ${TST} 1>>${LFB}-2.log 2>&1 &"
				echo "DBG=>>> waiting ${DLY} seconds for load generators to spin up ..." >> ${CLF}
				sleep ${DLY}
				echo "DBG=>>> $HOME/bin/isltst loadctl ${GIT} ${TNT} ${GBN} 801:802" >> ${CLF}
				$HOME/bin/isltst loadctl ${GIT} ${TNT} ${GBN} "801:802" 1>>${CLF} 2>&1
			else
				echo "DBG=>>> $HOME/bin/isltst loadctl ${GIT} ${TNT} ${GBN} ''" >> ${CLF}
				$HOME/bin/isltst loadctl ${GIT} ${TNT} ${GBN} '' 1>>${CLF} 2>&1
			fi
		else
			rsync -ai ./bin/ ../bin/
			if [ -d ./lib ]; then rsync -ai ./lib/ ../lib/ ; fi
			rsync -ai ./etc/ ../etc/
			rsync -ai $HOME/jmeter/ ../target/jmeter/
			rsync -ai ./target/jmeter/ ../target/jmeter/
			rsync -ai ${HOME}/etc/*.jks ../target/jmeter/bin/
			cd ..
			echo "DBG=>>> $HOME/bin/isltst loadgen ${GIT} ${TNT} ${GBN} 80${LGX}"
			$HOME/bin/isltst loadgen ${GIT} ${TNT} ${GBN} "80${LGX}"
		fi
		sync
		if [ "${TPC}" != "" ]; then kill ${TPC} ; fi
	fi
}

function spinUpLG {
	local BOT="000"
	local LGN=${1}
	local LGX=${2}
	local TST=${3}
	local HST=${4}
	local FRM=${5}
	if [ "${LGX}" != "" ]; then BOT="$(printf %03d ${LGX})" ; fi
	if [ "${BOT}" != "000" ]; then
		local JDL=${HOME}/var/starling/test/${TST}/${BOT}
	#	echo "DBG=>>> spinning up LG ${LGX} / ${LGN} with Test: ${TST}, in ${JDL}"
		rm -rvf ${JDL}
		mkdir -pv ${JDL}/log
		cd ${JDL}
		if [ "${HST}" != "" ]; then
		#	local CMD="nohup ${HOME}/bin/rc.lgs ${TST} ${LGX} ${LGN} </dev/null 1>log/console.log 2>&1 &"
			local CMD="${HOME}/bin/rc.lgs ${TST} ${LGX} ${LGN} ${FRM}"
		#	echo "DBG=>>> CMD: ssh ${HST} \"${CMD}\""
			ssh ${HST} "${CMD}"
		else
			nohup ${HOME}/bin/rc.lgs ${TST} ${LGX} ${LGN} 1> log/console.log 2>&1 &
		fi
	fi
}

function launchTestPlan {
	local TST=$((${1}))
	if [ ${TST} -gt 0 ]; then
		local DBC=$(dbCxn)
		local QRY="select a.id,p.ngn,p.frm,a.job,t.nfo from tst t join pln p on p.id=t.pln join app a on a.id=p.app where t.id=${TST};"
		local QVS=$(echo "${QRY}" | mysql -s -B ${DBC} | sed "s/,/;/g" | sed "s/	/,/g")
		local APP=$(echo "${QVS}" | cut -d',' -f1)
		local NGN=$(echo "${QVS}" | cut -d',' -f2)
		local FRM=$(echo "${QVS}" | cut -d',' -f3)
		local JOB=$(echo "${QVS}" | cut -d',' -f4)
		local NFO=$(echo "${QVS}" | cut -d',' -f5 | sed "s/;/,/g")
		echo "DBG=>>> nfo: ${NFO}"
		local LGC=$(($(echo "${NFO}" | jq .lgs)))
		local JSN=$(${HOME}/bin/isllgs --allocate=${TST})
		local LGL=$(($(echo "${JSN}" | jq '. | length')))
		echo "DBG=>>> LGL: ${LGL} <-> LGC: ${LGC}"
		if [ ${LGL} -eq ${LGC} ]; then
			local LHS=$(echo "${JSN}" | tr -d '"[],')
		#	echo "Got: ${LHS}"
			local LG=$((0))
			for LGH in ${LHS}; do
				LG=$((${LG} + 1))
			#	echo "Spinning up LG ${LG} / ${LGL} on host: ${LGH} ..."
			#	echo "      spinUpLG ${LGL} ${LG} ${TST} ${LGH} ${FRM}"
				spinUpLG ${LGL} ${LG} ${TST} ${LGH} ${FRM}
				sleep 0.1
			done
		else
			echo "ERR=>>> LG allocation failure"
			${HOME}/bin/isllgs --release=${TST}
		fi
	else echo "ERR=>>> invalid test specification: ${1}" ; fi
}

launchTestPlan $@
