#!/usr/bin/python3
from __future__ import print_function
import sys
import os
import json
from kazoo.client import KazooClient
from starling.svc import StarlingService

def cmdAppList():
	pSvc = StarlingService()
	sUser = pSvc.getCfg('apiusr')
	sPass = pSvc.getCfg('apipwd')
	sRsp = pSvc.rpcAuth( sUser, sPass)
	if sRsp != "unauthorized for API":
		pReq = {'cmd':'applst'}
		pRsp = pSvc.rpcAPI(pReq)
		print(pRsp)
	else:
		print('%s=>>> %s' % ('ERR',sRsp))

def cmdJobList(sJob):
	pSvc = StarlingService()
	sUser = pSvc.getCfg('apiusr')
	sPass = pSvc.getCfg('apipwd')
	sRsp = pSvc.rpcAuth( sUser, sPass)
	if sRsp != "unauthorized for API":
		pReq = {'cmd':'appjob','id':int(sJob)}
		pRsp = pSvc.rpcAPI(pReq)
		lTst = int(pRsp['tst']) if 'tst' in pRsp else 0
		if lTst > 0:
			print('%s=>>> found test: %d' % ('DBG',lTst))
		else:
			print('%s=>>> %s' % ('DBG','no test found'))
	else:
		print('%s=>>> %s' % ('ERR',sRsp))

def cmdJobNudge(sJob):
	pSvc = StarlingService()
	sHost = pSvc.getCfg('hst')
	sUser = pSvc.getCfg('apiusr')
	sPass = pSvc.getCfg('apipwd')
	sRsp = pSvc.rpcAuth( sUser, sPass)
	if sRsp != "unauthorized for API":
		lJob = int(sJob)
		pReq = {'cmd':'appjob','id':lJob}
		pRsp = pSvc.rpcAPI(pReq)
		lTst = int(pRsp['tst']) if 'tst' in pRsp else 0
		if lTst > 0:
			pZK=None
			pZK = KazooClient(hosts=sHost+':2181')
			pZK.start()
			sNfo = '{}'
			sPath = '/control/starling/test/'+str(lTst)
			pStats = pZK.exists(sPath)
			if pStats != None:
				sNfo, stat = pZK.get(sPath)
				pNfo = json.loads(sNfo.decode('utf-8'))
				sJSON = json.dumps(pNfo)
				iTRG = int(pNfo['TRG']) if 'TRG' in pNfo else 0
				if iTRG > 0 and iTRG < 2:
					print('%s=>>> nudging TRG for job %d: %d -> 2' % ('NFO',lTst,iTRG))
					pNfo['TRG']=2
					sJSON = json.dumps(pNfo)
					try:
						pZK.set( sPath, sJSON.encode("utf-8"))
					except Exception as pErr:
						print(pErr)
				else:
					print('%s=>>> no TRG nudge applicable for job %d (TRG=%d)' % ('NFO',lTst,iTRG))
				print('%s=>>> JSON for job %d: %s' % ('DBG',lTst,sJSON))
		else:
			print('%s=>>> %s' % ('ERR','no test found'))
	else:
		print('%s=>>> %s' % ('ERR',sRsp))

def cmdStatus():
	pSvc = StarlingService()
	pDB = pSvc.dbConnect()
	if pDB != None:
		pQry = pDB.cursor()
	#	sQry = 'select * from tst where status > 0'
		sQry = 'select a.id,u.login,a.name from app a join who u on u.id=a.usr where a.id > 0 order by a.usr,a.created'
		pQry.execute(sQry)
		pRows = pQry.fetchall()
		for pRow in pRows:
			print(pRow)

if __name__ == '__main__':
	sCmd = 'status'
	sJob = ''
#	Argument parsing
	for arg in sys.argv:
		pair = arg.split( "=", 1)
		if pair[0] == "--applst":
			sCmd="applst"
		elif pair[0] == "--joblst":
			sCmd="appjob"
			sJob=pair[1]
		elif pair[0] == "--nudge":
			sCmd="nudge"
			sJob=pair[1]
		else:
		#	print('%s=>>> %s: %s' % ('DBG','arg',arg))
			pass

#	Command routing
	if sCmd == "applst":
		cmdAppList()
	elif sCmd == "appjob":
		cmdJobList(sJob)
	elif sCmd == "nudge":
		cmdJobNudge(sJob)
	else:
		cmdStatus()
