#!/usr/bin/python3
from __future__ import print_function
from os.path import expanduser
from urllib3 import disable_warnings
from urllib3.exceptions import InsecureRequestWarning
import sys
import time
import json
import ssl
import warnings
import requests
import traceback
import npyscreen
#import subprocess
import MySQLdb

disable_warnings(InsecureRequestWarning)

pCfg={	'home':expanduser("~"),
			'dbg':'full',
			'tgt':'starlingtargetenvironment',
			'hst':'starlingservicehost',
			'dbhost':'starlingdbhost',
			'dbuser':'starlingdblogin',
			'dbpass':'starlingdbpassword',
			'dbname':'starlingdbname',
			'dbcxn':None,
			'tkn':None,
			'log':None }

def logMsg( sMsg):
	global pCfg
	fLog = open( pCfg['home']+"/log/isladm.log", "a")
	fLog.write( "DBG=>>> "+sMsg+"\n")
	fLog.close

def loadConfig( sCfgFile=".starlingrc"):
	global pCfg
	fCfg = open( pCfg['home']+"/"+sCfgFile, "r")
	for sLine in fCfg:
		pOpt = sLine.split( "=", 1)
		if pOpt[0].rstrip() == "starling.env.tgt":
			pCfg['tgt'] = pOpt[1].lstrip().rstrip()
		elif pOpt[0].rstrip() == "starling.dbg.level":
			pCfg['dbg'] = pOpt[1].lstrip().rstrip()
		elif pOpt[0].rstrip() == "starling.api.host":
			pCfg['hst'] = pOpt[1].lstrip().rstrip()
		elif pOpt[0].rstrip() == "starling.db.host":
			pCfg['dbhost'] = pOpt[1].lstrip().rstrip()
		elif pOpt[0].rstrip() == "starling.db.user":
			pCfg['dbuser'] = pOpt[1].lstrip().rstrip()
		elif pOpt[0].rstrip() == "starling.db.pass":
			pCfg['dbpass'] = pOpt[1].lstrip().rstrip()
		elif pOpt[0].rstrip() == "starling.db.name":
			pCfg['dbname'] = pOpt[1].lstrip().rstrip()

def rpcAuth( sLogin, sPassword):
	global pCfg
	sRet = ''
	sURL = "https://"+pCfg['hst']+"/index.php?ctx=api"
	pHdr = {'Content-type':'application/json'}
	pOp = {}
	pOp['cmd'] = 'auth'
	pOp['starling_uid'] = sLogin;
	pOp['starling_pwd'] = sPassword;
	pDat = {'req': pOp}
	try:
		hRsp = requests.post( sURL, headers=pHdr, data=json.dumps(pDat), verify=False)
		pRsp = hRsp.json()
	#	logMsg("JSON: "+json.dumps(pRsp))
		sRet = pRsp['msg'] if 'msg' in pRsp else ''
		pCfg['tkn'] = None if sRet == "unauthorized for API" else sRet
	except Exception as pErr:
		print( pErr)
	return sRet

def rpcAPI( pReq):
	global pCfg
	pRet = {}
	sTkn = pArgs['rpctkn'] if 'rpctkn' in pReq else None
	if sTkn == None or sTkn == '':
		sTkn = pCfg['tkn'] if 'tkn' in pCfg else None
		if sTkn == None or sTkn == '':
			if 'starling_uid' in pReq and 'starling_pwd' in pReq:
				sTkn = rpcAuth( pReq['starling_uid'], pReq['starling_pwd'])
	sURL = "https://"+pCfg['hst']+"/index.php?ctx=api"
	pHdr = {'Content-type':'application/json','Cookie':'AUTH_BEARER_default='+sTkn}
	pDat = {'req': pReq}
	try:
		hRsp = requests.post( sURL, headers=pHdr, data=json.dumps(pDat), verify=False)
		pRet = hRsp.json()
	#	logMsg("JSON: "+json.dumps(pRet))
	except Exception as pErr:
		print( pErr)
	return pRet


def dbConnect():
	global pCfg
	pRet = pCfg['dbcxn']
	if pRet == None:
		pRet = MySQLdb.connect( pCfg['dbhost'],pCfg['dbuser'],pCfg['dbpass'],pCfg['dbname'])
		if pRet != None:
			pCfg['dbcxn']=pRet
	return pRet
		
def fetchOrgList():
	global pCfg
	pRet={}
	pCxn = dbConnect()
	pQry = pCxn.cursor()
	sQry = "select idx,label from lut where lut=201 and idx > 0 order by idx";
	pRows = None
	try:
		pQry.execute( sQry)
		pRows = pQry.fetchall()
	except Exception as pE1:
		print( "INF: no active starling tenant spaces found" % pE1)
	for pRow in pRows:
		sOID = str(pRow[0])
		sOrg = str(pRow[1])
		pRet[sOID]=sOrg
	pQry.close()
	return pRet

def fetchUserList():
	global pCfg
	pRet={}
	pCxn = dbConnect()
	pQry = pCxn.cursor()
	sQry = "select id,login from who where id > 0 order by id";
	pRows = None
	try:
		pQry.execute( sQry)
		pRows = pQry.fetchall()
	except Exception as pE1:
		print( "INF: no active starling users found" % pE1)
	for pRow in pRows:
		sUID = str(pRow[0])
		sUsr = str(pRow[1])
		pRet[sUID]=sUsr
	pQry.close()
	return pRet

def purgeUser( pUsr):
#	logMsg("purgeUser: "+pUsr['txt']+" ("+str(pUsr['id'])+")")
	lUsr = int(pUsr['id']) if 'id' in pUsr else 0
	sUsr = pUsr['txt'] if 'txt' in pUsr else '?'
	if lUsr > 0:
		pOrgs = fetchOrgList()
		for sOID in pOrgs:
			lOrg = int(sOID)
			sOrg = pOrgs[sOID]
			pRoster = rpcAPI({"cmd":"orgusrlst","org":lOrg})
			for sMID in pRoster['in']:
				lMbr = int(sMID)
				sMbr = pRoster['in'][sMID]
				if lMbr == lUsr:
					logMsg("removing "+sUsr+" from "+sOrg)
					rpcAPI({"cmd":"orgusrdel","org":lOrg,"usr":lUsr})
		logMsg("removing user "+sUsr+" ("+str(lUsr)+")")
		pCxn = dbConnect()
		try:
			pQry = pCxn.cursor()
			sQry = """update who set id=-id,status=-1,ptn=0,txt=concat(login,' ',ifnull(label,'NULL')),login=NULL,label=NULL where id=%s"""
			pQry.execute( sQry, (lUsr,))
			pQry.close()
			pCxn.commit()
		except Exception as pE1:
			logMsg("ERR: removal of user failed")
			logMsg(traceback.format_exc())
	else:
		logMsg("no valid user ID provided")

def purgeTenant( pTnt):
#	logMsg("purgeTenant: "+pTnt['txt']+" ("+str(pTnt['id'])+")")
	lOrg = int(pTnt['id']) if 'id' in pTnt else 0
	sOrg = pTnt['txt'] if 'txt' in pTnt else '?'
	if lOrg > 0:
		pRoster = rpcAPI({"cmd":"orgusrlst","org":lOrg})
		for sUID in pRoster['in']:
			logMsg("removing "+sUID+" from tenant space "+sOrg)
			rpcAPI({"cmd":"orgusrdel","org":lOrg,"usr":int(sUID)})
		logMsg("removing tenant space "+sOrg+" ("+str(lOrg)+")")
		pCxn = dbConnect()
		sQry = """update lut set lut=-lut,ptn=0,txt=concat(tag,'|',ifnull(label,'NULL')),tag='recycle',label='recycle' where lut=201 and idx=%s"""
		try:
			pQry = pCxn.cursor()
			pQry.execute( sQry, (lOrg,))
			pQry.close()
			pCxn.commit()
		except Exception as pE1:
			logMsg("ERR: removal of tenant space failed")
			logMsg(str(pE1))
	else:
		logMsg("no valid tenant space ID provided")

def manageTenant( pTnt):
	pass

class ConfirmDlg(npyscreen.ActionPopup):
	def create(self):
		self.bCancel = False
		self.pMsg = self.add(npyscreen.TitleFixedText,name="Are you sure ?",)
	def activate(self):
		self.bCancel = False
		self.edit()
		self.afterEditing()
	def on_cancel(self):
		self.bCancel = True
	def afterEditing(self):
		pCtx = self.parentApp.getCtx('c4m')
		if not self.bCancel:
			pOID = pCtx['oid']
			pFun = pCtx['fun']
			pFun(pOID)
			self.parentApp.setNextForm(pCtx['n4m'])
		else:
			self.parentApp.setNextForm(pCtx['p4m'])
		self.parentApp.setCtx('c4m',None)

class UserSel(npyscreen.MultiLineAction):
	def display_value( self, pObj):
		return(pObj['txt'])
	def actionHighlighted(self, act, key):
		pNfo = self.parent.parentApp.getCtx('c4m')
		pNfo['oid'] = act
		self.parent.parentApp.setNextForm('CONFIRMDLG')
		self.parent.exit_editing()

class SelectUserForm(npyscreen.Form):
	def create(self):
		self.pUsers = self.add( UserSel, name='Users', values = [])
	def activate(self):
		self.pUsers.values=[]
		pLst = fetchUserList()
		for sUID in pLst:
			self.pUsers.values.append({'id':int(sUID),'txt':pLst[sUID]})
		self.edit()
		self.afterEditing()
	def afterEditing(self):
		self.parentApp.setNextForm('CONFIRMDLG')

class TenantSel(npyscreen.MultiLineAction):
	def display_value( self, pObj):
		return(pObj['txt'])
	def actionHighlighted(self, act, key):
		pNfo=self.parent.parentApp.getCtx('c4m')
		pNfo['oid']=act
		self.parent.exit_editing()

class SelectTenantForm(npyscreen.Form):
	def create(self):
		self.pOrgs = self.add( TenantSel, name='Tenants', values = [])
	def activate(self):
		self.pOrgs.values=[]
		pLst = fetchOrgList()
		for sOID in pLst:
			self.pOrgs.values.append({'id':int(sOID),'txt':pLst[sOID]})
		self.edit()
		self.afterEditing()
	def afterEditing(self):
		pNfo = self.parentApp.getCtx('c4m')
		pFn = pNfo['fun']
		sNext = pNfo['n4m'] if pFn == None else 'CONFIRMDLG'
		self.parentApp.setNextForm(sNext)

class LoginForm(npyscreen.ActionPopup):
	def create(self):
		self.bCancel = False
		self.pLogin  = self.add(npyscreen.TitleText, name = "Login:",)
		self.pPassword  = self.add(npyscreen.TitlePassword, name = "Password:",)
	def on_cancel(self):
		self.bCancel = True
	def afterEditing(self):
		global pCfg
		sNext=None
		if not self.bCancel:
			rpcAuth( self.pLogin.value, self.pPassword.value)
			sNext = 'MENUMAIN' if pCfg['tkn'] != None else 'MAIN'
		self.parentApp.setNextForm(sNext)

class MemberMenu(npyscreen.MultiLineAction):
	def actionHighlighted(self, act, key):
		if str(act) == 'Back':
			self.parent.parentApp.setNextForm('MENUMAIN')
			self.parent.exit_editing()
		elif str(act) == 'Manage Tenant Space Membership':
			pNfo={'fun':manageTenant,'oid':None,'p4m':'MENUMAIN','n4m':'MEMBERS'}
			self.parent.parentApp.setCtx('c4m',pNfo)
			self.parent.parentApp.setNextForm('TNTSEL')
			self.parent.exit_editing()
		else:
			logMsg("MemberMenu::actionHighlighted - "+str(act))

class MemberSel(npyscreen.MultiLineAction):
	def display_value( self, pObj):
		return(pObj['txt'])
	def actionHighlighted(self, act, key):
		bIn = False if self.name == 'In:' else True
		self.parent.switch(act,bIn)

class MemberBox(npyscreen.BoxTitle):
	_contained_widget=MemberSel

class MemberForm(npyscreen.Form):
	def create(self):
		iW = (self.columns / 2) - 4
		iX = (iW + 3) * -1
		self.pOut = self.add(MemberBox,width=iW,max_width=iW,values=[],
									name="Out:",contained_widget_arguments={'name':"Out:"})
		self.pIn = self.add(MemberBox,width=iW,max_width=iW,relx=iX,rely=2,values=[],
									name="In:",contained_widget_arguments={'name':"In:"})
	def activate(self):
		pNfo = self.parentApp.getCtx('c4m')
		self.lTNT = pNfo['oid']['id']
		self.sTNT = pNfo['oid']['txt']
		self.pAdd = {}
		self.pRmv = {}
		self.pTNT = rpcAPI({"cmd":"orgusrlst","org":self.lTNT})
		self.pIn.values=[]
		for sUID in self.pTNT['in']:
			self.pIn.values.append({"id":int(sUID),"txt":self.pTNT['in'][sUID]})
		self.pOut.values=[]
		for sUID in self.pTNT['out']:
			self.pOut.values.append({"id":int(sUID),"txt":self.pTNT['out'][sUID]})
		self.edit()
		self.afterEditing()
	def afterEditing(self):
		self.parentApp.setNextForm('MENUMAIN')
		for sUsr in self.pAdd:
			if not sUsr in self.pTNT['in']:
				logMsg("ADD "+str(self.pAdd[sUsr])+" ("+sUsr+") to TNT "+self.sTNT)
				rpcAPI({"cmd":"orgusradd","org":self.lTNT,"usr":int(sUsr),"axs":"adm"})
		for sUsr in self.pRmv:
			if not sUsr in self.pTNT['out']:
				logMsg("DEL "+str(self.pRmv[sUsr])+" ("+sUsr+") from TNT "+self.sTNT)
				rpcAPI({"cmd":"orgusrdel","org":self.lTNT,"usr":int(sUsr)})
	def switch(self,pUsr,bIn):
		lUsr = pUsr['id']
		sUID = str(lUsr)
		sUsr = pUsr['txt']
		pItm = {"id":lUsr,"txt":sUsr}
		if bIn:
			self.pIn.values.append(pItm)
			self.pOut.values.remove(pItm)
			self.pAdd[str(lUsr)]=pItm
			if sUID in self.pRmv:
				del self.pRmv[sUID]
		else:
			self.pOut.values.append(pItm)
			self.pIn.values.remove(pItm)
			self.pRmv[str(lUsr)]=pItm
			if sUID in self.pAdd:
				del self.pAdd[sUID]
		self.pIn.display()
		self.pOut.display()

class MainMenu(npyscreen.MultiLineAction):
	def actionHighlighted(self, act, key):
		if str(act) == 'Exit':
			self.parent.parentApp.setNextForm(None)
			self.parent.exit_editing()
		elif str(act) == 'Purge User':
			pNfo={'fun':purgeUser,'oid':None,'p4m':'MENUMAIN','n4m':'MENUMAIN'}
			self.parent.parentApp.setCtx('c4m',pNfo)
			self.parent.parentApp.setNextForm('USERSEL')
			self.parent.exit_editing()
		elif str(act) == 'Purge Tenant Space':
			pNfo={'fun':purgeTenant,'oid':None,'p4m':'MENUMAIN','n4m':'MENUMAIN'}
			self.parent.parentApp.setCtx('c4m',pNfo)
			self.parent.parentApp.setNextForm('TNTSEL')
			self.parent.exit_editing()
		elif str(act) == 'Manage Tenant Membership':
			pNfo={'fun':None,'oid':None,'p4m':'MENUMAIN','n4m':'MEMBERS'}
			self.parent.parentApp.setCtx('c4m',pNfo)
			self.parent.parentApp.setNextForm('TNTSEL')
			self.parent.exit_editing()
		else:
			logMsg("MainMenu::actionHighlighted - "+str(act))

class MainForm(npyscreen.FormBaseNew):
	def create(self):
		self.pValues=[
		#	"Create Tenant Space",
		#	"Create User",
		#	"Create Application Profile",
			"Manage Tenant Membership",
			"Purge User",
			"Purge Tenant Space",
			"Exit"
		]
		self.pSel = self.add( MainMenu, values=self.pValues)
	def pre_edit_loop(self):
		pass
	def post_edit_loop(self):
		pass

class App(npyscreen.NPSAppManaged):
	def onStart(self):
		self.pCtx={}
		self.addForm( 'MAIN', LoginForm, name='Starling Administration')
		self.addForm( 'MENUMAIN', MainForm, name='Starling Administration')
		self.addForm( 'USERSEL', SelectUserForm, name='Starling Administration')
		self.addForm( 'CONFIRMDLG', ConfirmDlg, name='Starling Administration')
		self.addForm( 'TNTSEL', SelectTenantForm, name='Starling Administration')
		self.addForm( 'MEMBERS', MemberForm, name='Starling Administration')
	def getCtx( self, sCtx):
		return self.pCtx[sCtx] if sCtx in self.pCtx else None
	def setCtx( self, sCtx, pVal):
		self.pCtx[sCtx]=pVal

if __name__ == '__main__':
	sCmd="status"
	loadConfig()
	sJob = ""
	sRPC = ""
#	Command-line argument processing ...
	for arg in sys.argv:
		pair = arg.split( "=", 1)
		if pair[0] == "--launch":
			sCmd="launch"
			sJob=pair[1]
		elif pair[0] == "--something":
			sCmd="something"
			sRPC=pair[1]
	App().run()
