<?php
require_once( __DIR__."/etc/private/localcfg.php");
$sProto="https";

function genPortalView() {
	$sRet = "<html>\n";
	$sRet .= "<head><meta http-equiv=\"Refresh\" content=\"0; url=portal.html\"/></head>\n";
	$sRet .= "<body><br></body>\n";
	$sRet .= "</html>\n";
	return( $sRet);
}

$sCtx="portal";
if ( !isset($_POST['ctx'])) {
	if ( isset($_GET['ctx'])) $sCtx=$_GET['ctx'];
} else $sCtx=$_POST['ctx'];
if ( $sCtx != "portal") {
	require_once( "lib/rig.php");
	showPage();
} else printf( "%s", genPortalView());
?>
