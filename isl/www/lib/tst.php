<?php
require_once( __DIR__."/../etc/cfg.php");
require_once( __DIR__."/../lib/stored.php");

class Tst extends Stored {
	var $sFilterUsr;

	function Tst() {
		$this->Stored();
		$this->sPrd = "starling";
		$this->sTable = "tst";
		$this->sFilterUsr = "";
		$this->addField( "id", "int", "ID", 1000, "tst_id");
		$this->addField( "created", "date", "Created", 1000, "tst_created");
		$this->addField( "chg", "date", "Changed", 1000, "tst_chg");
		$this->addField( "eid", "int", "EID", 1000, "tst_eid");
		$this->addField( "app", "int", "App", 1000, "tst_app");
		$this->addField( "pln", "int", "Plan", 0, "tst_pln");
		$this->addField( "who", "int", "User", 0, "tst_who");
		$this->addField( "status", "int", "Status", 0, "tst_status");
		$this->addField( "job", "int", "Job ID", 0, "tst_job");
		$this->addField( "api", "str", "API", 0, "tst_api");
		$this->addField( "name", "str", "Name", 0, "tst_name");
		$this->addField( "rpturl", "str", "Report URL", 0, "tst_rpturl");
		$this->addField( "ipg", "int", "Page Count", 0, "tst_ipg");
		$this->addField( "xpg", "str", "Console Size", 0, "tst_xpg");
		$this->addField( "nfo", "txt", "Info", 0, "tst_nfo");
		$this->addField( "xqt", "txt", "Execution Attributes", 0, "tst_xqt");
		$this->addField( "txt", "txt", "Brief", 0, "tst_txt");
		return;
	}

	function creationClause() { return( "(created) values (now())"); }

	function getHeaderField() { return( 'name'); }

	function getDisplayedFields() {
		$pRet = array(	0 => 'id',
							1 => 'name',
							2 => 'status',
							3 => 'rpturl',
							4 => 'txt' );
		return( $pRet);
	}

	function getClass( $sCls) { return( ""); }

	function getBackLink() {
		$sRet =	"<a href=\"index.php?ctx=tst&cmd=qry&".
					"tst_id=".$this->lID."\">Back ...</a>";
		return( $sRet);
	}

	function getSaveLink() {
		$sRet = "<input type=submit value=Save>";
		return( $sRet);
	}

	function findByEID( $sTnt, $lEID) {
		global $pCfg;
		$lRet=0;
		$sWhere = "a.tnt='".$sTnt."' and t.eid=".$lEID;
		$sQry = "select t.id from ".$this->sTable." t join app a on a.id=t.app where ".$sWhere." order by t.id";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$lRet = intval($pRow['id']);
		}
		return( $lRet);
	}

	function findByApp( $lApp, $bActive) {
		global $sRigWWW;
		global $pCfg;
		$pRet = array();
		$sWhere = $bActive ? "and status > 1" : "and status in (-1,0)";
		$sQry =	"select id,name from ".$this->sTable." where app=".$lApp." ".$sWhere." order by created desc limit 0,60";
	//	error_log( "DBG=>>> findByApp query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$pRet[$pRow['id']] = "".$pRow['name'];
		}
		mysqli_free_result( $pRslt);
		return( $pRet);
	}

	function findActive() {
		global $sRigWWW;
		global $pCfg;
		$pRet = array();
		$sWhere = "a.id > 0 and t.status > 1";
		$sQry =	"select distinct(a.id),a.name from app a join ".$this->sTable." t on a.id=t.app where ".$sWhere." order by a.name";
	//	error_log( "DBG=>>> findActive query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRslt != null) {
			while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
				$pRet[$pRow['id']] = "".$pRow['name'];
			}
			mysqli_free_result( $pRslt);
		}
		return( $pRet);
	}

	function getCreated() { return( $this->pFlds['created']['val']); }

	function getEID() { return( $this->pFlds['eid']['val']); }
	function setEID( $lEID) {
		$this->pFlds['eid']['val'] = $lEID;
		return;
	}

	function getApp() { return( $this->pFlds['app']['val']); }
	function setApp( $lApp) {
		$this->pFlds['app']['val'] = $lApp;
		return;
	}

	function getPlan() { return( $this->pFlds['pln']['val']); }
	function setPlan( $lPlan) {
		$this->pFlds['pln']['val'] = $lPlan;
		return;
	}

	function getWho() { return( $this->pFlds['who']['val']); }
	function setWho( $lWho) {
		$this->pFlds['who']['val'] = $lWho;
		return;
	}

	function getName() { return( $this->pFlds['name']['val']); }
	function setName( $sTxt) {
		$this->pFlds['name']['val'] = $sTxt;
		return;
	}

	function getStatus() { return( $this->pFlds['status']['val']); }
	function setStatus( $iStatus) {
		$this->pFlds['status']['val'] = $iStatus;
		return;
	}

	function getJobName() {
		global $pCfg;
		$sRet = "";
		$sQry = "select job from app where id=".$this->pFlds['app']['val'];
//		echo "<br>Query:<br>".$sQry."<br><br>\n";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$sRet = "".$pRow['job'];
		}
		mysqli_free_result( $pRslt);
		return( $sRet);
	}

	function getJob() { return( $this->pFlds['job']['val']); }
	function setJob( $iJob) {
		$this->pFlds['job']['val'] = $iJob;
		return;
	}

	function getApi() { return( $this->pFlds['api']['val']); }
	function setApi( $sAPI) {
		$this->pFlds['api']['val'] = $sAPI;
		return;
	}

	function getURL() { return( $this->pFlds['rpturl']['val']); }
	function setURL( $sURL) {
		$this->pFlds['rpturl']['val'] = $sURL;
		return;
	}

	function getPageCount() { return( $this->pFlds['ipg']['val']); }
	function setPageCount( $iPg) {
		$this->pFlds['ipg']['val'] = $iPg;
	}

	function getConsoleSize() { return( $this->pFlds['xpg']['val']); }

	function addPage( $sTxt) {
		global $pCfg;
		if ( $this->lID > 0) {
			$iPg = intval($this->pFlds['ipg']['val']);
			$sQry = sprintf( "insert into txt (chg,oid,idx,txt) values (now(),%d,%d,'%s')", $this->lID, $iPg, $sTxt);
		//	error_log( "DBG=>>> addPage query: ".$sQry);
			mysqli_query( $pCfg['dbcxn'], $sQry);
			$iRet = $iPg + 1;
			$this->pFlds['ipg']['val'] = $iRet;
			$this->pFlds['xpg']['val'] += strlen( $sTxt);
			$this->commit();
		}
		return( $iRet);
	}

	function getPage( $iPage=-1) {
		global $pCfg;
		$sRet = "";
		$iCPg = intval($this->pFlds['ipg']['val']);
		if ( $iCPg > 0) {
			$iPg = ($iPage < 0) ? $iCPg-1 : $iPage;
			$sQry = sprintf( "select txt from txt where ref=0 and oid=%d and idx=%d", $this->lID, $iPg);
			$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
			if ( $pRow = mysqli_fetch_assoc( $pRslt)) { $sRet = $pRow['txt']; }
			mysqli_free_result( $pRslt);
		}
		return( $sRet);
	}

	function getVUsers() { return( json_decode( $this->pFlds['nfo']['val'], true)['vusr']); }
	function setVUsers( $lVUsers) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['vusr'] = $lVUsers;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getLGs() { return( json_decode( $this->pFlds['nfo']['val'], true)['lgs']); }
	function setLGs( $iLGs) {
		$pNfo = json_decode( $this->pFlds['nfo']['val']);
		$pNfo['lgs'] = $iLGs;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getCores() {
		$sRet="1";
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['cpu'])) {
				$sRet = $pNfo['cpu'];
			}
		}
		return( $sRet);
	}

	function setCores( $sCPU) {
		$pNfo = json_decode( $this->pFlds['nfo']['val']);
		$pNfo['cpu'] = $sCPU;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getMMX() {
		$iRet=3072;
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['mmx'])) $iRet = $pNfo['mmx'];
		}
		return( $iRet);
	}

	function setMMX( $iRAM) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['mmx'] = $iRAM;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getMMS() {
		$iRet=1024;
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['mms'])) $iRet = $pNfo['mms'];
		}
		return( $iRet);
	}

	function setMMS( $iRAM) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['mms'] = $iRAM;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getNfo() { return( $this->pFlds['nfo']['val']); }
	function setNfo( $sNfo) {
		$this->pFlds['nfo']['val'] = $sNfo;
		return;
	}

	function getExe() {
		$pRet = array();
		$sExe = isset($this->pFlds['xqt']['val']) ? $this->pFlds['xqt']['val'] : '';
		if ( $sExe !== '') $pRet = json_decode( $sExe, true);
		return( $pRet);
	}

	function setExe( $pNewExe) {
		$pExe = ($pNewExe !== null) ? $pNewExe : array();
		$this->pFlds['xqt']['val'] = json_encode( $pExe);
		return;
	}

	function getBrief() { return( $this->pFlds['txt']['val']); }
	function setBrief( $sTxt) {
		$this->pFlds['txt']['val'] = $sTxt;
		return;
	}

	function setCompletion() {
		$this->pFlds['chg']['val'] = "now()";
		return;
	}

	function genRpt() {
		global $pCfg;
		global $sLogin;
		$pRet = array();
		$sFlds = "t.id as testid, a.label as appname, t.status, p.tnt as tenant, a.ask as askid, a.cmdb as ci, a.fin as glcode, p.ngn as fabric, p.frm as framework, p.git, p.doc, convert_tz(t.created,'America/Chicago','UTC') as start";
		$sTbls = $this->sTable." t join pln p on p.id=t.pln join app a on a.id=p.app";
		$sQry = sprintf( "select %s from %s where t.id=%d", $sFlds, $sTbls, $this->lID);
	//	error_log( "DBG=>>> tst.genRpt query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			foreach ( $pRow as $sFld => $pVal) { $pRet[$sFld] = $pVal; }
			$lUsr = $this->getWho();
			if ( $lUsr > 0) {
				require_once( $pCfg['path_www']."/lib/who.php");
				$pWho = new Who();
				$pRet['msid'] = $pWho->mapUID($lUsr);
			} else $pRet['msid']=$sLogin;
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return( $pRet);
	}

	function markDeleted() {
		$lAFK = $this->getApp();
		if ( $lAFK > 0) {
			$this->setApp( 0 - $lAFK);
			$this->commit();
		}
		return;
	}
}
?>
