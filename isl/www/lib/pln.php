<?php
require_once( __DIR__."/../etc/cfg.php");
require_once( __DIR__."/../lib/stored.php");

class Pln extends Stored {
	var $sFilterUsr;

	function Pln() {
		$this->Stored();
		$this->sPrd = "starling";
		$this->sTable = "pln";
		$this->sFilterUsr = "";
		$this->addField( "id", "int", "ID", 1000, "pln_id");
		$this->addField( "created", "date", "Created", 1000, "pln_created");
		$this->addField( "eid", "int", "External ID", 1000, "pln_eid");
		$this->addField( "chg", "date", "Changed", 1000, "pln_chg");
		$this->addField( "eid", "int", "EID", 1000, "pln_eid");
		$this->addField( "app", "int", "App", 1000, "pln_app");
		$this->addField( "ngn", "str", "Engine", 0, "pln_ngn");
		$this->addField( "frm", "str", "Framework", 0, "pln_frm");
		$this->addField( "git", "str", "Git Repository", 0, "pln_git");
		$this->addField( "gbn", "str", "Git Branch Name", 0, "pln_gbn");
		$this->addField( "grd", "str", "Git Reference Dir", 0, "pln_grd");
		$this->addField( "tnt", "str", "Tenant", 0, "pln_tnt");
		$this->addField( "name", "str", "Name", 0, "pln_name");
		$this->addField( "doc", "str", "Document", 0, "pln_doc");
		$this->addField( "vis", "str", "Visualization", 0, "pln_vis");
		$this->addField( "nfo", "txt", "Info", 0, "pln_nfo");
		$this->addField( "xqt", "txt", "Execution Attributes", 0, "pln_xqt");
		$this->addField( "txt", "txt", "Brief", 0, "pln_txt");
		return;
	}

	function creationClause() { return( "(created) values (now())"); }

	function getHeaderField() { return( 'name'); }

	function getDisplayedFields() {
		$pRet = array(	0 => 'id',
							1 => 'name',
							2 => 'doc',
							3 => 'txt' );
		return( $pRet);
	}

	function getClass( $sCls) { return( ""); }

	function getBackLink() {
		$sRet =	"<a href=\"index.php?ctx=pln&cmd=qry&".
					"pln_id=".$this->lID."\">Back ...</a>";
		return( $sRet);
	}

	function getSaveLink() {
		$sRet = "<input type=submit value=Save>";
		return( $sRet);
	}

	function findByEID( $sTnt, $lEID) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/dbcxn.php");
		$lRet=0;
		$sWhere = "a.tnt='".$sTnt."' and p.eid=".$lEID;
		$sQry = "select p.id from ".$this->sTable." p join app a on a.id=p.app where ".$sWhere." order by p.id";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRslt != null) {
			if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
				$lRet = intval($pRow['id']);
			}
		}
		return( $lRet);
	}

	function findByApp( $lApp, $bActive=false) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/dbcxn.php");
		$pRet = array();
		$sWhere = "id > 0 and app=".$lApp;
		$sQry =	"select id,name,doc from ".$this->sTable." where ".$sWhere." order by created desc";
	//	error_log( "DBG=>>> plan query: ".$sQry);
		if ( $pRslt = mysqli_query( $pCfg['dbcxn'], $sQry)) {
			while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
				$pRet[$pRow['id']] = "".$pRow['name']." (".$pRow['id']."): ".$pRow['doc'];
			}
			mysqli_free_result( $pRslt);
		}
		return( $pRet);
	}

	function getCreated() { return( $this->pFlds['created']['val']); }

	function getEID() { return( $this->pFlds['eid']['val']); }
	function setEID( $lEID) {
		$this->pFlds['eid']['val'] = $lEID;
		return;
	}

	function getApp() { return( $this->pFlds['app']['val']); }
	function setApp( $lApp) {
		$this->pFlds['app']['val'] = $lApp;
		return;
	}

	function getEngine() { return( $this->pFlds['ngn']['val']); }
	function setEngine( $sTxt) {
		$this->pFlds['ngn']['val'] = $sTxt;
		return;
	}

	function getFramework() { return( $this->pFlds['frm']['val']); }
	function setFramework( $sTxt) {
		$this->pFlds['frm']['val'] = $sTxt;
		return;
	}

	function getRepository() { return( $this->pFlds['git']['val']); }
	function setRepository( $sTxt) {
		$this->pFlds['git']['val'] = $sTxt;
		return;
	}

	function getBranch() { return( $this->pFlds['gbn']['val']); }
	function setBranch( $sTxt) {
		$this->pFlds['gbn']['val'] = $sTxt;
		return;
	}

	function getRefDir() { return( $this->pFlds['grd']['val']); }
	function setRefDir( $sTxt) {
		$this->pFlds['grd']['val'] = $sTxt;
		return;
	}

	function getTenant() { return( $this->pFlds['tnt']['val']); }
	function setTenant( $sTxt) {
		$this->pFlds['tnt']['val'] = $sTxt;
		return;
	}

	function mapName( $lPlanID=0) {
		global $pCfg;
		$sRet = ($this->lID > 0) ? $this->pFlds['name']['val'] : '';
		if ( $lPlanID > 0) {
			require_once( $pCfg['path_www']."/lib/dbcxn.php");
			$sQry = "select name from ".$this->sTable." where id=".$lPlanID;
			$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
			if ( $pRslt != null) {
				if ( $pRow = mysqli_fetch_assoc( $pRslt)) $sRet = $pRow['name'];
				mysqli_free_result( $pRslt);
			}
		}
		return( $sRet);
	}
	function getName() { return( $this->pFlds['name']['val']); }
	function setName( $sTxt) {
		$this->pFlds['name']['val'] = $sTxt;
		return;
	}

	function getDoc() { return( $this->pFlds['doc']['val']); }
	function setDoc( $sURL) {
		$this->pFlds['doc']['val'] = $sURL;
		return;
	}

	function getVis() { return( $this->pFlds['vis']['val']); }
	function setVis( $sVis) {
		$this->pFlds['vis']['val'] = $sVis;
		return;
	}

	function getVUsers() {
		$lRet=1;
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['vusr'])) $lRet = $pNfo['vusr'];
		}
		return( $lRet);
	}

	function setVUsers( $lVUsers) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['vusr'] = $lVUsers;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getLGs() {
		$iRet=1;
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['lgs'])) $iRet = $pNfo['lgs'];
		}
		return( $iRet);
	}

	function setLGs( $iLGs) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['lgs'] = $iLGs;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getDuration() {
		$sRet='';
		$pXqt = json_decode( $this->pFlds['xqt']['val'], true);
		if ( $pXqt != null) {
			if ( isset( $pXqt['duration'])) $sRet = $pXqt['duration'];
		}
		return( $sRet);
	}

	function setDuration( $sDuration) {
		$pXqt = json_decode( $this->pFlds['xqt']['val'], true);
		$pXqt['duration'] = $sDuration;
		$this->pFlds['xqt']['val'] = json_encode( $pXqt);
		return;
	}

	function getRampUpInterval() {
		$sRet='';
		$pXqt = json_decode( $this->pFlds['xqt']['val'], true);
		if ( $pXqt != null) {
			if ( isset( $pXqt['rui'])) $sRet = $pXqt['rui'];
		}
		return( $sRet);
	}

	function setRampUpInterval( $sRUI) {
		$pXqt = json_decode( $this->pFlds['xqt']['val'], true);
		$pXqt['rui'] = $sRUI;
		$this->pFlds['xqt']['val'] = json_encode( $pXqt);
		return;
	}

	function getCores() {
		$sRet="1";
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['cpu'])) {
				$sRet = $pNfo['cpu'];
			}
		}
		return( $sRet);
	}

	function setCores( $sCPU) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['cpu'] = $sCPU;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getMMX() {
		$iRet=3072;
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['mmx'])) $iRet = $pNfo['mmx'];
		}
		return( $iRet);
	}

	function setMMX( $iRAM) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['mmx'] = $iRAM;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getMMS() {
		$iRet=1024;
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		if ( $pNfo != null) {
			if ( isset( $pNfo['mms'])) $iRet = $pNfo['mms'];
		}
		return( $iRet);
	}

	function setMMS( $iRAM) {
		$pNfo = json_decode( $this->pFlds['nfo']['val'], true);
		$pNfo['mms'] = $iRAM;
		$this->pFlds['nfo']['val'] = json_encode( $pNfo);
		return;
	}

	function getNfo() { return( $this->pFlds['nfo']['val']); }
	function setNfo( $sNfo) {
		$this->pFlds['nfo']['val'] = $sNfo;
		return;
	}

	function getExe() {
		$pRet = array();
		$sExe = isset($this->pFlds['xqt']['val']) ? $this->pFlds['xqt']['val'] : '';
		if ( $sExe !== '') $pRet = json_decode( $sExe, true);
		return( $pRet);
	}

	function setExe( $pNewExe) {
		$pExe = ($pNewExe !== null) ? $pNewExe : array();
		$this->pFlds['xqt']['val'] = json_encode( $pExe);
		return;
	}

	function getBrief() { return( $this->pFlds['txt']['val']); }
	function setBrief( $sTxt) {
		$this->pFlds['txt']['val'] = $sTxt;
		return;
	}

	function getSyn() {
		$sRet = ($this->getEngine() == 'garden') ? 'healthcheck' : '';
		return( $sRet);
	}
	
	function setSyn( $sVal) {
		$sNgn = $this->getEngine();
		if ( $sVal == 'healthcheck') {
			if ( $sNgn != 'garden') $this->setEngine('garden');
		} else if ( $sNgn == 'garden') $this->setEngine('k8s');
		return;
	}
}
?>
