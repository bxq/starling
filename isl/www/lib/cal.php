<?php
$sBaseInc = isset($sPageBaseLoc)?$sPageBaseLoc:dirname($_SERVER['SCRIPT_FILENAME']);
require_once( $sBaseInc."/etc/cfg.php");
require_once( $pCfg['path_www']."/lib/stored.php");

class Cal extends Stored {
	var $sFilterUsr;

	function Cal() {
		$this->Stored();
		$this->sPrd = "starling";
		$this->sTable = "cal";
		$this->sFilterUsr = "";
		$this->addField( "id", "int", "ID", 1000, "cal_id");
		$this->addField( "created", "date", "Created", 1000, "cal_created");
		$this->addField( "chg", "date", "Changed", 1000, "cal_chg");
		$this->addField( "who", "int", "Who", 0, "cal_who");
		$this->addField( "pln", "int", "Plan", 0, "cal_pln");
		$this->addField( "tst", "int", "Test", 0, "cal_tst");
		$this->addField( "wip", "int", "WIP", 0, "cal_wip");
		$this->addField( "cts", "str", "Crontab Specification", 0, "cal_cts");
		$this->addField( "top", "date", "Start", 0, "cal_top");
		$this->addField( "btm", "date", "End", 0, "cal_btm");
		$this->addField( "name", "str", "Name", 0, "cal_name");
		$this->addField( "email", "str", "EMail", 0, "cal_email");
		$this->addField( "txt", "txt", "Brief", 0, "cal_txt");
		return;
	}

	function creationClause() {
		return( "(created) values (now())");
	}

	function getHeaderField() {
		return( 'name');
	}

	function getDisplayedFields() {
		$pRet = array(	0 => 'id',
							1 => 'name',
							2 => 'top',
							3 => 'btm',
							4 => 'txt' );
		return( $pRet);
	}

	function getClass( $sCls) {
		$sRet = "";
		switch ( $sCls) {
			default:
				$sRet = "";
				break;
		};
		return( $sRet);
	}

	function getBackLink() {
		$sRet =	"<a href=\"index.php?ctx=cal&cmd=qry&".
					"cal_id=".$this->lID."\">Back ...</a>";
		return( $sRet);
	}

	function getSaveLink() {
		$sRet = "<input type=submit value=Save>";
		return( $sRet);
	}

	function findByStatus( $sCode='healthcheck') {
		global $pCfg;
		global $sRigWWW;
	#	$bActive = ($sCode == 'active') ? true : false;
		$bActive = true;
		require_once( $sRigWWW."lib/dbcxn.php");
		$pRet = array();
		$sWhere  = $bActive ? "and e.wip > -1" : "and e.wip < 0";
		$sWhere .= " and p.ngn='garden'";
		$sQry =	"select e.id,a.cmdb from ".$this->sTable." e join pln p on p.id=e.pln join app a on a.id=p.app where e.id > 0 ".$sWhere." order by e.created desc";
	//	error_log( "DBG=>>> findByStatus query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( ($pRslt != null) && ($pRow = mysqli_fetch_assoc( $pRslt))) {
		//	error_log( "DBG=>>> findByStatus result: ".$pRow['cmdb']." (".$pRow['id'].")");
			$pRet[$pRow['id']] = "".$pRow['cmdb'];
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return( $pRet);
	}

	function findByApp( $lApp, $bActive, $bPast=false) {
		global $pCfg;
		global $sRigWWW;
		require_once( $sRigWWW."lib/dbcxn.php");
		$pRet = array();
		$sWhere = $bActive ? "and e.wip > -1" : "and e.wip < 0";
		$sWhere .= $bPast ? " and e.btm < now()" : "";
		$sQry =	"select e.id,p.id as plnid,e.name from ".$this->sTable." e join pln p on p.id=e.pln where e.id > 0 and p.app=".$lApp." ".$sWhere." order by e.created desc";
	//	error_log( "DBG=>>> findByApp query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( ($pRslt != null) && ($pRow = mysqli_fetch_assoc( $pRslt))) {
		//	error_log( "DBG=>>> findByApp result: ".$pRow['name']." (".$pRow['id'].")");
			$pRet[$pRow['id']] = $pRow['name'];
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return( $pRet);
	}

	function getCreated() { return( $this->pFlds['created']['val']); }

	function getChanged() { return( $this->pFlds['chg']['val']); }

	function getWho() { return( $this->pFlds['who']['val']); }
	function setWho( $lWho) {
		$this->pFlds['who']['val'] = $lWho;
		return;
	}

	function getPlan() { return( $this->pFlds['pln']['val']); }
	function setPlan( $lNewPlan) {
		$this->pFlds['pln']['val'] = $lNewPlan;
		return;
	}

	function getWIP() { return( $this->pFlds['wip']['val']); }
	function setWIP( $lWIP) {
		$this->pFlds['wip']['val'] = $lWIP;
		return;
	}

	function getTest() { return( $this->pFlds['tst']['val']); }
	function setTest( $lTst) {
		$this->pFlds['tst']['val'] = $lTst;
		return;
	}

	function getCron() { return( $this->pFlds['cts']['val']); }
	function setCron( $sCTS) {
		$this->pFlds['cts']['val'] = $sCTS;
		return;
	}

	function getStart() { return( $this->pFlds['top']['val']); }
	function setStart( $sWhen) {
		$this->pFlds['top']['val'] = $sWhen;
		return;
	}

	function getEnd() { return( $this->pFlds['btm']['val']); }
	function setEnd( $sWhen) {
		$this->pFlds['btm']['val'] = $sWhen;
		return;
	}

	function getName() { return( $this->pFlds['name']['val']); }
	function setName( $sTxt) {
		$this->pFlds['name']['val'] = $sTxt;
		return;
	}

	function getEMail() { return( $this->pFlds['email']['val']); }
	function setEMail( $sTxt) {
		$this->pFlds['email']['val'] = $sTxt;
		return;
	}

	function getJobName() {
		global $pCfg;
		global $sRigWWW;
		require_once( $sRigWWW."lib/dbcxn.php");
		$sRet = "";
		$sQry = "select job from app where id=".$this->pFlds['app']['val'];
	//	error_log( "DBG=>>> getJobName query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$sRet = "".$pRow['job'];
		}
		mysqli_free_result( $pRslt);
		return( $sRet);
	}

	function getBrief() { return( $this->pFlds['txt']['val']); }
	function setBrief( $sTxt) {
		$this->pFlds['txt']['val'] = $sTxt;
		return;
	}

	function mapSyn() {
		global $pCfg;
		global $sRigWWW;
		require_once( $sRigWWW."lib/dbcxn.php");
		$sRet='';
		$sQry = "select ngn from pln where id=".$this->pFlds['pln']['val'];
	//	error_log( "DBG=>>> mapFabric query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRslt !== null) {
			$sNgn='';
			if ( $pRow = mysqli_fetch_assoc( $pRslt)) $sNgn = $pRow['ngn'];
			mysqli_free_result( $pRslt);
			if ( $sNgn == 'garden') $sRet='healthcheck';
		}
		return( $sRet);
	}

	function mapFabric() {
		global $pCfg;
		global $sRigWWW;
		require_once( $sRigWWW."lib/dbcxn.php");
		$sRet='garden';
		$sQry = "select ngn from pln where id=".$this->pFlds['pln']['val'];
	//	error_log( "DBG=>>> mapFabric query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRslt !== null) {
			if ( $pRow = mysqli_fetch_assoc( $pRslt)) $sRet = $pRow['ngn'];
			mysqli_free_result( $pRslt);
		}
		return( $sRet);
	}

	function mapCI() {
		global $pCfg;
		global $sRigWWW;
		require_once( $sRigWWW."lib/dbcxn.php");
		$sRet='starlingsynth';
		$sQry = "select cmdb from app a join pln p on a.id=p.app where p.id=".$this->pFlds['pln']['val'];
	//	error_log( "DBG=>>> mapCI query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRslt !== null) {
			if ( $pRow = mysqli_fetch_assoc( $pRslt)) $sRet = $pRow['cmdb'];
			mysqli_free_result( $pRslt);
		}
		return( $sRet);
	}

	function triggerCalSync() {
		global $pCfg;
		if ( $this->lID > 0) {
			$sURL = $pCfg['url_cal'];
			$pCxn = curl_init($sURL);
			$pReq = array( 'cmd' => 'job', 'job' => $this->lID, 'pub' => true);
			$pJSON = array( 'ctx' => 'api', 'req' => $pReq);
			$sJSON = json_encode( $pJSON);
		#	curl_setopt( $pCxn, CURLOPT_POST, 1);
			curl_setopt( $pCxn, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt( $pCxn, CURLOPT_POSTFIELDS, $sJSON);
			curl_setopt( $pCxn, CURLOPT_RETURNTRANSFER, true);
			curl_setopt( $pCxn, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: '.strlen($sJSON))
			);
			$result = curl_exec( $pCxn);
		//	Bump ATF Broker ...
			$sCmd = sprintf( "sudo -H -u %s %s/islctl atff", $pCfg['unix_login'], $pCfg['unix_path']);
		//	error_log( "DBG=>>> bumping ATF Broker with -> ".$sCmd);
			$sCSV = shell_exec( $sCmd);
		}
		return;
	}

	function schedule( $sStart, $sEnd) {
	//	error_log( "DBG=>>> Cal schedule: ".$sStart." ... ".$sEnd);
		$this->setStart( $sStart);
		$this->setEnd( $sEnd);
		$this->commit();
		return;
	}

	function chgEvent( $pWhen, $bToggle=null) {
		$lRet=0;
		$bOK=false;
	//	error_log( "DBG=>>> chgCalEvt ...");
		$lPln = $this->getPlan();
		if ( $this->pFlds['email']['val'] != $this->pFlds['email']['old']) $bOK=true;
		if ( $this->pFlds['pln']['val'] != $this->pFlds['pln']['old']) $bOK=true;
		if ( $bToggle !== null) {
			$bOK=true;
			$lWIP = $bToggle ? 0 : -1;
		#	error_log( "DBG=>>> changing WIP to ".$lWIP);
			$this->setWIP( $lWIP);
		} # else error_log( "DBG=>>> WIP unchanged");
		if ( ($lPln > 0) && ($pWhen != null)) {
			$bOK=true;
		#	$iYr = isset( $pWhen['yr']) ? $pWhen['yr'] : 0;
		#	$iMo = isset( $pWhen['mo']) ? $pWhen['mo'] : 1;
		#	$iDay = isset( $pWhen['day']) ? $pWhen['day'] : 1;
		#	$iHr = isset( $pWhen['hr']) ? $pWhen['hr'] : 0;
		#	$iMi = isset( $pWhen['mi']) ? $pWhen['mi'] : 0;
		#	$sWhen = sprintf( '%04d-%02d-%02d %02d:%02d:00', $iYr, $iMo, $iDay, $iHr, $iMi);
			$sWhen = $pWhen['cron'];
		//	error_log( "DBG=>>> cal when: ".$sWhen);
		#	$pCal->setStart( $sWhen);
		#	$pCal->setEnd( $sWhen);
			$this->setCron( $sWhen);
		}
		if ( $bOK) {
			$this->commit();
			$lRet = $this->lID;
			$this->triggerCalSync();
		} else error_log( "ERR=>>> cal update with no changes");
		return( $lRet);
	}

	function addEvent( $lPln, $sName, $pWhen, $lWho=0) {
		$lRet=0;
	//	error_log( "DBG=>>> Cal::addEvent ...");
		if ( ($lPln > 0) && ($pWhen != null)) {
			$this->setPlan( $lPln);
			$this->setName( $sName);
			$this->setWho( $lWho);
			$this->chgEvent( $pWhen);
			$lRet = $this->lID;
		}
		return( $lRet);
	}

	function markDeleted() {
	//	Capture ID
		$lTmp = $this->lID;
		$this->remove();
	//	Capture result of removal
		$lDel = $this->lID;
	//	Suppress ID-detection logic
		$this->lID = $lTmp;
		$this->triggerCalSync();
	//	Restore result of removal
		$this->lID = $lDel;
		return;
	}
}
?>
