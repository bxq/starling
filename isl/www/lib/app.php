<?php
require_once( __DIR__."/../etc/cfg.php");
require_once( __DIR__."/../lib/stored.php");

class App extends Stored {
	var $sFilterUsr;

	function App() {
		$this->Stored();
		$this->sPrd = "starling";
		$this->sTable = "app";
		$this->sFilterUsr = "";
		$this->addField( "id", "int", "Profile ID", 1000, "app_id");
		$this->addField( "created", "date", "Created", 1000, "app_created");
		$this->addField( "eid", "int", "EID", 0, "app_eid");
		$this->addField( "axs", "int", "Public", 0, "app_axs");
		$this->addField( "usr", "int", "Owner", 1000, "app_usr");
		$this->addField( "tnt", "str", "Tenancy", 0, "app_tnt");
		$this->addField( "name", "str", "Profile Key/Tag", 0, "app_name");
		$this->addField( "label", "str", "Application Name", 0, "app_label");
		$this->addField( "cmdb", "str", "CI", 0, "app_cmdb");
		$this->addField( "ask", "str", "ASK ID", 0, "app_ask");
		$this->addField( "fin", "str", "Financial Code", 0, "app_fin");
		$this->addField( "job", "str", "Pipeline", 0, "app_job");
		$this->addField( "txt", "txt", "Notes", 0, "app_txt");
		$this->bEmbedID = true;
		return;
	}

	function creationClause() {
		return( "(created) values (now())");
	}

	function getHeaderField() {
		return( 'label');
	}

	function getDisplayedFields() {
		$pRet = array(	0 => 'id',
							1 => 'name',
							2 => 'label',
							3 => 'cmdb',
							4 => 'ask',
							5 => 'fin',
							6 => 'job',
							7 => 'axs',
							8 => 'txt' );
		return( $pRet);
	}

	function getClass( $sCls) {
		$sRet = "";
		switch ( $sCls) {
			default:
				$sRet = "class=appView";
				break;
		};
		return( $sRet);
	}

	function getBackLink() {
		$sRet =	"<a href=\"index.php?ctx=app&cmd=qry&".
					"app_id=".$this->lID."\">Back ...</a>";
		return( $sRet);
	}

	function getSaveLink() {
		$sRet = "<input type=submit value=Save>";
		return( $sRet);
	}

	function findAll() {
		global $pCfg;
		$pRet = array();
		$sWhere = "id > 0";
		$sQry = "select id,label from ".$this->sTable." where ".$sWhere." order by label";
	//	error_log( "DBG=>>> App::findAll query: ".$sQry);
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$pRet[$pRow['id']] = $pRow['label'];
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return( $pRet);
	}

	function findByName( $sAppName) {
		global $pCfg;
		$lRet=0;
		$sWhere = "id > 0 and name='".$sAppName."'";
		$sQry = "select id from ".$this->sTable." where ".$sWhere." order by id";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$lRet = intval($pRow['id']);
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return( $lRet);
	}

	function findByEID( $sTnt, $lEID) {
		global $pCfg;
		$lRet=0;
		$sWhere = "tnt='".$sTnt."' and eid=".$lEID;
		$sQry = "select id from ".$this->sTable." where ".$sWhere." order by id";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		if ( $pRslt != null) {
			if ( $pRow = mysqli_fetch_assoc( $pRslt)) {
				$lRet = intval($pRow['id']);
			}
		}
		return( $lRet);
	}

	function findByRBAC( $sUser, $bAdmin, $lOrg=0) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/starlingrbac.php");
		require_once( $pCfg['path_www']."/lib/who.php");
		require_once( $pCfg['path_www']."/lib/org.php");
		$pRet = array();
		$pRBAC = null;
		$pWho = new Who();
		$lUID = $pWho->mapLogin( $sUser);
		$pLst = array();
		$sWhere = "id > 0";
		$sQry = "select id,axs,tnt,usr,name,label,cmdb from ".$this->sTable." where ".$sWhere." order by label";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$pInf = array();
			$pInf['axs'] = $pRow['axs'];
			$pInf['tnt'] = $pRow['tnt'];
			$pInf['usr'] = $pRow['usr'];
			$pInf['name'] = $pRow['name'];
			$pInf['label'] = $pRow['label'];
			$pInf['cmdb'] = $pRow['cmdb'];
			$pLst[$pRow['id']] = $pInf;
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		$pOrg = new Org();
	//	Build list of orgs that user belongs to, or to filter by
		$pOrgs = array();
		if ( $lOrg < 1) {
			$pTmpLst = $pOrg->findAll();
			foreach ( $pTmpLst as $lOrgID => $sOrg) {
				if ( $pOrg->isMember( $lUID, $lOrgID)) $pOrgs[$lOrgID] = $sOrg;
			}
		} else {
			if ( $pOrg->isMember( $lUID, $lOrg)) {
				$pOrg->find( $lOrg);
			} else $pOrg = null;
		}
	//	Check RBAC for any that require it
		foreach ( $pLst as $lApp => $pInf) {
			$bInc = false;
			$lUsr = intval( $pInf['usr']);
			$sPerm = $bAdmin ? "app_adm_".$lApp : "app_".$lApp;
			if ( $lOrg < 1) {
			//	Inclusion logic unfiltered by specific tenancy
				if ( $lUID != $lUsr) {
					$bInc = rbacCheck( $lUID, $sPerm);
					if ( ($pInf['axs'] == 0) && ($bInc == false)) {
						foreach ( $pOrgs as $lOrg => $sOrg) {
							if ( rbacCheck( 0, "app_".$lApp, "org_adm_".$lOrg)) $bInc = true;
						}
					}
				} else $bInc = true;
			} else {
			//	Inclusion logic filtered by specific tenancy
				if ( $pOrg != null) {
					if ( rbacCheck( 0, "app_".$lApp, "org_adm_".$lOrg)) {
						if ( $lUID != $lUsr) {
							$bInc = rbacCheck( $lUID, $sPerm);
							if ( ($pInf['axs'] == 0) && ($bInc == false)) $bInc = true;
						} else $bInc = true;
					}
				}
			}
			if ( $bInc) {
				$sUserName = $pWho->mapUID( intval($pInf['usr']));
				$pRet[$lApp] = "".$pInf['label'];
			}
		}
		return( $pRet);
	}

	function findByAdmin( $sUser) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/who.php");
		$pRet = array();
		$pApp = new App();
		$pWho = new Who();
		$sQry = "select id from ".$this->sTable." where id > 0";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$lUID = $pWho->mapLogin( $sUser);
			$sPerm = "app_adm_".$pRow['id'];
			if ( $pRBAC->check( $sPerm, $lUID)) {
				$lApp = intval( $pRow['id']);
				$pApp->find( $lApp);
				$sUserName = $pWho->mapUID( intval($pApp->getUID()));
				$pRet[$pRow['id']] = "".$pApp->getLabel()." <i>(".$pApp->getName()." / ".$sUserName.")</i>";
			}
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return( $pRet);
	}

	function getOrgs() {
		global $pCfg;
		$pRet = array();
		$sQry = "select distinct(r.title),r.description from rbac_roles r join rbac_rolepermissions x on x.RoleID=r.ID join rbac_permissions p on p.id=x.PermissionID where r.title regexp 'org_[0-9].*' and p.title regexp 'app_[0-9].*'";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
			$lOrg = intval( explode("_",$pRow['title'])[1]);
			$pRet[$lOrg] = $pRow['description'];
		}
		return( $pRet);
	}

	function getEID() { return( $this->pFlds['eid']['val']); }
	function setEID( $lEID) {
		$this->pFlds['eid']['val'] = $lEID;
		return;
	}

	function isPublic() {
		$bRet = ($this->pFlds['axs']['val'] == "0") ? true : false;
		return( $bRet);
	}

	function setPublic( $bPublic) {
		$sAxs = $bPublic ? "0" : "1";
		$this->pFlds['axs']['val'] = $sAxs;
		return;
	}

	function getUID() { return( $this->pFlds['usr']['val']); }
	function setUID( $lUID) {
		$this->pFlds['usr']['val'] = $lUID;
	}

	function setUser( $sUser) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/dbcxn.php");
		$sQry =	"select id from who where login='".$sUser."'";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		$pRow = ($pRslt != null) ? mysqli_fetch_assoc( $pRslt) : null;
		if ( $pRow != null) {
			$this->setUID( $pRow['id']);
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return;
	}

	function setFilterUser( $sUser) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/dbcxn.php");
		$sQry =	"select id from who where login='".$sUser."'";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		$pRow = ($pRslt != null) ? mysqli_fetch_assoc( $pRslt) : null;
		if ( $pRow != null) {
			$this->sFilterUsr = $pRow['id'];
		}
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		return;
	}

	function getTenancy() { return( $this->pFlds['tnt']['val']); }
	function setTenancy( $sTxt) {
		$this->pFlds['tnt']['val'] = $sTxt;
		return;
	}

	function getName() { return( $this->pFlds['name']['val']); }
	function setName( $sTxt) {
		$sName = preg_replace( '/\s/', '', $sTxt);
		if (	($this->pFlds['cmdb']['val'] == '') ||
				($this->pFlds['cmdb']['val'] == $this->pFlds['name']['val'])) {
			$this->pFlds['cmdb']['val'] = $sName;
		}
		$this->pFlds['name']['val'] = $sName;
		return;
	}

	function getLabel() { return( $this->pFlds['label']['val']); }
	function setLabel( $sTxt) {
		$this->pFlds['label']['val'] = $sTxt;
		return;
	}

	function getCI() { return( $this->pFlds['cmdb']['val']); }
	function setCI( $sTxt) {
		$this->pFlds['cmdb']['val'] = $sTxt;
		return;
	}

	function mapCI() {
		$sRet = $this->pFlds['cmdb']['val'];
		if ( $sRet == '') $sRet = $this->pFlds['name']['val'];
		if ( $sRet == '') $sRet = 'unregistered';
		return( $sRet);
	}

	function getAskID() { return( $this->pFlds['ask']['val']); }
	function setAskID( $sTxt) {
		$this->pFlds['ask']['val'] = $sTxt;
		return;
	}

	function getGLCode() { return( $this->pFlds['fin']['val']); }
	function setGLCode( $sTxt) {
		$this->pFlds['fin']['val'] = $sTxt;
		return;
	}

	function getJob() { return( $this->pFlds['job']['val']); }
	function setJob( $sTxt) {
		$this->pFlds['job']['val'] = $sTxt;
		return;
	}

	function getPlanFromPool( $bAutoCreate=true) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/dbcxn.php");
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pRet = new Pln();
		$lPln = 0;
		$sQry =	"select p.id as id,t.id as tid,t.status from pln p left join tst t on t.pln=p.id where p.app=".$this->lID." and p.tnt='liftoff' and (t.id is null or t.status=0)";
		$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
		$pRow = ($pRslt != null) ? mysqli_fetch_assoc($pRslt) : null;
		if ( $pRow != null) $lPln = $pRow['id'];
		if ( $pRslt != null) mysqli_free_result( $pRslt);
		if ( $bAutoCreate && ($lPln < 1)) {
			$pRet->setApp($this->lID);
			$pRet->setName("LiftOff pool plan");
			$pRet->setTenant($this->getTenancy());
			$pRet->commit();
		} else $pRet->find($lPln);
		return($pRet);
	}

	function getTestHistory( $lTheApp=0, $bFull=false) {
		$lApp = ($lTheApp > 0) ? $lTheApp : $this->lID;
		$pRet = array();
		if ( $lApp > 0) {
			global $pCfg;
			require_once( $pCfg['path_www']."/lib/tst.php");
			require_once( $pCfg['path_www']."/lib/pln.php");
			$pTst = new Tst();
			$pLst = $pTst->findByApp( $lApp, true);
			if ( count( $pLst) > 0) {
				$pTst->find( array_keys($pLst)[0]);
				$sLabel = sprintf( "In progress (%d) ... %s",$pTst->lID,$pTst->getName());
				if ( $bFull) {
					$pPln = new Pln();
					$pPln->find( $pTst->getPlan());
					$pNfo = array(	'label' => $sLabel,
										'pln' => $pPln->lID, 
										'plnlbl' => $pPln->getName(), 
										'doc' => $pPln->getDoc(),
										'rpturl' => $pTst->getURL());
					$pRet[$pTst->lID] = $pNfo;
				} else $pRet[$pTst->lID] = $sLabel;
			}
			$pLst = $pTst->findByApp( $lApp, false);
			foreach ( $pLst as $lTst => $sTst) {
				$pTst->find( $lTst);
				$sLabel = sprintf( "%s", "".$pTst->getCreated());
				if ( $bFull) {
					$pPln = new Pln();
					$pPln->find( $pTst->getPlan());
					$pNfo = array(	'label' => $sLabel,
										'pln' => $pPln->lID, 
										'plnlbl' => $pPln->getName(), 
										'doc' => $pPln->getDoc(),
										'rpturl' => $pTst->getURL());
					$pRet[$pTst->lID] = $pNfo;
				} else $pRet[$lTst] = $sLabel;
			}
		}
		return( $pRet);
	}

	function customPostLoad( $sFld) {
		$bRet = false;
		switch ( $sFld) {
			case 'axs':
				$sKey = $this->pFlds[$sFld]['post'];
				$bPub = isset( $_POST[$sKey]) ? ($_POST[$sKey] == "true") : false;
				$this->setPublic( $bPub);
				$bRet = true;
				break;
		};
		return( $bRet);
	}

	function customSessionLoad( $sFld) {
		$bRet = false;
		switch ( $sFld) {
			case 'axs':
				$sKey = $this->pFlds[$sFld]['post'];
				$bPub = isset( $_SESSION[$sKey]) ? ($_SESSION[$sKey] == "true") : false;
				$this->setPublic( $bPub);
				$bRet = true;
				break;
		};
		return( $bRet);
	}

	function showField( $sKey, $iMode) {
		$sRet = '';
		$this->bEdit = true;
		switch ( $sKey) {
			case 'id':
				$sCtl = "appidnouse";
				$sRet .= sprintf( "<td colspan=2><label class=%s><input type=text id=%s name=%s value=\"%s\" onchange=\"this.dataset.empty = !this.value;\" disabled=disabled><span data-label=\"%s\"></span></label></td>\n",
										"txtFld", $sCtl, $sCtl, $this->pFlds[$sKey]['val'], $this->pFlds[$sKey]['label']);
				break;

			case 'label':
			case 'name':
			case 'cmdb':
			case 'ask':
			case 'fin':
			case 'job':
				$sCtl = $this->pFlds[$sKey]['post'];
				$sRet .= sprintf( "<td colspan=2><label class=%s><input type=text id=%s name=%s value=\"%s\" onchange=\"this.dataset.empty = !this.value;\"><span data-label=\"%s\"></span></label></td>\n",
										"txtFld", $sCtl, $sCtl, $this->pFlds[$sKey]['val'], $this->pFlds[$sKey]['label']);
				break;

			case 'axs':
				$pFld = $this->pFlds[$sKey];
				$sVal = $this->isPublic() ? " checked" : "";
				$sFlags = "";
				if ( $iMode < $pFld['mode']) $sFlags = " disabled";
				$sRet .= sprintf( "<td>%s:</td>", $pFld['label']);
				$sRet .= sprintf(	"<td><input type=checkbox name=%s value=true %s %s/></td>\n", $pFld['post'], $sVal, $sFlags);
				break;

			default: $sRet = sprintf( "<td colspan=2></td>"); break;
		}
		return( $sRet);
	}
}
?>
