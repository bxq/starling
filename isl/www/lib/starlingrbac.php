<?php
require_once( __DIR__."/../etc/cfg.php");

function rbacCheck( $lUID, $sPerm, $sRole=null) {
	global $pCfg;
	require_once( $pCfg['path_www']."/lib/phprbac.php");
	$pRBAC = new PhpRbac\Rbac();
	$bRet = false;
	try {
		if ( $lUID > 0) {
			if ( $sPerm == null) {
				if ( $sRole != null) {
					$bRet = $pRBAC->Users->hasRole( $sRole, $lUID);
				}
			} else $bRet = $pRBAC->check( $sPerm, $lUID);
		} else {
			if ( $sRole != null) {
				$lRole = $pRBAC->Roles->titleId( $sRole);
				$lPerm = $pRBAC->Permissions->titleId( $sPerm);
				$bRet = $pRBAC->Roles->hasPermission( $lRole, $lPerm);
			}
		}
	} catch ( Exception $e) { $bRet=false; /*error_log( "DBG=>>> ".$e->getMessage()."");*/ }
	return( $bRet);
}

function findOrgUsers( $lOrg, $sUser=null, $bAdm=null) {
	require_once( "lib/dbcxn.php");
	require_once( "lib/who.php");
	require_once( "lib/org.php");
	$pRet = array();
	$pIn = array();
	$pOrgs = array();
	$pOrg = new Org();
	$pWho = new Who();
	$pOut = $pWho->findAll();
	if ( $lOrg < 1) {
		$lUID = $pWho->mapLogin( $sUser);
		$pOrgs = $pOrg->findByAdmin( $lUID);
	} else {
		$sGrp = $bAdm ? "org_adm_"+$lOrg : "org_"+$lOrg;
		$pOrgs[$lOrg]=$sGrp;
	}
	foreach ( $pOrgs as $lOrg => $sOrg) {
		if ( $lOrg > 0) {
			$bAdmin = ($bAdm != null) ? $bAdm : false;
			$pOrg->find( $lOrg);
			foreach ( $pOut as $lUsr => $sUsr) {
				if ( $lUsr > 1) {
					$bAdd = $bAdmin	? $pOrg->isAdmin( $lUsr, $lOrg)
											: $pOrg->isMember( $lUsr, $lOrg);
					if ( $bAdd) $pIn[$lUsr] = $sUsr;
				}
			}
		}
	}
	foreach ( $pIn as $lUsr => $sUsr) {
		if ( array_key_exists( $lUsr, $pOut)) unset($pOut[$lUsr]);
	}
	if ( array_key_exists( 0, $pOut)) unset( $pOut[0]);
	$pRet['in'] = $pIn;
	$pRet['out'] = $pOut;
	$pRet['orgs'] = $pOrgs;
	return( $pRet);
}

function findOrgAdmins( $lOrg) {
	return( findOrgUsers( $lOrg, null, true));
}

function findAppUsers( $lApp) {
	require_once( "lib/dbcxn.php");
	require_once( "lib/who.php");
	$pRet = array();
	$pIn = array();
	$pEng = array();
	$pAdm = array();
//	error_log( "DBG=>>> findAppUsers invoked");
	$pWho = new Who();
	$pOut = $pWho->findAll();
	foreach ( $pOut as $lUsr => $sUsr) {
		if ( $lUsr > 1) {
			$bAxs = false;
		//	error_log( "DBG=>>> checking if ".$sUsr." is admin for app ".$lApp."...");
			$bAxs = rbacCheck( $lUsr, null, "app_adm_".$lApp);
			if ( !$bAxs) {
			//	error_log( "DBG=>>> checking if ".$sUsr." is engineer for app ".$lApp."...");
				$bAxs = rbacCheck( $lUsr, null, "app_eng_".$lApp);
				if ( !$bAxs) {
				//	error_log( "DBG=>>> checking if ".$sUsr." is viewer for app ".$lApp."...");
					$bAxs = rbacCheck( $lUsr, null, "app_".$lApp);
					if ( $bAxs) $pIn[$lUsr] = $sUsr;
				} else $pEng[$lUsr] = $sUsr;
			} else { $pAdm[$lUsr] = $sUsr; }
		}
	}
	foreach ( $pIn as $lUsr => $sUsr) {
		if ( array_key_exists( $lUsr, $pOut)) unset($pOut[$lUsr]);
	}
	foreach ( $pEng as $lUsr => $sUsr) {
		if ( array_key_exists( $lUsr, $pOut)) unset($pOut[$lUsr]);
	}
	foreach ( $pAdm as $lUsr => $sUsr) {
		if ( array_key_exists( $lUsr, $pOut)) unset($pOut[$lUsr]);
	}
	if ( array_key_exists( 0, $pOut)) unset( $pOut[0]);
	$pRet['in'] = $pIn;
	$pRet['eng'] = $pEng;
	$pRet['adm'] = $pAdm;
	$pRet['out'] = $pOut;
	return( $pRet);
}
?>
