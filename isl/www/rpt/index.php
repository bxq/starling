<?php
require_once( "../etc/private/localcfg.php");
require_once( "../lib/dbcxn.php");

function genRpt() {
	$pRet = array();
	$pRet['rpt'] = pgStr( "rpt", "tst");
	$sTop = pgStr( "from");
	if ( $sTop != '') {
		$pRet['rng'] = "l.chg >= '".$sTop."'";
		$pRet['sfx'] = $sTop;
		$sBtm = pgStr( "to");
		if ( $sBtm != '') {
			$pRet['monthly'] = false;
			$pRet['rng'] .= " and l.chg < '".$sBtm."'";
			$pRet['sfx'] .= "-to-".$sBtm;
		}
	} else {
		$sRng = pgStr( "monthly");
		$bMonthly = ($sRng != "") ? true : false;
		$sNow = $bMonthly ? date("Y-m-01") : date("Y-m-d");
		$sOffset = $bMonthly ? "interval 1 month" : "interval 1 day";
		$pRet['rng'] = "l.chg >= '".$sNow."' - ".$sOffset;
		$pRet['sfx'] = $bMonthly ? "monthly" : "daily";
	}
	return( $pRet);
}

function genRspTnt( $pRpt) {
	global $pCfg;
	$pRet = array();
	$sWhere = "l.ltd=1011 and ".$pRpt['rng'];
	$sQry="select o.idx as tntid,convert_tz(l.chg,'America/Chicago','UTC') as chg,'',u.login as msid,o.tag as name,'' as glcode from log l join lut o on (o.lut=201 and o.idx=l.oid) join who u on u.id=l.who where ".$sWhere." order by l.chg";
//	error_log( "DBG=>>> query: ".$sQry);
	$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
	while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
		$pItm = array();
		$pItm['tntid']=intval($pRow['tntid']);
		$pItm['created']=$pRow['chg'];
		$pItm['tenant']=$pRow['name'];
		$pItm['msid']=$pRow['msid'];
		$pItm['glcode']=$pRow['glcode'];
		array_push( $pRet, $pItm);
	}
	if ( $pRslt != null) mysqli_free_result( $pRslt);
	return( $pRet);
}

function genRspApp( $pRpt) {
	global $pCfg;
	$pRet = array();
	$sWhere = "l.ltd=1111 and ".$pRpt['rng'];
	$sQry="select a.id as appid,convert_tz(l.chg,'America/Chicago','UTC') as chg,a.name as appname,u.login as msid,o.tag as tenant,'' as glcode from log l join app a on a.id=l.oid join lut o on (o.lut=201 and o.tag=a.tnt) join who u on u.id=a.usr where ".$sWhere." order by a.created";
//	error_log( "DBG=>>> query: ".$sQry);
	$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
	while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
		$pItm = array();
		$pItm['appid']=intval($pRow['appid']);
		$pItm['created']=$pRow['chg'];
		$pItm['appname']=$pRow['appname'];
		$pItm['msid']=$pRow['msid'];
		$pItm['tenant']=$pRow['tenant'];
		$pItm['glcode']=$pRow['glcode'];
		array_push( $pRet, $pItm);
	}
	if ( $pRslt != null) mysqli_free_result( $pRslt);
	return( $pRet);
}

function genRspTst( $pRpt) {
	global $pCfg;
	$pRet = array();
	$sWhere = $pRpt['rng'];
//	$sWhere = "l.ltd=108 and ".$pRpt['rng'];
//	$sQry="select t.id as testid,convert_tz(l.chg,'America/Chicago','UTC') as start,'',u.login as msid,a.label as appname,t.status,p.tnt as tenant,a.ask as askid,a.cmdb as ci,a.fin as glcode,p.ngn as fabric,p.frm as framework,p.git,p.doc,p.vusr from log l join tst t on t.id=l.oid join pln p on p.id=t.pln join app a on a.id=p.app join who u on u.id=l.who where ".$sWhere." order by t.created";
	$sQry="select t.id as testid, convert_tz(l.chg,'America/Chicago','UTC') as start,u.login as msid, a.label as appname, t.status,p.tnt as tenant, a.ask as askid, a.cmdb as ci, a.fin as glcode, p.ngn as fabric, p.frm as framework, p.git,p.doc, p.vusr from log l left join log lb on (lb.oid=l.oid and l.ltd=108 and lb.ltd=109) join tst t on t.id=l.oid join pln p on p.id=t.pln join app a on a.id=p.app join who u on u.id=l.who where ".$sWhere." order by t.created";
//	error_log( "DBG=>>> query: ".$sQry);
	$pRslt = mysqli_query( $pCfg['dbcxn'], $sQry);
	while ( $pRow = mysqli_fetch_assoc( $pRslt)) {
		$pItm = array();
		$pItm['testid']=intval($pRow['testid']);
		$pItm['start']=$pRow['start'];
		$pItm['tenant']=$pRow['tenant'];
		$pItm['appname']=$pRow['appname'];
		$pItm['askid']=$pRow['askid'];
		$pItm['ci']=$pRow['ci'];
		$pItm['msid']=$pRow['msid'];
		$pItm['glcode']=$pRow['glcode'];
		$pItm['fabric']=$pRow['fabric'];
		$pItm['framework']=$pRow['framework'];
		$pItm['git']=$pRow['git'];
		$pItm['doc']=$pRow['doc'];
		$pItm['status']=intval($pRow['status']);
		array_push( $pRet, $pItm);
	}
	if ( $pRslt != null) mysqli_free_result( $pRslt);
	return( $pRet);
}

function genJSON() {
	$pRpt = genRpt();
	$sRpt = $pRpt['rpt'];
	header( 'Content-type:application/json;charset=utf-8');
	header( "Content-Disposition: attachment; filename=\"starling-".$sRpt."-".$pRpt['sfx'].".json\"");
	header( "Pragma: public");
	header( "Cache-Control: must-revalidate, post-check=0, pre-check=0");
	switch ( $sRpt) {
		case "tnt": echo json_encode(genRspTnt($pRpt)); break;
		case "app": echo json_encode(genRspApp($pRpt)); break;
		default: echo json_encode(genRspTst($pRpt)); break;
	}
	return;
}

function rptBypass() {
	$sHTML = "<html>\n";
	$sHTML .= "<html>\n";
	$sHTML .= "<body>\n";
	$sHTML .= "<table align=center>\n";
	$sHTML .= "<tr><td align=center>This is a legacy reporting endpoint that is no longer active.</td></tr>\n";
	$sHTML .= "</table>\n";
	$sHTML .= "</body>\n";
	$sHTML .= "</html>\n";
	printf( "%s", $sHTML);
}

#genJSON();
rptBypass();
?>
