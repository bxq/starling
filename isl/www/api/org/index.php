<?php
$sPageBaseLoc = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
require_once( $sPageBaseLoc.'/etc/private/localcfg.php');

function apiOrgGET( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiOrgGET invoked ... ");
	$lOrg = pgInt( 'id', 0);
	$sFilter = pgStr( 'filterbyuser', '');
	if ( $lOrg > 0) {
		$pReq['id'] = $lOrg;
		$pOrg = $pLogic->apiOrgInfo( $pReq);
		if ( isset($pOrg['id'])) array_push( $pRet, $pOrg);
	} else {
		if ( $sFilter != '') $pReq['filterbyuser'] = ($sFilter == "True") ? true : false;
		$pOrgs = $pLogic->apiOrgList( $pReq);
		foreach ( $pOrgs as $lOrg => $sOrg) array_push( $pRet, array("id"=>$lOrg,'name'=>$sOrg));
	}
	return( $pRet);
}

function apiOrgPOST( $pLogic, $pReq) {
//	error_log( "DBG=>>> apiOrgPOST invoked ... ");
	error_log( "ERR=>>> unsupported apiOrgPOST invoked ... ");
	return( array());
}

function apiOrgPUT( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiOrgPUT invoked ... ");
	error_log( "ERR=>>> unsupported apiOrgPUT invoked ... ");
	return( $pRet);
}

function apiOrgDELETE( $pLogic, $pReq) {
//	error_log( "DBG=>>> apiOrgDELETE invoked ... ");
	error_log( "ERR=>>> unsupported apiOrgDELETE invoked ... ");
	return( array());
}

function genRspREST( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> genRsp invoked ... ".var_export($pReq,true));
	$pRsp = array();
	switch ( $_SERVER['REQUEST_METHOD']) {
		case 'GET': $pRsp = apiOrgGET( $pLogic, $pReq); break;
		case 'POST': $pRsp = apiOrgPOST( $pLogic, $pReq); break;
		case 'PUT': $pRsp = apiOrgPUT( $pLogic, $pReq); break;
		case 'DELETE': $pRsp = apiOrgDELETE( $pLogic, $pReq); break;
	}
	foreach ( $pRsp as $pItm) array_push( $pRet, $pItm);
	return( $pRet);
}

require_once( '../index.php');
?>
