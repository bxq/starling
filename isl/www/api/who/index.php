<?php
$sPageBaseLoc = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
require_once( $sPageBaseLoc.'/etc/private/localcfg.php');

function apiWhoGET( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiWhoGET invoked ... ");
	$pLst = $pLogic->apiWhoInfo_redir( pgInt('id',0));
	$iRC = (isset($pLst['rc'])) ? $pLst['rc'] : -1;
	if ( $iRC == 0) {
		array_push( $pRet, array('id'=>$pLst['id'],'login'=>$pLst['login']));
	}
	return( $pRet);
}

function apiWhoPOST( $pLogic, $pReq) {
//	error_log( "DBG=>>> apiWhoPOST invoked ... ");
	error_log( "ERR=>>> unsupported apiWhoPOST invoked ... ".var_export($pReq,true));
	return( array());
}

function apiWhoPUT( $pLogic, $pReq) {
//	error_log( "DBG=>>> apiWhoPUT invoked ... ");
	error_log( "ERR=>>> unsupported apiWhoPUT invoked ... ".var_export($pReq,true));
	return( array());
}

function apiWhoDELETE( $pLogic, $pReq) {
//	error_log( "DBG=>>> apiWhoDELETE invoked ... ");
	error_log( "ERR=>>> unsupported apiWhoDELETE invoked ... ".var_export($pReq,true));
	return( array());
}

function genRspREST( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> genRsp invoked ... ".var_export($pReq,true));
	$pRsp = array();
	switch ( $_SERVER['REQUEST_METHOD']) {
		case 'GET': $pRsp = apiWhoGET( $pLogic, $pReq); break;
		case 'POST': $pRsp = apiWhoPOST( $pLogic, $pReq); break;
		case 'PUT': $pRsp = apiWhoPUT( $pLogic, $pReq); break;
		case 'DELETE': $pRsp = apiWhoDELETE( $pLogic, $pReq); break;
	}
	foreach ( $pRsp as $pItm) array_push( $pRet, $pItm);
	return( $pRet);
}

require_once( '../index.php');
?>
