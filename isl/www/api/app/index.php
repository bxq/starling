<?php
$sPageBaseLoc = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
require_once( $sPageBaseLoc.'/etc/private/localcfg.php');

function apiAppGET( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiAppGET invoked ... ");
	$lApp=pgInt( 'id', 0);
	$lEID = pgInt( 'eid', 0);
	$sTnt = pgStr( 'tnt', '');
	if ( ($lEID > 0) && ($sTnt != '')) {
		array_push( $pRet, $pLogic->apiAppInfo( array('eid'=>$lEID,'tnt'=>$sTnt)));
	} else if ( $lApp <= 0) {
		$pLst = $pLogic->apiAppList( array());
		foreach ( $pLst as $lApp => $sApp) array_push( $pRet, array( 'id'=>$lApp, 'label'=>$sApp));
	} else array_push( $pRet, $pLogic->apiAppInfo( array('id'=>$lApp)));
	return( $pRet);
}

function apiAppPOST( $pLogic, $pReq) {
	global $pCfg;
	$pRet = array();
//	error_log( "DBG=>>> apiAppPOST invoked ... ");
	$sTNT='';
	$sGLC='';
	$pTNT=array();
	$pApp=array();
	foreach ( $pReq as $sKey => $pItm) {
		if ( ($sKey == 'tnt') && is_array( $pReq[$sKey])) {
			$pTNT = $pReq['tnt'];
			$sTNT = isset($pReq['tnt']['name']) ? $pReq['tnt']['name'] : '';
			$sGLC = isset($pReq['tnt']['fin']) ? $pReq['tnt']['fin'] : '';
		} else $pApp[$sKey]=$pReq[$sKey];
	}
	if ( !isset($pApp['fin'])) $pApp['fin']=$sGLC;
	$pNfo = $pLogic->apiOnBoard( array( 'tnt' => $pTNT, 'app' => $pApp));
	$pDat = array();
	$pDat['tnt'] = array();
	$pDat['id'] = isset($pNfo['app']) ? $pNfo['app'] : 0; 
	$pDat['tnt']['id'] = isset($pNfo['tnt']) ? $pNfo['tnt'] : 0; 
	$pDat['tnt']['name'] = $sTNT; 
	array_push( $pRet, $pDat);
	return( $pRet);
}

function apiAppPUT( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiAppPUT invoked ... ");
	$pRet = array();
	$sTNT='';
	$pApp=array();
	foreach ( $pReq as $sKey => $pItm) {
		if ( ($sKey == 'tnt') && is_array( $pReq[$sKey])) {
			$sTNT = isset($pReq['tnt']['name']) ? $pReq['tnt']['name'] : '';
		} else $pApp[$sKey]=$pReq[$sKey];
	}
	if ( $sTNT != '') $pApp['tnt']=$sTNT;
	$pNfo = $pLogic->apiAppUpdate( $pApp);
	$pDat = array();
	$pDat['tnt'] = array();
	$pDat['id'] = isset($pNfo['app']) ? $pNfo['app'] : 0; 
	$pDat['tnt']['id'] = isset($pNfo['tnt']) ? $pNfo['tnt'] : 0; 
	$pDat['tnt']['name'] = $sTNT; 
	return( array_push( $pRet, $pDat));
	return( $pRet);
}

function apiAppDELETE( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiAppDELETE invoked ... ");
	$lApp=pgInt( 'id', 0);
	$lEID = pgInt( 'eid', 0);
	$sTnt = pgStr( 'tnt', '');
	if ( ($lEID > 0) && ($sTnt != '')) {
		$pLogic->apiAppDelete( array('eid'=>$lEID,'tnt'=>$sTnt));
	} else if ( $lApp > 0) {
		$pLogic->apiAppDelete( array('id'=>$lApp));
	}
	return( array());
}

function genRspREST( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> genRsp invoked ... ".var_export($pReq,true));
	$pRsp = array();
	switch ( $_SERVER['REQUEST_METHOD']) {
		case 'GET': $pRsp = apiAppGET( $pLogic, $pReq); break;
		case 'POST': $pRsp = apiAppPOST( $pLogic, $pReq); break;
		case 'PUT': $pRsp = apiAppPUT( $pLogic, $pReq); break;
		case 'DELETE': $pRsp = apiAppDELETE( $pLogic, $pReq); break;
	}
	foreach ( $pRsp as $pItm) array_push( $pRet, $pItm);
	return( $pRet);
}

require_once( '../index.php');
?>
