<?php
require_once( $pCfg['path_www']."/lib/dbcxn.php");
require_once( $pCfg['path_www']."/api/svc.php");
require_once( $pCfg['path_www']."/lib/phprbac.php");
use PhpRbac\Rbac;

function islEventLog( $sLog, $pMap, $bWhen=true) {
//	global $pCfg;
//	$sEvents = isset($pCfg['events']) ? $pCfg['events'] : null;
//	$sTopic = ($sLog != null) ? $sLog : '';
//	if ( $bWhen && !isset($pMap['when'])) $pMap['when']=gmdate('r');
//	$sMsg = json_encode( $pMap, JSON_UNESCAPED_SLASHES);
//	if ( ($sEvents != null) && ($sTopic != '') && ($sMsg != '')) {
//		$pEvents = new RdKafka\Producer();
//		$pEvents->addBrokers( $sEvents);
//		$topic = $pEvents->newTopic( $sTopic);
//		$topic->produce( RD_KAFKA_PARTITION_UA, 0, $sMsg);
//		while ( $pEvents->getOutQLen() > 0) { $pEvents->poll(0); }
//	}
	return;
}

function ownerCheck( $lUsr, $sPerm) {
	global $pCfg;
	$bRet=false;
	$sTag = substr( $sPerm,0,4);
	if ( $sTag === "app_") {
		$sTag = substr( $sPerm,0,8);
		if ( $sTag === "app_adm_") {
			$lApp = intval( substr( $sPerm, 8));
		} else $lApp = intval( substr( $sPerm, 4));
		require_once( $pCfg['path_www']."/lib/app.php");
		$pApp = new App();
		$pApp->find( $lApp);
		if ( ($pApp->lID == $lApp) && ($pApp->getUID() == $lUsr)) $bRet=true;
	}
	return( $bRet);
}

function launchAccess( $lOID, $lPOID) {
	global $pCfg;
	global $sLogin;
	require_once( $pCfg['path_www']."/lib/who.php");
	require_once( $pCfg['path_www']."/lib/app.php");
	require_once( $pCfg['path_www']."/lib/phprbac.php");
	$pRet=array();
	$pWho = new Who();
	$lUsr = isset($pReq['usr']) ? intval($pReq['usr']) : 0;
	$lUsr = $pWho->mapLogin( $sLogin);
//	Determine if user owns profile
	$pApp = new App();
	$pApp->find( $lPOID);
	$bOwn = (($pApp->lID == $lPOID) && ($pApp->getUID() == $lUsr));
	if ( !$bOwn && ($sLogin != 'starling')) {
		$bPub = $pApp->isPublic();
		if ( $lUsr > 0) {
			$pRBAC = new PhpRbac\Rbac();
			$sPerm = "app_xqt_".$lPOID;
			$pRoles = $pRBAC->Users->allRoles( $lUsr);
			foreach ( $pRoles as $pRole) {
				$sRole = $pRole['Title'];
				$lRole = $pRBAC->Roles->titleId( $sRole);
				$lPerm = $pRBAC->Permissions->titleId( $sPerm);
				$bChk = $pRBAC->Roles->hasPermission( $lRole, $lPerm);
				if ( !$bChk) {
					$sTag = substr( $sRole,0,4);
					if ( $sTag === "org_") {
						$sTag = substr( $sRole,0,8);
						if ( $sTag === "org_adm_") {
							$lOrg = intval( substr( $sRole, 8));
						} else $lOrg = intval( substr( $sRole, 4));
					//	rbacCheck( 0, "app_".$lApp, "org_adm_".$lOrg)
						$lRole = $pRBAC->Roles->titleId( 'org_adm_'.$lOrg);
						$lPerm = $pRBAC->Permissions->titleId( $sPerm);
						$bChk = $pRBAC->Roles->hasPermission( $lRole, $lPerm);
						if ( $bChk && $bPub) $pRet['has']=true;
					}
				} else $pRet['has']=true;
			}
		}
	} else $pRet['has']=true;
	return( $pRet);
}

function detectOID( $sType, $pReq, $sFld='id') {
	global $pCfg;
	$lRet=0;
	$lEID = isset($pReq['eid']) ? $pReq['eid'] : pgInt('eid',0);
	$sTNT = isset($pReq['tnt']) ? $pReq['tnt'] : pgStr('tnt','');
	if ( ($lEID > 0) && ($sTNT != '')) {
		$pObj=null;
		switch ( $sType) {
			case 'app':
				require_once( $pCfg['path_www']."/lib/app.php");
				$pObj = new App();
				break;
			case 'pln':
				require_once( $pCfg['path_www']."/lib/pln.php");
				$pObj = new Pln();
				break;
			case 'tst':
				require_once( $pCfg['path_www']."/lib/tst.php");
				$pObj = new Tst();
				break;
		}
		if ($pObj != null) $lRet = $pObj->findByEID( $sTNT, $lEID);
	} else $lRet = isset($pReq[$sFld]) ? intval($pReq[$sFld]) : pgInt($sFld,0);
	return( $lRet);
}

class StarlingLogic extends SvcLogic {
	function StarlingLogic() { parent::SvcLogic(); }
//	Call-convention identity redirections ...
	function apiDetectJob_redir( $lApp) { $pReq['id']=$lApp; return($this->apiDetectJob($pReq)); }
	function apiCalList_redir( $lApp) { $pReq['id']=$lApp; return($this->apiCalList($pReq)); }
	function apiWhoInfo_redir( $lWho) { $pReq['id']=$lWho; return($this->apiWhoInfo($pReq)); }
	function apiOrgInfo_redir( $lOrg) { $pReq['id']=$lOrg; return($this->apiOrgInfo($pReq)); }
	function apiAppUsrList_redir( $lApp) { $pReq['app']=$lApp; return($this->apiAppUsrList($pReq)); }
	function apiPlnDel_redir( $lPln) { $pReq['pln']=$lPln; return($this->apiPlnDel($pReq)); }
	function apiTestDelete_redir( $lTst) { $pReq['id']=$lTst; return($this->apiTestDelete($pReq['id'])); }
//	Call-convention complex redirections ...
	function apiTestUpdate_redir( $lTst, $sURL) { $pReq['id']=$lTst; $pReq['url']=$sURL; return($this->apiTestUpdate($pReq)); }
	function apiOrgUsrAdd_redir( $lOrg,$lUsr) { $pReq['org']=$lOrg;$pReq['usr']=$lUsr; return($this->apiOrgUsrAdd($pReq)); }
	function apiOrgUsrDel_redir( $lOrg,$lUsr) { $pReq['org']=$lOrg;$pReq['usr']=$lUsr; return($this->apiOrgUsrDel($pReq)); }
	function apiAppUsrAdd_redir( $lApp, $lUsr, $sAxs=null) { $pReq['app']=$lApp; $pReq['usr']=$lUsr; $pReq['axs']=$sAxs; return($this->apiAppUsrAdd($pReq)); }
	function apiAppUsrDel_redir( $lApp, $lUsr, $sAxs=null) { $pReq['app']=$lApp; $pReq['usr']=$lUsr; $pReq['axs']=$sAxs; return($this->apiAppUsrDel($pReq)); }
//	Call-convention reverse-redirections ...
	function apiOrgAdmAdd_redir( $pReq) { $pReq['axs']='admin'; return($this->apiOrgUsrAdd($pReq)); }
	function apiOrgAdmDel_redir( $pReq) { $pReq['axs']='admin'; return($this->apiOrgUsrDel($pReq)); }
	function apiAppEngAdd_redir( $pReq) { $pReq['axs']='eng'; return($this->apiAppUsrAdd($pReq)); }
	function apiAppAdmAdd_redir( $pReq) { $pReq['axs']='adm'; return($this->apiAppUsrAdd($pReq)); }
	function apiAppEngDel_redir( $pReq) { $pReq['axs']='eng'; return($this->apiAppUsrDel($pReq)); }
	function apiAppAdmDel_redir( $pReq) { $pReq['axs']='adm'; return($this->apiAppUsrDel($pReq)); }
//	Standard call-convention methods
//	...
	function apiCheckRBAC( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/who.php");
		require_once( $pCfg['path_www']."/lib/phprbac.php");
		$pRet=array();
		$pWho = new Who();
	//	$lUsr = isset($pReq['usr']) ? intval($pReq['usr']) : 0;
		$lUsr = $pWho->mapLogin( $sLogin);
		$sPerm = isset($pReq['perm']) ? $pReq['perm'] : "";
		if ( !ownerCheck( $lUsr, $sPerm)) {
			if ( false && ($lUsr > 0) && ($sPerm != "")) {
				$pRBAC = new PhpRbac\Rbac();
			//	error_log( "DBG=>>> lUsr: ".$lUsr.", sRole: ".$sPerm);
				$pRoles = $pRBAC->Users->allRoles( $lUsr);
				foreach ( $pRoles as $pRole) {
					$sRole = $pRole['Title'];
				//	error_log( "DBG=>>> checking role ".$sRole." for perm: ".$sPerm);
					if ( $pRBAC->Roles->hasPermission( $sRole, $sPerm)) {
					//	error_log( "DBG=>>> YES: ".$sRole);
						$pRet['has']=true;
					} // else error_log( "DBG=>>> NO: ".$sRole);
				}
			}
		} else $pRet['has']=true;
		return( $pRet);
	}

	function apiOrgUsrList( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/starlingrbac.php");
		$pRBAC = new PhpRbac\Rbac();
		$lOrg = isset($pReq['org']) ? intval($pReq['org']) : 0;
		if ( ($lOrg < 1) && isset($pReq['id'])) $lOrg = intval($pReq['id']);
		$sUsr = isset($pReq['usr']) ? $pReq['usr'] : $sLogin;
	//	error_log( "DBG=>>> API - orgusrlst for ".$lOrg.", ".$sUsr);
		$pRet = findOrgUsers( $lOrg, $sUsr);
		return( $pRet);
	}

	function apiOrgAdmList( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/starlingrbac.php");
		$pRBAC = new PhpRbac\Rbac();
		$lOrg = isset($pReq['org']) ? intval($pReq['org']) : 0;
		if ( ($lOrg < 1) && isset($pReq['id'])) $lOrg = intval($pReq['id']);
		$pRet = findOrgAdmins( $lOrg);
		return( $pRet);
	}

	function apiOrgList( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/org.php");
		require_once( $pCfg['path_www']."/lib/who.php");
		$pRet = array( 'rc' => 0, 'msg' => 'OK', 'lst' => array());
		$pOrg = new Org();
		$pWho = new Who();
		$lUID = $pWho->mapLogin( $sLogin);
		if ( isset($pReq['tag'])) $pOrg->bTag=True;
		$pOrgs = $pOrg->findAll();
		foreach ( $pOrgs as $lOrg => $sOrg) {
			if ( isset($pReq['filterbyuser'])) {
				if ( $pOrg->isMember( $lUID, $lOrg)) $pRet['lst'][$lOrg] = $sOrg;
			} else if ( isset($pReq['filterbyadmin'])) {
				if ( $pOrg->isAdmin( $lUID, $lOrg)) $pRet['lst'][$lOrg] = $sOrg;
			} else $pRet['lst'][$lOrg] = $sOrg;
		}
		return( $pRet);
	}

	function apiOrgInfo( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/org.php");
		$pRet = array();
		$lOrg = isset($pReq['id']) ? $pReq['id'] : 0;
		$pOrg = new Org();
		$pOrg->find( $lOrg);
		if ( $pOrg->lID > 0) {
			$pRet['id'] = $pOrg->getOrgID();
			$pRet['name'] = $pOrg->getName();
			$pRet['label'] = $pOrg->getLabel();
			$pRet['fin'] = $pOrg->getRef();
		}
		return( $pRet);
	}

	function apiOrgAdd( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/org.php");
		$pRet = array();
		$sOrg = isset($pReq['name']) ? $pReq['name'] : '';
		$sLabel = isset($pReq['label']) ? $pReq['label'] : $sOrg;
		if ( $sOrg != '') {
			$pOrg = new Org();
			$pOrg->setName( $sOrg);
			$pOrg->setLabel( $sLabel);
			if ( isset($pReq['fin'])) $pOrg->setRef( $pReq['fin']);
			$pOrg->setIndex();
			$pOrg->commit();
			if ( $pOrg->lID > 0) {
				$lOrg = $pOrg->getOrgID();
				islEventLog( 'sys.starling.create', array("id"=>$lOrg,"obj"=>"tenant","name"=>$sOrg,"who"=>$sLogin));
				$pRet['id'] = $lOrg;
				$pRet['name'] = $pOrg->getName();
				$pRet['label'] = $pOrg->getLabel();
			}
		}
		return( $pRet);
	}

	function apiOrgUpdate( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/org.php");
		$pRet = array( 'rc' => 1, 'msg' => "failure to locate org");
		$lOrg = isset($pReq['id']) ? $pReq['id'] : 0;
		if ( $lOrg > 0) {
			require_once( $pCfg['path_www']."/lib/org.php");
			$pOrg = new Org();
			$pOrg->find( $lOrg);
			if ( $pOrg->lID > 0) {
				if ( isset($pReq['name'])) $pOrg->setName( $pReq['name']);
				if ( isset($pReq['label'])) $pOrg->setLabel( $pReq['label']);
				if ( isset($pReq['fin'])) $pOrg->setRef( $pReq['fin']);
				$pOrg->commit();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			}
		}
		return( $pRet);
	}

	function apiOrgAppAdd( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/org.php");
		require_once( $pCfg['path_www']."/lib/app.php");
		$pRet = array( 'rc' => -1, 'msg' => "profile add failure");
		$pRBAC=null;
		$lOrg = isset($pReq['id']) ? intval($pReq['id']) : 0;
	//	$lApp = isset($pReq['app']) ? intval($pReq['app']) : 0;
		$lApp = detectOID( 'app', $pReq, 'app');
		if ( ($lOrg > 0) && ($lApp > 0)) {
			$pOrg = new Org();
			$bOrg = $pOrg->find( $lOrg);
			$pApp = new App();
			$pApp->find( $lApp);
			if ( $bOrg && ($pApp->lID == $lApp)) {
				$sApp = $pApp->getName();
				$sTnt = $pOrg->getName();
				$pApp->setTenancy( $sTnt);
				$pApp->commit();
				if ( $pRBAC == null) $pRBAC = new Rbac();
				$pRoles = array( "app_".$lApp=>"viewer", "app_eng_".$lApp=>"engineer", "app_adm_".$lApp=>"admin");
				foreach ( $pRoles as $sRole => $sTxt) {
					$lRole = $pRBAC->Roles->add( $sRole, $sTxt);
				}
				$pPerms = array( "app_".$lApp=>"view", "app_xqt_".$lApp=>"execute", "app_adm_".$lApp=>"administration");
				foreach ( $pPerms as $sPerm => $sTxt) {
					$lRole = $pRBAC->Permissions->add( $sPerm, $sTxt);
				}
				$pRBAC->Roles->assign( "app_".$lApp, "app_".$lApp);
				$pRBAC->Roles->assign( "app_eng_".$lApp, "app_".$lApp);
				$pRBAC->Roles->assign( "app_eng_".$lApp, "app_xqt_".$lApp);
				$pRBAC->Roles->assign( "app_adm_".$lApp, "app_".$lApp);
				$pRBAC->Roles->assign( "app_adm_".$lApp, "app_xqt_".$lApp);
				$pRBAC->Roles->assign( "app_adm_".$lApp, "app_adm_".$lApp);
				$pRBAC->Roles->assign( "org_adm_".$lOrg, "app_".$lApp);
				$pRBAC->Roles->assign( "org_adm_".$lOrg, "app_xqt_".$lApp);
				$pRBAC->Roles->assign( "org_adm_".$lOrg, "app_adm_".$lApp);
				$pRet['tnt'] = $lOrg;
				$pRet['app'] = $lApp;
				$pRet['rc'] = 0;
				$pRet['msg'] = "added ".$sApp." to ".$sTnt." (".$lOrg.")";
			} else $pRet['msg'] = "tenancy or profile not found";
		} else $pRet['msg'] = "both tenancy and profile must be specified";
		return( $pRet);
	}

	function apiOrgAppRemove( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/org.php");
		require_once( $pCfg['path_www']."/lib/app.php");
		$pRet = array( 'rc' => -1, 'msg' => "profile remove failure");
		$pRBAC=null;
		$lOrg = isset($pReq['id']) ? intval($pReq['id']) : 0;
		$lApp = detectOID( 'app', $pReq, 'app');
		if ( ($lOrg > 0) && ($lApp > 0)) {
			$pOrg = new Org();
			$bOrg = $pOrg->find( $lOrg);
			$pApp = new App();
			$pApp->find( $lApp);
			if ( $bOrg && ($pApp->lID == $lApp)) {
				$sApp = $pApp->getName();
				$sTnt = $pOrg->getName();
				$pApp->setTenancy( '');
				$pApp->commit();
				if ( $pRBAC == null) $pRBAC = new Rbac();
				$pRBAC->Roles->unassign( "org_adm_".$lOrg, "app_".$lApp);
				$pRBAC->Roles->unassign( "org_adm_".$lOrg, "app_xqt_".$lApp);
				$pRBAC->Roles->unassign( "org_adm_".$lOrg, "app_adm_".$lApp);
				$pRet['rc'] = 0;
				$pRet['msg'] = "removed ".$sApp." from ".$sTnt." (".$lOrg.")";
			} else $pRet['msg'] = "tenancy or profile not found";
		} else $pRet['msg'] = "both tenancy and profile must be specified";
		return( $pRet);
	}

	function apiOrgUsrAdd( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/who.php");
		require_once( $pCfg['path_www']."/lib/phprbac.php");
		$pRet=array();
		$lOrg = isset($pReq['org']) ? $pReq['org'] : 0;
		$lUsr = isset($pReq['usr']) ? $pReq['usr'] : 0;
		$sUsr = isset($pReq['login']) ? $pReq['login'] : null;
		$sAxs = isset($pReq['axs']) ? $pReq['axs'] : null;
		if ( ($lUsr == 0) || ($sUsr != null)) {
		//	Auto-create
			$pWho = new Who();
			$lUsr = $pWho->mapLogin($sUsr);
			if ( $lUsr == 0) {
				$pWho->setLogin($sUsr);
				$pWho->commit();
				$lUsr = $pWho->lID;
			}
		}
		$pRBAC = new PhpRbac\Rbac();
		$pRBAC->Users->assign( "org_".$lOrg, $lUsr);
		if ( $sAxs != null) $pRBAC->Users->assign( "org_adm_".$lOrg, $lUsr);
		return( $pRet);
	}

	function apiOrgUsrDel( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/phprbac.php");
		$pRet=array();
		$lOrg = isset($pReq['org']) ? $pReq['org'] : 0;
		$lUsr = isset($pReq['usr']) ? $pReq['usr'] : 0;
		$sAxs = isset($pReq['axs']) ? $pReq['axs'] : null;
		$pRBAC = new PhpRbac\Rbac();
		$pRBAC->Users->unassign( "org_adm_".$lOrg, $lUsr);
		if ( $sAxs == null) $pRBAC->Users->unassign( "org_".$lOrg, $lUsr);
		return( $pRet);
	}

	function apiWhoInfo( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/who.php");
		$pRet = array( 'rc' => 1, 'msg' => 'error');
		$pWho = new Who();
		$lWho = isset($pReq['id']) ? $pReq['id'] : 0;
		if ( isset($pReq['login']) && ($lWho == 0)) {
			$lWho = $pWho->mapLogin($pReq['login']);
		}
		$pWho->find( $lWho);
		if ( $pWho->lID > 0) {
			$pRet['rc'] = 0;
			$pRet['msg'] = 'ok';
			$pRet['id'] = $lWho;
			$pRet['login'] = $pWho->getLogin();
		}
		return( $pRet);
	}

	function apiWhoAdd( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/who.php");
		$pRet = array();
		$pWho = new Who();
		$pWho->setLogin( $pReq['login']);
		$pWho->setEMail( $pReq['email']);
		$pWho->setBrief( $pReq['fullname']);
		$pWho->commit();
		if ( $pWho->lID > 0) {
			$pRet['id'] = $pWho->lID;
			$pRet['login'] = $pWho->getLogin();
			$pRet['email'] = $pWho->getEMail();
			$pRet['fullname'] = $pWho->getBrief();
		}
		return( $pRet);
	}

	function apiAppList( $pReq) {
		global $pCfg;
		global $sLogin;
		$pRet = array( 'rc' => 0, 'msg' => 'OK');
		$bAdm = isset($pReq['adm']) ? true : false;
		$bSA = isset($pReq['superadmin']) ? true : false;
		$lOrg = isset($pReq['filterbyorg']) ? $pReq['filterbyorg'] : 0;
		require_once( $pCfg['path_www']."/lib/app.php");
		$pApp = new App();
		$pRet['lst'] = $bSA ? $pApp->findAll() : $pApp->findByRBAC( $sLogin, $bAdm, $lOrg);
		return( $pRet);
	}

	function apiAppInfo( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/app.php");
		$pRet = array();
		$lApp = detectOID( "app", $pReq);
		$pApp = new App();
		$pApp->find( $lApp);
		if ( $pApp->lID > 0) {
			$pRet['id'] = $pApp->lID;
			$pRet['usr'] = $pApp->getUID();
			$pRet['name'] = $pApp->getName();
			$pRet['label'] = $pApp->getLabel();
			$pRet['cmdb'] = $pApp->getCI();
			$pRet['ask'] = $pApp->getAskID();
			$pRet['fin'] = $pApp->getGLCode();
			$pRet['job'] = $pApp->getJob();
			if ( $pApp->isPublic()) $pRet['public']=true;
		}
		return( $pRet);
	}

	function apiAppAdd( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/app.php");
		$pRet = array();
		$pApp = new App();
		$sApp = isset($pReq['name']) ? $pReq['name'] : '';
		if ( $sApp != '') {
			$pApp->setName( $sApp);
			$pApp->setLabel( $pReq['label']);
			$pApp->setUser( $sLogin);
			if ( isset($pReq['cmdb'])) $pApp->setCI( $pReq['cmdb']);
			if ( isset($pReq['fin'])) $pApp->setGLCode($pReq['fin']);
			$pApp->commit();
			$lApp = $pApp->lID;
			if ( $lApp > 0) {
				$sCI = $pApp->mapCI();
				$pLogNfo = array("id"=>$lApp,"obj"=>"profile","name"=>$sApp,"who"=>$sLogin);
				islEventLog( 'sys.starling.'.$sCI.'.create', $pLogNfo);
				islEventLog( 'sys.starling.create', $pLogNfo);
				$pRet['id'] = $pApp->lID;
				$pRet['name'] = $sApp;
				$pRet['label'] = $pApp->getLabel();
				if ( $pApp->isPublic()) $pRet['public']=true;
			}
		}
		return( $pRet);
	}

	function apiAppUpdate( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/app.php");
		$pRet = array( 'rc' => 1, 'msg' => "failure to locate profile");
		$lApp = detectOID( "app", $pReq);
		if ( $lApp > 0) {
			require_once( $pCfg['path_www']."/lib/app.php");
			$pApp = new App();
			$pApp->find( $lApp);
			if ( $pApp->lID == $lApp) {
				if ( isset($pReq['usr'])) $pApp->setUID( $pReq['usr']);
				if ( isset($pReq['name'])) $pApp->setName( $pReq['name']);
				if ( isset($pReq['label'])) $pApp->setLabel( $pReq['label']);
				if ( isset($pReq['cmdb'])) $pApp->setCI( $pReq['cmdb']);
				if ( isset($pReq['ask'])) $pApp->setAskID( $pReq['ask']);
				if ( isset($pReq['fin'])) $pApp->setGLCode( $pReq['fin']);
				if ( isset($pReq['job'])) $pApp->setJob( $pReq['job']);
				if ( isset($pReq['public'])) $pApp->setPublic( $pReq['public']);
				$pApp->commit();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			}
		}
		return( $pRet);
	}

	function apiAppDelete( $pReq) {
		global $pCfg;
		$pRet = array( 'rc' => 1, 'msg' => "failure to retrieve profile info");
		$lApp = detectOID( "app", $pReq);
		if ( $lApp > 0) {
			require_once( $pCfg['path_www']."/lib/app.php");
			$pApp = new App();
			$pApp->find( $lApp);
			if ( $pApp->lID == $lApp) {
				$pApp->remove();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			}
		}
		return( $pRet);
	}

	function apiAppUsrList( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/starlingrbac.php");
	//	error_log( "DBG=>>> API - appusrlst for ".$lApp);
		$lApp = detectOID( 'app', $pReq, 'app');
		$pRet = findAppUsers( $lApp);
		return( $pRet);
	}

	function apiAppUsrAdd( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/phprbac.php");
		$pRet=array();
		$pRBAC = new PhpRbac\Rbac();
		$lApp = isset($pReq['app']) ? $pReq['app'] : 0;
		$lUsr = isset($pReq['usr']) ? $pReq['usr'] : 0;
		$sAxs = isset($pReq['axs']) ? $pReq['axs'] : null;
		$sRole = ($sAxs == "usr") ? "app_".$lApp : "app_".$sAxs."_".$lApp;
	//	error_log( "DBG=>>> API - appusradd ".$lUsr." -> ".$sRole);
		$pRBAC->Users->assign( $sRole, $lUsr);
		return( $pRet);
	}

	function apiAppUsrDel( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/phprbac.php");
		$pRet=array();
		$pRBAC = new PhpRbac\Rbac();
		$lApp = isset($pReq['app']) ? $pReq['app'] : 0;
		$lUsr = isset($pReq['usr']) ? $pReq['usr'] : 0;
		$sAxs = isset($pReq['axs']) ? $pReq['axs'] : null;
		$sRole = ($sAxs == "usr") ? "app_".$lApp : "app_".$sAxs."_".$lApp;
	//	error_log( "DBG=>>> API - appusrdel ".$lUsr." -> ".$sRole);
		$pRBAC->Users->unassign( $sRole, $lUsr);
		return( $pRet);
	}

	function apiDetectJob( $pReq) {
		global $pCfg;
		$pRet = array( 'rc' => 0, 'msg' => "no active job detected");
		$lApp = isset($pReq['id']) ? intval($pReq['id']) : 0;
	//	error_log( "DBG=>>> apiDetectJob invoked with app ID=".$lApp);
		if ( $lApp > 0) {
			require_once( $pCfg['path_www']."/lib/tst.php");
			$pTst = new Tst();
			$pLst = $pTst->findByApp( $lApp, true);
			if ( count( $pLst) > 0) {
				require_once( $pCfg['path_www']."/lib/app.php");
				require_once( $pCfg['path_www']."/lib/pln.php");
				reset( $pLst);
				$pTst->find( key($pLst));
				$pPlan = new Pln();
				$pApp = new App();
				$pPlan->find( $pTst->getPlan());
				$pApp->find( $pPlan->getApp());
				$sEngine = ($pPlan->lID > 0) ? $pPlan->getEngine() : "xaas";
				$sFramework = ($pPlan->lID > 0) ? $pPlan->getFramework() : "jmeter";
				$sGit = ($pPlan->lID > 0) ? $pPlan->getRepository() : "";
				$sBranch = ($pPlan->lID > 0) ? $pPlan->getBranch() : "";
				$sRefDir = ($pPlan->lID > 0) ? $pPlan->getRefDir() : "";
				$sTenant = ($pPlan->lID > 0) ? $pPlan->getTenant() : "perftest";
				$sDoc = ($pPlan->lID > 0) ? $pPlan->getDoc() : "starling.jmx";
				$sPlan = ($pPlan->lID > 0) ? $pPlan->getName() : "default";
				$sCI = $pApp->mapCI();
				$pRet['tst'] = intval( array_keys($pLst)[0]);
				$pRet['eid'] = $pTst->getEID();
				$pRet['status'] = $pTst->getStatus();
				$pRet['cmdb'] = $sCI;
				$pRet['pln'] = $pPlan->lID;
				$pRet['ngn'] = $sEngine;
				$pRet['frm'] = $sFramework;
				$pRet['git'] = $sGit;
				$pRet['gbn'] = $sBranch;
				$pRet['grd'] = $sRefDir;
				$pRet['tnt'] = $sTenant;
				$pRet['doc'] = $sDoc;
				$pRet['api'] = $pTst->getApi();
				$pRet['lgs'] = $pPlan->getLGs();
				$pRet['cpu'] = $pPlan->getCores();
				$pRet['mmx'] = $pPlan->getMMX();
				$pRet['mms'] = $pPlan->getMMS();
				$pRet['vusr'] = $pTst->getVUsers();
				$pRet['created'] = $pTst->getCreated();
				$pRet['rc'] = 0;
				$pRet['msg'] = $sPlan;
			}
		}
	//	error_log( "TRC=>>> apiDetectJob: ".var_export($pRet,true));
		return( $pRet);
	}

	function apiPlanList( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/pln.php");
		$lApp = detectOID( 'app', $pReq);
		$pPln = new Pln();
		$pRet = $pPln->findByApp( $lApp);
		return( $pRet);
	}

	function apiPlanInfo( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pRet = array( 'rc' => 1, 'msg' => "failure to retrieve plan info");
		$lPln=0;
		$lPln = detectOID( 'pln', $pReq);
	//	error_log( "DBG=>>> detectOID returns plan -> ".$lPln);
		if ( $lPln > 0) {
			$pPln = new Pln();
			$pPln->find( $lPln);
			if ( $pPln->lID == $lPln) {
				$pRet['id'] = $lPln;
				$pRet['app'] = $pPln->getApp();
				$pRet['name'] = $pPln->getName();
				$pRet['ngn'] = $pPln->getEngine();
				$pRet['syn'] = $pPln->getSyn();
				$pRet['dtn'] = $pPln->getDuration();
				$pRet['rui'] = $pPln->getRampUpInterval();
				$pRet['lgs'] = $pPln->getLGs();
				$pRet['vusr'] = $pPln->getVUsers();
				$pRet['cpu'] = $pPln->getCores();
				$pRet['mmx'] = $pPln->getMMX();
				$pRet['mms'] = $pPln->getMMS();
				$pRet['frm'] = $pPln->getFramework();
				$pRet['doc'] = $pPln->getDoc();
				$pRet['git'] = $pPln->getRepository();
				$pRet['gbn'] = $pPln->getBranch();
				$pRet['grd'] = $pPln->getRefDir();
				$pRet['vis'] = $pPln->getVis();
				$pRet['xqt'] = $pPln->getExe();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			}
		}
		return( $pRet);
	}

	function apiPlnUpdate( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pRet = array( 'rc' => 1, 'msg' => "plan update failed");
		$lRet = 0;
		$lPln = detectOID( "pln", $pReq);
		$sXqt = isset($pReq['xqt']) ? $pReq['xqt'] : null;
		$sTnt = isset($pReq['tnt']) ? $pReq['tnt'] : null;
		$sName = isset($pReq['name']) ? $pReq['name'] : null;
		$sSyn = isset($pReq['syn']) ? $pReq['syn'] : null;
		$sFrm = isset($pReq['frm']) ? $pReq['frm'] : null;
		$sRepo = isset($pReq['git']) ? $pReq['git'] : null;
		$sBranch = isset($pReq['gbn']) ? $pReq['gbn'] : null;
		$sRefDir = isset($pReq['grd']) ? $pReq['grd'] : null;
		$sDoc = isset($pReq['doc']) ? $pReq['doc'] : null;
		$sVis = isset($pReq['vis']) ? $pReq['vis'] : null;
		$lVUsers = isset($pReq['vusr']) ? intval($pReq['vusr']) : null;
		$sDTN = isset($pReq['dtn']) ? $pReq['dtn'] : null;
		$sRUI = isset($pReq['rui']) ? $pReq['rui'] : null;
		$iLGs = isset($pReq['lgs']) ? intval($pReq['lgs']) : null;
		$sCPUs = isset($pReq['cpu']) ? $pReq['cpu'] : null;
		$iMMX = isset($pReq['mmx']) ? intval($pReq['mmx']) : 3072;
		$iMMS = isset($pReq['mms']) ? intval($pReq['mms']) : 1024;
		$pPln = new Pln();
		$pPln->find( $lPln);
		if ( $pPln->lID > 0) {
			if ( $sTnt != null) $pPln->setTenant( $sTnt);
			if ( $sName != null) $pPln->setName( $sName);
			if ( $sFrm != null) $pPln->setFramework( $sFrm);
			if ( $sSyn != null) $pPln->setSyn( $sSyn);
			if ( $sRepo != null) $pPln->setRepository( $sRepo);
			if ( $sBranch != null) $pPln->setBranch( $sBranch);
			if ( $sRefDir != null) $pPln->setRefDir( $sRefDir);
			if ( $sDoc != null) $pPln->setDoc( $sDoc);
			if ( $sVis != null) $pPln->setVis( $sVis);
			if ( $lVUsers != null) $pPln->setVUsers( $lVUsers);
			if ( $iLGs != null) $pPln->setLGs( $iLGs);
			if ( $sCPUs != null) $pPln->setCores( $sCPUs);
			if ( $iMMS != null) $pPln->setMMS( $iMMS);
			if ( $iMMX != null) $pPln->setMMX( $iMMX);
			if ( $sXqt != null) $pPln->setExe($sXqt);
			if ( $sDTN != null) $pPln->setDuration( $sDTN);
			if ( $sRUI != null) $pPln->setRampUpInterval( $sRUI);
			$pPln->commit();
			$lRet = $pPln->lID;
			$pRet['pln']=$lRet;
			$pRet['rc']=0;
			$pRet['msg']="ok";
		}
		return( $pRet);
	}

	function apiPlnAdd( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/app.php");
		$pRet = array();
		$pOID = array();
		if ( isset($pReq['app'])) $pOID['id']=$pReq['app'];
		if ( isset($pReq['eid'])) $pOID['eid']=$pReq['eid'];
		if ( isset($pReq['tnt'])) $pOID['tnt']=$pReq['tnt'];
		$lApp = detectOID( 'app', $pOID);
		$lPln = isset($pReq['pln']) ? intval($pReq['pln']) : 0;
		$sName = isset($pReq['name']) ? $pReq['name'] : '';
		$sTnt = isset($pReq['tnt']) ? $pReq['tnt'] : '';
		$sSyn = isset($pReq['syn']) ? $pReq['syn'] : null;
		$sFrm = isset($pReq['frm']) ? $pReq['frm'] : 'jmeter';
		$sRepo = isset($pReq['git']) ? $pReq['git'] : '';
		$sBranch = isset($pReq['gbn']) ? $pReq['gbn'] : '';
		$sRefDir = isset($pReq['grd']) ? $pReq['grd'] : '';
		$sDoc = isset($pReq['doc']) ? $pReq['doc'] : 'starling.jmx';
		$sVis = isset($pReq['vis']) ? $pReq['vis'] : '';
		$lVUsers = isset($pReq['vusr']) ? intval($pReq['vusr']) : 2;
		$iLGs = isset($pReq['lgs']) ? intval($pReq['lgs']) : 1;
		$sCores = isset($pReq['cpu']) ? $pReq['cpu'] : "0.256";
		$iMMS = isset($pReq['mms']) ? intval($pReq['mms']) : 1024;
		$iMMX = isset($pReq['mmx']) ? intval($pReq['mmx']) : 3072;
		$pApp = new App();
		$pApp->find( $lApp);
		$sCI = $pApp->mapCI();
		if ( $sTnt == '') $sTnt = ($pApp->lID > 0) ? $pApp->getTenancy() : 'epe';
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pPln = new Pln();
		if ( $lPln > 0) $pPln->setEID( $lPln);
		$pPln->setApp( $lApp);
		$pPln->setName( $sName);
		$pPln->setTenant( $sTnt);
		$pPln->setFramework( $sFrm);
		if ( $sSyn != null) $pPln->setSyn( $sSyn);
		$pPln->setRepository( $sRepo);
		$pPln->setBranch( $sBranch);
		$pPln->setRefDir( $sRefDir);
		$pPln->setDoc( $sDoc);
		$pPln->setVis( $sVis);
		$pPln->setVUsers( $lVUsers);
		$pPln->setLGs( $iLGs);
		$pPln->setCores( $sCores);
		$pPln->setMMS( $iMMS);
		$pPln->setMMX( $iMMX);
		if ( isset($pReq['xqt'])) $pPln->setExe($pReq['xqt']);
		$pPln->commit();
		$pRet['id'] = $pPln->lID;
		$pRet['rc'] = 0;
		$pRet['msg'] = 'ok: added '.$sName;
		$pLogNfo = array("id"=>$pPln->lID,"obj"=>"plan","name"=>$sName);
		islEventLog( 'sys.starling.'.$sCI.'.create', $pLogNfo);
		islEventLog( 'sys.starling.create', $pLogNfo);
		return( $pRet);
	}

	function apiPlnDel( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pRet = array( 'rc' => 1, 'msg' => "plan not specified or found");
		$lPln = detectOID( 'pln', $pReq);
		$pPln = new Pln();
		$pPln->find( $lPln);
		if ( $pPln->lID > 0) {
			$lNegApp = 0 - $pPln->getApp();
			$pPln->setApp( $lNegApp);
			$pPln->commit();
			$pRet['rc'] = 0;
			$pRet['msg'] = sprintf( "plan %d marked for removal", $lPln);
		}
		return( $pRet);
	}

	function apiTestInfo( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pRet = array( 'rc' => 1, 'msg' => "failure to retrieve test info");
		$lTst=0;
		$lTst = detectOID( "tst", $pReq);
		if ( $lTst > 0) {
			$pTst = new Tst();
			$pTst->find( $lTst);
			if ( $pTst->lID == $lTst) {
				$pRet['id'] = $lTst;
				$lEID = $pTst->getEID();
				$pRet['eid'] = ($lEID != null) ? intval($lEID) : 0;
				$pRet['created'] = $pTst->getCreated();
				$pRet['pln'] = $pTst->getPlan();
				$pRet['status'] = $pTst->getStatus();
				$pRet['name'] = $pTst->getName();
				$pRet['jobname'] = $pTst->getJobName();
				$pRet['rpturl'] = $pTst->getURL();
				$pRet['brief'] = $pTst->getBrief();
				$pRet['vusr'] = $pTst->getVUsers();
				$pRet['lgs'] = $pTst->getLGs();
				$pRet['cpu'] = $pTst->getCores();
				$pRet['mms'] = $pTst->getMMS();
				$pRet['mmx'] = $pTst->getMMX();
				$pRet['xqt'] = $pTst->getExe();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			}
		}
		return( $pRet);
	}

	function apiTestUpdate( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pRet = array( 'rc' => 1, 'msg' => "failure to locate test");
		$lTst = detectOID( "tst", $pReq);
		$lTst = isset($pReq['id']) ? $pReq['id'] : 0;
		if ( $lTst > 0) {
			require_once( $pCfg['path_www']."/lib/app.php");
			require_once( $pCfg['path_www']."/lib/pln.php");
			require_once( $pCfg['path_www']."/lib/evt.php");
			$pTst = new Tst();
			$pPln = new Pln();
			$pApp = new App();
			$pTst->find( $lTst);
			if ( $pTst->lID == $lTst) {
				$sRptURL = isset($pReq['url']) ? $pReq['url'] : '';
				$bLogOK = ($pTst->getStatus() == 2) ? true : false;
				$pPln->find( $pTst->getPlan());
				$pApp->find( $pPln->getApp());
				$sCI = $pApp->mapCI();
				if ( $sRptURL != '') $pTst->setURL( $sRptURL);
				$pMsg = array( 'rptURL' => $sRptURL);
				islEventLog( 'console.starling.'.$sCI.'.test.'.strval($lTst), $pMsg, false);
				$pTst->setStatus( 0);
				$pTst->setCompletion();
				$pTst->commit();
				$sJob = $pApp->getJob();
				$sNgn = $pPln->getEngine();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
				$pRet['app'] = array();
				$pRet['app']['id'] = $pApp->lID;
				$pRet['app']['pln'] = array();
				$pRet['app']['usr'] = $pApp->getUID();
				$pRet['app']['name'] = $pApp->getName();
				$pRet['app']['label'] = $pApp->getLabel();
				$pRet['app']['job'] = $sJob;
				$pRet['app']['pln']['tst'] = array();
				$pRet['app']['pln']['id'] = $pPln->lID;
				$pRet['app']['pln']['created'] = $pPln->getCreated();
				$pRet['app']['pln']['syn'] = $pPln->getSyn();
				$pRet['app']['pln']['ngn'] = $sNgn;
				$pRet['app']['pln']['frm'] = $pPln->getFramework();
				$pRet['app']['pln']['git'] = $pPln->getRepository();
				$pRet['app']['pln']['gbn'] = $pPln->getBranch();
				$pRet['app']['pln']['grd'] = $pPln->getRefDir();
				$pRet['app']['pln']['tnt'] = $pPln->getTenant();
				$pRet['app']['pln']['name'] = $pPln->getName();
				$pRet['app']['pln']['lgs'] = $pPln->getLGs();
				$pRet['app']['pln']['vusr'] = $pPln->getVUsers();
				$pRet['app']['pln']['doc'] = $pPln->getDoc();
				$pRet['app']['pln']['vis'] = $pPln->getVis();
				$pRet['app']['pln']['tst']['id'] = $pTst->lID;
				$pRet['app']['pln']['tst']['created'] = $pTst->getCreated();
				$pRet['app']['pln']['tst']['name'] = $pTst->getName();
				$pRet['app']['pln']['tst']['vusr'] = $pTst->getVUsers();
				$pRet['app']['pln']['tst']['sts'] = $pTst->getStatus();
				$pRet['app']['pln']['tst']['url'] = $pTst->getURL();
			//	$pEvt = new Evt();
			//	$pEvt->setRef( $pPln->lID);
			//	$sDesc = "completion: ".$pPln->getName();
			//	$pEvt->logEvt( 109, $pTst->lID, $sDesc);
				$pRpt = $pTst->genRpt();
				if ( $bLogOK) {
					islEventLog( 'report.starling.'.$sCI.'.job', $pRpt, false);
					islEventLog( 'report.starling.job', $pRpt, false);
				}
				if ( $sNgn == 'xaas') {
					$sCmd = sprintf( "sudo -H -u %s %s/isllgs --%s=%d %s", $pCfg['unix_login'], $pCfg['unix_path'], 'release', $lTst, "2>/dev/null");
				}
			}
		}
		return( $pRet);
	}

	function launchTest( $pApp, $pPln, $sUsr, $lWho, $lEID=0) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/app.php");
		require_once( $pCfg['path_www']."/lib/pln.php");
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pRet = array( 'rc' => 1, 'msg' => "\nfailure to launch test");
		if ( ($pApp != null) && ($pPln != null)) {
			$pTst = new Tst();
			$sCI = $pApp->mapCI();
			$sPlan = $pPln->getName();
			$sFabric = $pPln->getEngine();
		//	$pEvt = new Evt();
		//	$pEvt->setWho( $pWho->mapLogin( $sUsr));
		//	$pEvt->setRef( $pPln->lID);
			if ( $lEID > 0) $pTst->setEID( $lEID);
			$pTst->setApp( $pApp->lID);
			$pTst->setPlan( $pPln->lID);
			$pTst->setWho( $lWho);
			$pTst->setStatus( 2);
			$pTst->setApi( session_id());
			$pTst->setName( $sPlan." (".$pPln->getDoc()." x ".$sUsr.")");
			$pTst->setVUsers( $pPln->getVUsers());
			$pTst->setNfo( $pPln->getNfo());
			$pTst->setExe( $pPln->getExe());
			$pTst->setBrief( "Powered by Starling");
			$pTst->commit();
			if ( isset($pCfg['url_rpt'])) {
			//	This is done here with a commit in anticipation of
			//	eventual scenarios where it may be desirable to add
			//	the actual test ID to the URL
				$pTst->setURL( $pCfg['url_rpt']);
				$pTst->commit();
			}
			$sDesc = "execution: ".$sPlan." x ".$sUsr;
		//	$pEvt->logEvt( 108, $pTst->lID, $sDesc);
			islEventLog( 'sys.starling.'.$sCI.'.run', array("id"=>$pTst->lID,"job"=>"test","name"=>$sDesc));
			$pRet['id'] = $pTst->lID;
		//	$sLaunchOp = ($sFabric == "k8s") ? "launchk8s" : "launchxaas";
			$sLaunchOp = ($sFabric == "k8s") ? "launchxaas" : "launchxaas";
			if ($sFabric == "origin") $sLaunchOp = "launch";
		//	$bMaven = ($pPln->getFramework() == "maven") ? true : false;
		//	$sJob = ($bOpenShift && !$bMaven) ? $pApp->getName() : $pApp->getJob();
			$sJob = $pApp->getJob();
			$sCmd = sprintf( "sudo -H -u %s %s/starling --%s=%s %s", $pCfg['unix_login'], $pCfg['unix_path'], $sLaunchOp, $sJob, "2>/dev/null");
		//	error_log( "DBG=>>> launching with -> ".$sCmd);
			$sCSV = shell_exec( $sCmd);
			$pRet['id'] = $pTst->lID;
			$pRet['name'] = $pTst->getName();
			$pRet['doc'] = $pPln->getDoc();
			$pRet['rc'] = 0;
			$pRet['msg'] = "\nload generation spinning up - please stand by ...";
		} else error_log( "ERR=>>> launch attempted with invalid app or pln");
		return($pRet);
	}

	function apiTestLaunch( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/pln.php");
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pRet = array( 'rc' => 1, 'msg' => "\nfailure to retrieve test plan");
		$sSuspend = isset($pCfg['suspend']) ? $pCfg['suspend'] : '';
		$lPln = detectOID( "pln", $pReq);
		$sUsr = isset($pReq['login']) ? $pReq['login'] : $sLogin;
		$lTst = isset($pReq['tst']) ? intval($pReq['tst']) : 0;
		if ( $sSuspend == '') {
			if ( $lPln > 0) {
				$pPln = new Pln();
				$pPln->find( $lPln);
				if ( $pPln->lID == $lPln) {
					$lApp = $pPln->getApp();
					$pTst = new Tst();
					$pLst = $pTst->findByApp( $lApp, true);
					if ( count($pLst) > 0) {
						$pRet['rc']=1;
						$pRet['msg'] = "only one consecutive run per profile is allowed";
						return($pRet);
					}
					$pChk = launchAccess($lPln,$lApp);
					if ( !isset($pChk['has'])) {
						$pRet['rc']=1;
						$pRet['msg'] = "no permission to execute plan";
						return($pRet);
					}
					$sFabric = $pPln->getEngine();
					if ( $sFabric != "garden") {
						require_once( $pCfg['path_www']."/lib/app.php");
						require_once( $pCfg['path_www']."/lib/evt.php");
						require_once( $pCfg['path_www']."/lib/who.php");
						$pWho = new Who();
						$pApp = new App();
						$pApp->find( $lApp);
						if ( $pApp->lID > 0) {
							$pRet = $this->launchTest($pApp,$pPln,$sUsr,$pWho->mapLogin($sUsr),$lTst);
						} else $pRet['msg'] = "\ntest plan orphaned from app";
					} else { $pRet['rc']=3 ; $pRet['msg'] = "\nhealthcheck not allowed from workbench"; }
				} else $pRet['rc'] = 2;
			} else $pRet['msg'] = "\nno test plan specified";
		} else $pRet['msg'] = "\n".$sSuspend;
		return( $pRet);
	}

	function apiTestCancel( $pReq) {
		global $pCfg;
		global $sLogin;
		$bRet = false;
		$pRet = array( 'rc' => 1, 'msg' => "no app profile specified");
		require_once( $pCfg['path_www']."/lib/app.php");
		$sUsr = isset($pReq['login']) ? $pReq['login'] : $sLogin;
		$lApp = detectOID( "app", $pReq);
		$pApp = new App();
		$pApp->find( $lApp);
		if ( $pApp->lID > 0) {
			require_once( $pCfg['path_www']."/lib/tst.php");
			$pTst = new Tst();
			$pLst = $pTst->findByApp( $lApp, true);
			$lJob = (count($pLst) > 0) ? array_keys($pLst)[0] : 0;
			if ($lJob > 0) {
				$pTst->find( $lJob);
				$pTst->setStatus( -1);
				$pTst->commit();
			//	Log the cancellation event
				require_once( $pCfg['path_www']."/lib/evt.php");
				require_once( $pCfg['path_www']."/lib/who.php");
				$pWho = new Who();
			//	$pEvt = new Evt();
			//	$pEvt->setWho( $pWho->mapLogin( $sUsr));
			//	$pEvt->setRef( $pTst->getPlan());
				$sDesc = "cancellation: ".$sUsr." => ".$pTst->getName();
			//	$pEvt->logEvt( 102, $pTst->lID, $sDesc);
				$sCI = $pApp->mapCI();
				$pRpt = $pTst->genRpt();
				islEventLog( 'report.starling.'.$sCI.'.job', $pRpt, false);
				islEventLog( 'report.starling.job', $pRpt, false);
			//	Forward cancellation to the load gen fabric
				$sJob = $pApp->getJob();
				$sCmd = sprintf( "sudo -H -u %s %s/starling --cancel=%s %s &", $pCfg['unix_login'], $pCfg['unix_path'], $sJob, "2>/dev/null");
				$sCSV = shell_exec( $sCmd);
				sleep( 4);
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			} else $pRet['msg'] = "running job not detected";
		}
		return( $pRet);
	}

	function apiTestDelete( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pRet = array( 'rc' => 1, 'msg' => "failure to retrieve test info");
		$lTst = detectOID( "tst", $pReq);
		if ( $lTst > 0) {
			require_once( $pCfg['path_www']."/lib/tst.php");
			$pTst = new Tst();
			$pTst->find( $lTst);
			if ( $pTst->lID == $lTst) {
				$pTst->markDeleted();
				$pRet['rc'] = 0;
				$pRet['msg'] = "ok";
			}
		}
		return( $pRet);
	}

	function apiTestConsole( $pReq) {
		global $pCfg;
		$pRet = array( 'rc'=>1,'msg'=>"failure to locate test",'txt'=>"");
		$sZooKeeper = isset($pCfg['zookeeper']) ? "--host=".$pCfg['zookeeper'] : null;
		$lTst = detectOID( "tst", $pReq);
		$lSeg = isset($pReq['seg']) ? intval($pReq['seg']) : 0;
		if ( $lTst > 0) {
			require_once( $pCfg['path_www']."/lib/tst.php");
			$pTst = new Tst();
			$pTst->find( $lTst);
			if ( $pTst->lID == $lTst) {
				$lSegC=$lSeg;
				$sTxt = ($lSeg == 1) ? "test is active ... " : ".";
				$sTxt = "";
				if ( $sZooKeeper == "--host=default") $sZooKeeper = "";
				$sCmd = sprintf( "sudo -H -u %s %s/tstcon %ld %ld %s", "starling", "/home/starling/bin", $lTst, $lSeg, "2>/dev/null");
			//	error_log( "DBG=>>> running: ".$sCmd);
				$sLines = array();
				exec( $sCmd, $sLines);
				foreach ( $sLines as $sLine) {
					$sTxt .= $sLine."\n";
					$lSegC = $lSegC + 1;
				}
				$pRet['txt'] = $sTxt;
				$pRet['seg'] = $lSegC;
			} else $pRet['txt'] = "test is no longer active\n";
			$pRet['rc'] = 0;
			$pRet['msg'] = "ok";
		}
		return( $pRet);
	}

	function apiTestConsole_zookeeper( $pReq) {
		global $pCfg;
		$pRet = array( 'rc'=>1,'msg'=>"failure to locate test",'txt'=>"");
		$sZooKeeper = isset($pCfg['zookeeper']) ? "--host=".$pCfg['zookeeper'] : null;
		$lTst = isset($pReq['id']) ? intval($pReq['id']) : 0;
		$lSeg = isset($pReq['seg']) ? intval($pReq['seg']) : 0;
		if ( $lTst > 0) {
			require_once( $pCfg['path_www']."/lib/tst.php");
			$pTst = new Tst();
			$pTst->find( $lTst);
			if ( $pTst->lID == $lTst) {
				$lSegC=$lSeg;
			/*	Old Jenkins console ... needs to be refactored
				$sTxt = $pTst->getPage();
				$iTxt = ($sTxt != null) ? strlen( $sTxt) : 0;
				if ( $iTxt > 0) {
					$pRet['txt'] = $sTxt."-------> ".date( "Y-m-d H:i:s")." <-------\n";
					$iTxt = $iTxt + $pTst->getPageOffset();
					$pTst->setPage( "");
					$pTst->setPageOffset( $iTxt);
					$pTst->commit();
				} else {
					$sCmd = sprintf( "sudo -H -u %s %s/starling --app=%d --console=%d%s", "starling", "/home/starling/bin", intval($pTst->getApp()), intval($pTst->getJob()), " 1>/dev/null 2>/dev/null &");
				// The python does the DB update, so we don't need the entire text
				//	$sTxt = shell_exec( $sCmd);
				//	$pRet['txt'] = $sTxt."(as of ".date("Y-m-d H:i:s").")\n";
					exec( $sCmd);
				} */
				$sTxt = ($lSeg == 1) ? "test is active ... " : ".";
				if ( $sZooKeeper != null) {
					$sTxt = "";
					if ( $sZooKeeper == "--host=default") $sZooKeeper = "";
					$sCmd = sprintf( "sudo -H -u %s %s/tstcon %s --view --idx=%ld %ld %s", "starling", "/home/starling/bin", $sZooKeeper, $lSeg, $lTst, "2>/dev/null");
				//	error_log( "DBG=>>> running: ".$sCmd);
					$sLines = array();
					exec( $sCmd, $sLines);
					foreach ( $sLines as $sLine) {
						$pMs = array();
						if ( preg_match( "/^[0-9][[0-9]*:=>.*/", $sLine, $pMs, PREG_OFFSET_CAPTURE) == 1) {
							$pTks = explode( ":", $sLine, 2);
							$lSegC = intval($pTks[0]);
							$sTxt .= "\n".$pTks[1];
						} else $sTxt .= $sLine;
					}
				} else if ( ($lSeg % 10) == 0) { $sTxt = ".\ntest remains active ... "; }
				$pRet['txt'] = $sTxt;
				$pRet['seg'] = $lSegC+1;
			} else $pRet['txt'] = "test is no longer active\n";
			$pRet['rc'] = 0;
			$pRet['msg'] = "ok";
		}
		return( $pRet);
	}

	function apiCalList( $pReq) {
		global $pCfg;
		$pRet = array( 'rc' => 1, 'msg' => "profile not found");
		$lApp = intval($pReq['id']);
		$bSparse = isset($pReq['sparse']) ? $pReq['sparse'] : true;
		require_once( $pCfg['path_www']."/lib/cal.php");
		$pCal = new Cal();
		if ( $lApp > 0) {
			require_once( $pCfg['path_www']."/lib/app.php");
			require_once( $pCfg['path_www']."/lib/pln.php");
			$pApp = new App();
			$pPln = new Pln();
			$pApp->find( $lApp);
		//	Active list
			if ( !$bSparse) {
				$pRet['active'] = array();
				$pLst = $pCal->findByApp( $lApp, true);
				foreach ( $pLst as $lCal => $sCal) {
					$pCal->find( $lCal);
					$lPln = $pCal->getPlan();
					$pPln->find( $lPln);
					$sPln = $pPln->getName();
					$sCI = $pApp->mapCI();
					$pNfo = array(	'name'=>$sCal,'pln'=>$lPln,'planname'=>$sPln,'when'=>$pCal->getCron(),'cmdb'=>$sCI);
					$pRet['active'][$lCal] = $pNfo;
				}
			} else $pRet['active'] = $pCal->findByApp( $lApp, true);
		//	Inactive list
			if ( !$bSparse) {
				$pRet['inactive'] = array();
				$pLst = $pCal->findByApp( $lApp, false);
				foreach ( $pLst as $lCal => $sCal) {
					$pCal->find( $lCal);
					$lPln = $pCal->getPlan();
					$pPln->find( $lPln);
					$sPln = $pPln->getName();
					$sCI = $pApp->mapCI();
					$pNfo = array(	'name'=>$sCal,'pln'=>$lPln,'planname'=>$sPln,'when'=>$pCal->getCron(),'cmdb'=>$sCI);
					$pRet['inactive'][$lCal] = $pNfo;
				}
			} else $pRet['inactive'] = $pCal->findByApp( $lApp, false);
		#	$pRet['past'] = $pCal->findByApp( $lApp, true, true);
			$pRet['rc'] = 0;
			$pRet['msg'] = "ok";
		} else $pRet = $pCal->findByStatus();
		return( $pRet);
	}

	function apiCalInfo( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/cal.php");
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pRet = array( 'rc' => 1, 'msg' => "no active job detected");
		$lCal = isset($pReq['id']) ? intval($pReq['id']) : 0;
		$pCal = new Cal();
		$pCal->find( $lCal);
		$lCal = $pCal->lID;
		if ( $lCal > 0) {
			$pRet['rc']=0;
			$pRet['wip']=$pCal->getWIP();
			$pRet['cts']=$pCal->getCron();
			$pRet['email']=$pCal->getEMail();
			$pRet['cmdb']=$pCal->mapCI();
			$lPln = $pCal->getPlan();
			$pPln = new Pln();
			$pPln->find( $lPln);
			if ( $pPln->lID > 0) {
				$pRet['pln']=$lPln;
				$pRet['ngn']=$pPln->getEngine();
				$pRet['frm']=$pPln->getFramework();
				$pRet['syn']=$pPln->getSyn();
				$pRet['msg']="job: ".$pCal->getName()." (".$lCal.")";
			} else $pRet['msg'] = "job ".$lCal." plan (".$lPln.") not found";
		} else $pRet['msg'] = "job ".$lCal." not scheduled";
		return( $pRet);
	}

	function apiCalAdd( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/cal.php");
		require_once( $pCfg['path_www']."/lib/pln.php");
		$pRet = array( 'rc' => 1, 'msg' => "scheduling failure");
	//	error_log( "DBG=>>> apiCalAdd ...");
		$lPln = isset( $pReq['pln']) ? $pReq['pln'] : 0;
		$sWhen = isset( $pReq['when']) ? $pReq['when'] : null;
		$sName = isset( $pReq['name']) ? $pReq['name'] : null;
		$pWhen = array( "cron"=>$sWhen);
		$pPln = new Pln();
		$pPln->find( $lPln);
		$lPln = $pPln->lID;
		if ( ($lPln > 0) && ($pWhen != null)) {
			if ( $sName == null) $sName = $pPln->getName();
			require_once( $pCfg['path_www']."/lib/starlingrbac.php");
			require_once( $pCfg['path_www']."/lib/who.php");
			$pWho = new Who();
		//	error_log( "DBG=>>> apiCalAdd - scheduling ...");
			$sWho = isset( $pReq['who']) ? $pReq['who'] : $sLogin;
			$lWho = ($sWho != null) ? $pWho->mapLogin( $sWho) : 0;
		//	$lCal = addCalEvt( $lPln, $pWhen, $lWho);
			$pCal = new Cal();
		//	error_log( "DBG=>>> apiCalAdd - adding: ".$lPln.", ".$sWhen.", ".$lWho." ...");
			$lCal = $pCal->addEvent( $lPln, $sName, $pWhen, $lWho);
			if ( $lCal > 0) {
				$pRet['id']=$lCal;
				$pRet['rc']=0;
				$pRet['msg']="scheduled: ".$sName." (".$lPln.")";
			} else $pRet['msg'] = "test plan ".$lPln." not scheduled";
		} else $pRet['msg'] = "plan/when not specified correctly";
		return( $pRet);
	}

	function apiCalUpdate( $pReq) {
		global $pCfg;
		$pRet = array( 'rc' => 1, 'msg' => "schedule update failure");
	//	error_log( "DBG=>>> apiCalUpdate ...");
	//	foreach ( $pReq as $sKey => $pVal) error_log( "DBG=>>> ".$sKey." => ".$pVal);
		$lCal = isset($pReq['id']) ? intval($pReq['id']) : 0;
		$lPln = isset($pReq['pln']) ? intval($pReq['pln']) : 0;
		$sName = isset( $pReq['name']) ? $pReq['name'] : null;
		$bChg = isset($pReq['suspend']) ? false : null;
		if ( isset($pReq['enable'])) $bChg=true;
		if ( $lCal > 0) {
			require_once( $pCfg['path_www']."/lib/cal.php");
			$pCal = new Cal();
			$pCal->find( $lCal);
			if ( $pCal->lID > 0) {
				if ( $lPln <= 0) {
					$lPln = $pCal->getPlan();
				} else $pCal->setPlan( $lPln);
				if ( isset( $pReq['email'])) $pCal->setEMail( $pReq['email']);
				$pWhen = isset($pReq['when']) ? array('cron'=>$pReq['when']) : array();
				if ( $sName != null) $pCal->setName( $sName);
				$pCal->chgEvent( $pWhen, $bChg);
				$pRet['rc']=0;
				$pRet['msg']="updated: ".$pCal->getName()." (".$lPln.")";
			}
		}
		return( $pRet);
	}

	function apiCalDelete( $pReq) {
		global $pCfg;
		$pRet = array( 'rc' => 1, 'msg' => "schedule delete failure");
	//	error_log( "DBG=>>> apiCalDelete ...");
		$lCal = isset($pReq['id']) ? intval($pReq['id']) : 0;
		if ( $lCal > 0) {
			require_once( $pCfg['path_www']."/lib/cal.php");
			$pCal = new Cal();
			$pCal->find( $lCal);
			if ( $pCal->lID > 0) {
				$pCal->markDeleted();
				$pRet['rc']=0;
				$pRet['msg']="deleted: ".$pCal->getName()." (".$lCal.")";
			}
		}
		return( $pRet);
	}

	function apiTestHistory( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/app.php");
		$lApp = detectOID( 'app', $pReq);
		$pApp = new App();
		$pRet = $pApp->getTestHistory( $lApp, isset($pReq['full']));
		return( $pRet);
	}

	function apiOnBoard( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/org.php");
		require_once( $pCfg['path_www']."/lib/who.php");
		require_once( $pCfg['path_www']."/lib/evt.php");
		$pRet = array( 'rc' => -1, 'msg' => "onboard failure");
		$pRBAC=null;
		$pWho = new Who();
		$lUsr = $pWho->mapLogin( $sLogin);
		if ( isset($pReq['tnt'])) {
			$lOrg = isset($pReq['tnt']['id']) ? intval($pReq['tnt']['id']) : 0;
			$sTnt = isset($pReq['tnt']['name']) ? $pReq['tnt']['name'] : '';
			$sGLC = isset($pReq['tnt']['fin']) ? $pReq['tnt']['fin'] : '';
			$sTntLbl = isset($pReq['tnt']['label']) ? $pReq['tnt']['label'] : $sTnt;
		//	error_log( "DBG=>>> API - lOrg=".$lOrg.", sTnt=".$sTnt.", sTntLbl=".$sTntLbl);
			$pOrg = new Org();
		// If not specified by ID, then try to identify by name
			if ( $lOrg < 1) $lOrg = $pOrg->findByName( $sTnt);
		//	Update name for detected org
			if ( ($sTnt == '') && ($lOrg > 0)) {
				$pOrg->find( $lOrg);
				$sTnt = $pOrg->getName();
				if ( $sGLC == '') $sGLC = $pOrg->getRef();
				if ( $sTntLbl == '') $sTntLbl = $sTnt;
			//	error_log( "DBG=>>> API - lOrg=".$lOrg.", sTnt=".$sTnt.", sTntLbl=".$sTntLbl);
			}
		// Auto-create if possible, and neither ID nor name identified
			if ( ($lOrg < 1) && ($sTnt != '')) {
				$pOrg->setName( $sTnt);
				$pOrg->setRef( $sGLC);
				$pOrg->setLabel( $sTntLbl);
				$pOrg->setIndex();
				$pOrg->commit();
				$lOrg = $pOrg->getOrgID();
				$pRBAC = new Rbac();
			//	Add org member role
				$sRole = "org_".$lOrg; $sTxt = "Org (".$lOrg.") member";
				$lRole = $pRBAC->Roles->add( $sRole, $sTxt);
			//	Add org admin role
				$sRole = "org_adm_".$lOrg; $sTxt = "Org (".$lOrg.") admin";
				$lRole = $pRBAC->Roles->add( $sRole, $sTxt);
			//	Add org admin permission
				$sPerm = "org_".$lOrg; $sTxt = "admin";
				$lPerm = $pRBAC->Permissions->add( $sPerm, $sTxt);
				$pRet['nfo'] = "Added tenancy ".$sTnt." with OrgID=".$lOrg.": ".$sTntLbl;
				$pRet['rc'] = 0;
			//	Add user as admin of new tenancy
				if ( $lUsr > 0) $pRBAC->Users->assign( "org_adm_".$lOrg, $lUsr);
			//	$pEvt = new Evt();
			//	$pEvt->setWho( $lUsr);
			//	$pEvt->setRef( $lOrg);
			//	$pEvt->logEvt( 1011, $lOrg, 'tenant created');
				islEventLog( 'sys.starling.create', array("id"=>$lOrg,"obj"=>"tenant","name"=>$sTnt));
			}
		// ...
			if ( $lOrg > 0) {
				require_once( $pCfg['path_www']."/lib/app.php");
				$pRet['tnt'] = $lOrg;
				$pApp = new App();
				$lApp = 0;
				if ( isset($pReq['app'])) {
					$lEID = isset($pReq['app']['eid']) ? intval($pReq['app']['eid']) : 0;
					if ( $lEID > 0) $lApp = $pApp->findByEID( $sTnt, $lEID);
					$pRet['app'] = $lApp;
					$sApp = isset($pReq['app']['name']) ? $pReq['app']['name'] : '';
					if ( ($lApp < 1) && ($sApp != '__NONE__')) {
						$sAppLbl = isset($pReq['app']['label']) ? $pReq['app']['label'] : '';
						if ( $sApp != '') $lApp = $pApp->findByName( $sApp);
					//	Duplicate and tenancy-misalignment prevention
						if ( $lApp > 0) { $sApp = $sTnt.time(); $lApp=0; }
						if ( $lApp < 1) {
							if ( $sApp == '') $sApp = "".$sTnt."".time();
							if ( $sAppLbl == '') $sAppLbl = $sApp;
							$pApp->setTenancy( $sTnt);
							$pApp->setGLCode( $sGLC);
							$pApp->setName( $sApp);
							$pApp->setLabel( $sAppLbl);
							$pApp->setUser( $sLogin);
							if ( isset($pReq['app']['eid'])) $pApp->setEID( intval($pReq['app']['eid']));
							if ( isset($pReq['app']['job'])) $pApp->setJob( $pReq['app']['job']);
							if ( isset($pReq['app']['cmdb'])) $pApp->setCI( $pReq['app']['cmdb']);
							$pApp->commit();
							$lApp = $pApp->lID;
							$sCI = $pApp->mapCI();
							if ( $pRBAC == null) $pRBAC = new Rbac();
							$pRoles = array( "app_".$lApp=>"viewer", "app_eng_".$lApp=>"engineer", "app_adm_".$lApp=>"admin");
							foreach ( $pRoles as $sRole => $sTxt) {
								$lRole = $pRBAC->Roles->add( $sRole, $sTxt);
							}
							$pPerms = array( "app_".$lApp=>"view", "app_xqt_".$lApp=>"execute", "app_adm_".$lApp=>"administration");
							foreach ( $pPerms as $sPerm => $sTxt) {
								$lRole = $pRBAC->Permissions->add( $sPerm, $sTxt);
							}
							$pRBAC->Roles->assign( $sRole, $sPerm);
							$pRBAC->Roles->assign( "app_".$lApp, "app_".$lApp);
							$pRBAC->Roles->assign( "app_eng_".$lApp, "app_".$lApp);
							$pRBAC->Roles->assign( "app_eng_".$lApp, "app_xqt_".$lApp);
							$pRBAC->Roles->assign( "app_adm_".$lApp, "app_".$lApp);
							$pRBAC->Roles->assign( "app_adm_".$lApp, "app_xqt_".$lApp);
							$pRBAC->Roles->assign( "app_adm_".$lApp, "app_adm_".$lApp);
							$pRBAC->Roles->assign( "org_adm_".$lOrg, "app_".$lApp);
							$pRBAC->Roles->assign( "org_adm_".$lOrg, "app_xqt_".$lApp);
							$pRBAC->Roles->assign( "org_adm_".$lOrg, "app_adm_".$lApp);
							$pRBAC->Users->assign( "app_adm_".$lApp, $lUsr);
							$pRet['app'] = $lApp;
							$pRet['rc'] = 0;
							$pRet['msg'] = "On-boarded ".$sApp." in ".$sTnt." (".$lOrg.")";
						//	$pEvt = new Evt();
						//	$pEvt->setWho( $lUsr);
						//	$pEvt->setRef( $lApp);
						//	$pEvt->logEvt( 1111, $lApp, 'profile created');
							$pLogNfo = array("id"=>$lApp,"obj"=>"profile","name"=>$sApp);
							islEventLog( 'sys.starling.'.$sCI.'.create', $pLogNfo);
							islEventLog( 'sys.starling.create', $pLogNfo);
						} else {
							$pRet['app'] = $lApp;
							$pRet['rc'] = 0;
							$pRet['msg'] = "app/profile already exists";
						}
					} else $pRet['msg'] = "app/profile already exists";
				} else $pRet['msg'] = "app/profile info missing";
			} else $pRet['msg'] = "unable to identify or create tenancy";
		} else $pRet['msg'] = "no tenancy info provided";
		return( $pRet);
	}

	function apiLiftOff( $pReq) {
		global $pCfg;
		global $sLogin;
		require_once( $pCfg['path_www']."/lib/org.php");
		require_once( $pCfg['path_www']."/lib/who.php");
		require_once( $pCfg['path_www']."/lib/evt.php");
		require_once( $pCfg['path_www']."/lib/app.php");
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pRet = array( 'rc' => -1, 'msg' => "failure");
		$lEID=0;
		$pWho = new Who();
		$lUsr = $pWho->mapLogin( $sLogin);
	//	Acquire LiftOff org data
		$lOrg = 1;
		$pOrg = new Org();
		$pOrg->find($lOrg);
		$sTnt = $pOrg->getName();
		$sTntLbl = $pOrg->getLabel();
		$sGLC = $pOrg->getRef();
	//	Find or auto-create tenant LiftOff profile
		$pApp = new App();
		$lApp = $pApp->findByName($sLogin);
		$sApp = $sLogin;
		$sAppLbl = $sLogin;
		if ( $lApp < 1) {
			$pApp->setTenancy( $sTnt);
			$pApp->setGLCode( $sGLC);
			$pApp->setJob( $sApp);
			$pApp->setName( $sApp);
			$pApp->setLabel( $sAppLbl);
			$pApp->setUser( $sLogin);
			$pApp->commit();
			$lApp = $pApp->lID;
		//	Stream log events
			$pLogNfo = array("id"=>$lApp,"obj"=>"profile","name"=>$sApp);
			islEventLog( 'sys.starling.'.$sCI.'.create', $pLogNfo);
			islEventLog( 'sys.starling.create', $pLogNfo);
		} else $pApp->find($lApp);
	//	Currency detection ...
		$pTst = new Tst();
		$pLst = $pTst->findByApp( $lApp, true);
		if ( count($pLst) > 0) {
			$pRet['rc']=1;
			$pRet['msg'] = "only one consecutive run per profile is allowed";
			return($pRet);
		}
	//	...
		$sCI = $pApp->mapCI();
		$pPln = $pApp->getPlanFromPool();
	//	Re-align plan
	//	$pTstNfo = $pReq['tst'];
		$pTstNfo = $pReq;
		$sFrm = isset($pTstNfo['frm']) ? $pTstNfo['frm'] : 'jmeter';
		$pPln->setEngine('xaas');
		$pPln->setFramework($sFrm);
		$iLGs = isset($pTstNfo['lgs']) ? intval($pTstNfo['lgs']) : 1;
		$pPln->setLGs($iLGs);
		if ( isset($pTstNfo['doc'])) {
			$pPln->setName($pTstNfo['name']);
			$pPln->setDoc($pTstNfo['doc']);
			$pPln->setVUsers(intval($pTstNfo['vuc']));
			if (isset($pTstNfo['xqt'])) $pPln->setExe($pTstNfo['xqt']);
			if (isset($pTstNfo['dtn'])) $pPln->setDuration("".$pTstNfo['dtn']);
		} else $pPln->setDoc("liftoff.json");
		$pPln->setRepository($pTstNfo['git']);
		$pPln->setBranch($pTstNfo['gbn']);
	//	$pPln->setRefDir($pTstNfo['grd']);
		$pPln->setCores("1.0");
		$pPln->setMMX(3072);
		$pPln->setMMS(1024);
		$pPln->commit();
	//	Launch test ...
		try {
			$pRet = $this->launchTest( $pApp, $pPln, $sLogin, $pWho->mapLogin($sLogin), $lEID);
			if ( isset($pRet['id'])) {
				$lTst = intval($pRet['id']);
				if ( $lTst > 0) {
					$sRptURL = isset($pCfg['url_rpt']) ? $pCfg['url_rpt'] : null;
					if ( $sRptURL != null) {
						$pRet['rpt'] = $sRptURL."/".$lTst."/";
						$pRet['arcrpt'] = $sRptURL."/".$lTst."/rpt-".$lTst.".zip";
						$pRet['arctst'] = $sRptURL."/".$lTst.".zip";
					}
				}
			}
		} catch (Exception $pErr) {}
		return( $pRet);
	}

	function apiJobList( $pReq) {
		global $pCfg;
		require_once( $pCfg['path_www']."/lib/tst.php");
		$pTst = new Tst();
		$pRet = $pTst->findActive();
		return( $pRet);
	}
}

class StarlingAPI extends SvcAPI {
	function StarlingAPI() {
		parent::SvcAPI();
		$this->pLogic = new StarlingLogic();
		$this->pCmds['applst']='apiAppList';
		$this->pCmds['appinf']='apiAppInfo';
		$this->pCmds['appupd']='apiAppUpdate';
		$this->pCmds['appadd']='apiAppAdd';
		$this->pCmds['appdel']='apiAppDelete';
		$this->pCmds['appjob']='apiDetectJob';
		$this->pCmds['joblst']='apiJobList';
		$this->pCmds['plnlst']='apiPlanList';
		$this->pCmds['plninf']='apiPlanInfo';
		$this->pCmds['plnupd']='apiPlnUpdate';
		$this->pCmds['plnadd']='apiPlnAdd';
		$this->pCmds['plndel']='apiPlnDel';
		$this->pCmds['tstinf']='apiTestInfo';
		$this->pCmds['tstupd']='apiTestUpdate';
		$this->pCmds['tstxgo']='apiTestLaunch';
		$this->pCmds['tstdel']='apiTestDelete';
		$this->pCmds['tsthst']='apiTestHistory';
		$this->pCmds['tstcon']='apiTestConsole';
		$this->pCmds['tstccl']='apiTestCancel';
		$this->pCmds['callst']='apiCalList';
		$this->pCmds['calinf']='apiCalInfo';
		$this->pCmds['caladd']='apiCalAdd';
		$this->pCmds['calupd']='apiCalUpdate';
		$this->pCmds['caldel']='apiCalDelete';
		$this->pCmds['rbac']='apiCheckRBAC';
		$this->pCmds['usrinf']='apiWhoInfo';
		$this->pCmds['usradd']='apiWhoAdd';
		$this->pCmds['orglst']='apiOrgList';
		$this->pCmds['orginf']='apiOrgInfo';
		$this->pCmds['orgadd']='apiOrgAdd';
		$this->pCmds['orgupd']='apiOrgUpdate';
		$this->pCmds['orgappadd']='apiOrgAppAdd';
		$this->pCmds['orgapprmv']='apiOrgAppRemove';
		$this->pCmds['orgusrlst']='apiOrgUsrList';
		$this->pCmds['orgadmlst']='apiOrgAdmList';
		$this->pCmds['orgusradd']='apiOrgUsrAdd';
		$this->pCmds['orgusrdel']='apiOrgUsrDel';
		$this->pCmds['orgadmadd']='apiOrgAdmAdd_redir';
		$this->pCmds['orgadmdel']='apiOrgAdmDel_redir';
		$this->pCmds['appusrlst']='apiAppUsrList';
		$this->pCmds['appusradd']='apiAppUsrAdd';
		$this->pCmds['appengadd']='apiAppEngAdd_redir';
		$this->pCmds['appadmadd']='apiAppAdmAdd_redir';
		$this->pCmds['appusrdel']='apiAppUsrDel';
		$this->pCmds['appengdel']='apiAppEngDel_redir';
		$this->pCmds['appadmdel']='apiAppAdmDel_redir';
		$this->pCmds['onboard']='apiOnBoard';
		$this->pCmds['liftoff']='apiLiftOff';
	}
}
?>
