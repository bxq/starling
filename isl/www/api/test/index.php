<?php
$sPageBaseLoc = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
require_once( $sPageBaseLoc.'/etc/private/localcfg.php');

function apiTstGET( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiTstGET invoked ... ");
	$lTst = pgInt( 'id', 0);
	$lApp = pgInt( 'app', 0);
	$sFlg = pgStr( 'job', '');
	if ( $lApp > 0) {
		if ( $sFlg == '') {
			$pLst = $pLogic->apiTestHistory( array('id'=>$lApp));
			foreach ( $pLst as $lTst => $pNfo) {
				$pTst = array();
				$pTst['id'] = $lTst;
				$pTst['name'] = isset($pNfo['label']) ? $pNfo['label'] : "Test ".$lTst;
				if ( isset( $pNfo['pln'])) {
					$pTst['pln'] = array();
					$lPln = intval($pNfo['pln']);
					$pTst['pln']['id'] = $lPln;
					$pTst['pln']['name'] = isset($pNfo['plnlbl']) ? $pNfo['plnlbl'] : "Plan ".$lPln;
				}
				$pTst['doc'] = isset($pNfo['doc']) ? $pNfo['doc'] : '';
				$pTst['rpturl'] = isset($pNfo['rpturl']) ? $pNfo['rpturl'] : '';
				array_push( $pRet, $pTst);
			}
		} else {
			$pInf = $pLogic->apiDetectJob( array('app'=>$lApp));
			$pPln = array();
			$pTst = array( 'pln' => $pPln);
			foreach ( $pInf as $sKey => $pVal) {
				if ( $sKey == 'tst') {
					$pTst['id'] = $pInf[$sKey];
				} else if ( $sKey == 'pln') {
					$pTst['pln']['id'] = $pInf[$sKey];
				} else if (	($sKey == 'tnt') ||
								($sKey == 'ngn') ||
								($sKey == 'frm') ||
								($sKey == 'git') ||
								($sKey == 'doc')) {
					$pTst['pln'][$sKey] = $pVal;
				} else $pTst[$sKey] = $pVal;
			}
			array_push( $pRet, $pTst);
		}
	} else {
		$pTst = $pLogic->apiTestInfo( array('id'=>$lTst));
		if ( isset($pTst['pln'])) {
			$pTst['pln'] = array();
			$pTst['pln']['id'] = intval($pTst['pln']);
		}
		array_push( $pRet, $pTst);
	}
	return( $pRet);
}

function apiTstPOST( $pLogic, $pReq) {
	global $sLogin;
	$pRet = array();
//	error_log( "DBG=>>> apiTstPOST invoked ... ");



	$pPln = isset($pReq['pln']) ? $pReq['pln'] : array();
	$lPln = isset($pPln['id']) ? intval($pPln['id']) : 0;
	$lTst = isset($pReq['id']) ? $pReq['id'] : 0;
	$lEID=0; $sTNT='';
	$pApp = isset($pReq['pln']['app']) ? $pReq['pln']['app'] : null;
	if ( $pApp != null) {
		$lEID = isset($pApp['eid']) ? intval($pApp['eid']) : 0;
		$sTNT = isset($pApp['tnt']) ? $pApp['tnt'] : '';
	}
	$sWho = isset( $sLogin) ? $sLogin : 'api';
//	Construct RPC payload
	$pRPC = array( 'id'=>$lPln, 'login'=>$sWho);
	if ( $lEID > 0) $pRPC['eid'] = $lEID;
	if ( $sTNT != '') $pRPC['tnt'] = $sTNT;
	if ( $lTst > 0) $pRPC['tst'] = $lTst;
//	Process RPC invocation
	$pNfo = $pLogic->apiTestLaunch( $pRPC);
	$lTst = isset($pNfo['id']) ? intval($pNfo['id']) : 0;
	$sTst = isset($pNfo['name']) ? $pNfo['name'] : '';
	$sDoc = isset($pNfo['doc']) ? $pNfo['doc'] : '';
	$pTst = array( 'id'=>$lTst);
	if ( $sTst != '') $pTst['name']=$sTst;
	if ( $sDoc != '') $pTst['doc']=$sDoc;
	array_push( $pRet, $pTst);
	return( $pRet);
}

function apiTstPUT( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiTstPUT invoked ... ");
	error_log( "ERR=>>> unsupported apiTstPUT invoked ... ");
	return( $pRet);
}

function apiTstDELETE( $pLogic, $pReq) {
	global $sLogin;
	$pRet = array();
//	error_log( "ERR=>>> apiTstDELETE invoked ... ");
	$lEID=0; $sTNT='';
	$lTst = isset($pReq['id']) ? $pReq['id'] : 0;
	$pApp = isset($pReq['pln']['app']) ? $pReq['pln']['app'] : null;
	if ( $pApp != null) {
		$lEID = isset($pApp['eid']) ? intval($pApp['eid']) : 0;
		$sTNT = isset($pApp['tnt']) ? $pApp['tnt'] : '';
	}
	$sWho = isset( $sLogin) ? $sLogin : 'api';
//	Construct RPC payload
	$pRPC = array( 'login'=>$sWho);
	if ( $lEID > 0) $pRPC['eid'] = $lEID;
	if ( $sTNT != '') $pRPC['tnt'] = $sTNT;
	if ( $lTst > 0) $pRPC['tst'] = $lTst;
	//	Process RPC invocation
	$pNfo = $pLogic->apiTestCancel( $pRPC);
	array_push( $pRet, array('rc'=>$pNfo['rc'],'msg'=>$pNfo['msg']));
	return( $pRet);
}

function genRspREST( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> genRsp invoked ... ".var_export($pReq,true));
	$pRsp = array();
	switch ( $_SERVER['REQUEST_METHOD']) {
		case 'GET': $pRsp = apiTstGET( $pLogic, $pReq); break;
		case 'POST': $pRsp = apiTstPOST( $pLogic, $pReq); break;
		case 'PUT': $pRsp = apiTstPUT( $pLogic, $pReq); break;
		case 'DELETE': $pRsp = apiTstDELETE( $pLogic, $pReq); break;
	}
	foreach ( $pRsp as $pItm) array_push( $pRet, $pItm);
	return( $pRet);
}

require_once( '../index.php');
?>
