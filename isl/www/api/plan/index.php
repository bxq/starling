<?php
$sPageBaseLoc = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
require_once( $sPageBaseLoc.'/etc/private/localcfg.php');

function apiPlnGET( $pLogic, $pReq) {
	global $pCfg;
	$pRet = array();
	$lPln=0;
	$lApp=0;
	$lEID = pgInt( 'eid', 0);
	$sTnt = pgStr( 'tnt', '');
	$lAppID = pgInt( 'app', 0);
	if ( $lAppID <= 0) {
		$pDat = array();
		if ( ($lEID > 0) && ($sTnt != '')) {
			$pDat['eid']=$lEID;
			$pDat['tnt']=$sTnt;
		} else $pDat['id'] = pgInt( 'id', 0);
		$pNfo = $pLogic->apiPlanInfo( $pDat);
		if ( isset($pNfo['app'])) {
			$lApp = intval($pNfo['app']);
			$pNfo['app'] = array('id'=>$lApp);
		}
		array_push( $pRet, $pNfo);
	} else {
		$pDat = array('id'=>$lAppID,'eid'=>$lEID,'tnt'=>$sTnt);
		$pNfo = $pLogic->apiPlanList( $pDat);
		foreach ( $pNfo as $lPln => $sPln) {
			$pPln = array( 'id' => $lPln, 'name' => $sPln);
			array_push( $pRet, $pPln);
		}
	}
	return( $pRet);
}

function apiPlnPOST( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiPlnPost invoked ... ");
	$lApp=0;
	$sTNT='';
	$pRPC=array();
	foreach ( $pReq as $sKey => $pItm) {
		if ( ($sKey == 'app') && is_array( $pReq[$sKey])) {
			$lApp = isset($pReq['app']['id']) ? intval($pReq['app']['id']) : 0;
			if ( isset($pReq['app']['tnt']) && is_array($pReq['app']['tnt'])) {
				$sTNT = isset($pReq['app']['tnt']['name']) ? $pReq['app']['tnt']['name'] : '';
			}
		} else $pRPC[$sKey]=$pReq[$sKey];
	}
	if ( $lApp > 0) $pRPC['app']=$lApp;
	if ( $sTNT != '') $pRPC['tnt']=$sTNT;
	$pNfo = $pLogic->apiPlnAdd( $pRPC);
	$pDat = array();
	$pDat['id'] = isset($pNfo['id']) ? $pNfo['id'] : 0; 
	array_push( $pRet, $pDat);
	return( $pRet);
}

function apiPlnPUT( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> apiPlnPUT invoked ... ");
	$lApp=0;
	$sTNT='';
	$pRPC=array();
	foreach ( $pReq as $sKey => $pItm) {
		if ( ($sKey == 'app') && is_array( $pReq[$sKey])) {
			$lApp = isset($pReq['app']['id']) ? intval($pReq['app']['id']) : 0;
			if ( isset($pReq['app']['tnt']) && is_array($pReq['app']['tnt'])) {
				$sTNT = isset($pReq['app']['tnt']['name']) ? $pReq['app']['tnt']['name'] : '';
			}
		} else $pRPC[$sKey]=$pReq[$sKey];
	}
	if ( $lApp > 0) $pRPC['app']=$lApp;
	if ( $sTNT != '') $pRPC['tnt']=$sTNT;
	$pNfo = $pLogic->apiPlnUpdate( $pRPC);
	$pDat = array();
	$pDat['id'] = isset($pNfo['pln']) ? $pNfo['pln'] : 0; 
	array_push( $pRet, $pDat);
	return( $pRet);
}

function apiPlnDELETE( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "ERR=>>> apiPlnDELETE invoked ... ");
	$lPln = pgInt('id',0);
	$lEID = pgInt('eid',0);
	$sTNT = pgStr('tnt','');
	$pRPC = array();
	if ( $lPln > 0) $pRPC['id']=$lPln;
	if ( $lEID > 0) $pRPC['eid']=$lEID;
	if ( $sTNT != '') $pRPC['tnt']=$sTNT;
	$pNfo = $pLogic->apiPlnDel( $pRPC);
	return( $pRet);
}

function genRspREST( $pLogic, $pReq) {
	$pRet = array();
//	error_log( "DBG=>>> genRsp invoked ... ".var_export($pReq,true));
	$pRsp = array();
	switch ( $_SERVER['REQUEST_METHOD']) {
		case 'GET': $pRsp = apiPlnGET( $pLogic, $pReq); break;
		case 'POST': $pRsp = apiPlnPOST( $pLogic, $pReq); break;
		case 'PUT': $pRsp = apiPlnPUT( $pLogic, $pReq); break;
		case 'DELETE': $pRsp = apiPlnDELETE( $pLogic, $pReq); break;
	}
	foreach ( $pRsp as $pItm) array_push( $pRet, $pItm);
	return( $pRet);
}

require_once( '../index.php');
?>
