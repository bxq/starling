#include <fcgi_stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include<sys/wait.h> 
#include<sys/types.h> 

int main( int argc, char *argv[]);
void shrugDefunct( int);

typedef struct nfostruct {
	char bIdx;
	unsigned long lTst;
	char sCI[40+1];
	char sSFX[80+1];
	char sPath[80+1];
} Nfo;

int pathFromURI( Nfo *, char *);
void genRpt( Nfo &);
void showRedirect( char *);
void showPage( char *, Nfo &, char *);

int bDbg=0;

void shrugDefunct( int signum) { wait(0L); } 

int main( int argc, char *argv[]) {
	Nfo aNfo;
	while ( FCGI_Accept() >= 0) {
		memset( &aNfo, 0, sizeof(aNfo));
		char *sURI = getenv("REQUEST_URI");
		int iRC = pathFromURI( &aNfo, sURI);
		char *sQry = getenv("QUERY_STRING");
		if ( !aNfo.bIdx) {
			char sQS[80+1];
			strncpy( sQS, sQry, 80); sQS[80]='\0';
			for ( char *pX=sQS; *pX; pX++) if ( *pX == '.') *pX=' ';
			int iScanned = sscanf( sQS, "sfx=starling %s test %lu", aNfo.sCI, &(aNfo.lTst));
			if ( iScanned == 2) {
				sprintf( aNfo.sSFX, "starling.%s.test.%lu", aNfo.sCI, aNfo.lTst);
				genRpt( aNfo);
			} else {
				iScanned = sscanf( sQS, "tst=%lu", &(aNfo.lTst));
				showPage( sURI, aNfo, sQry);
			}
		} else showRedirect( aNfo.sPath);
	}
	return( 0);
}

int pathFromURI( Nfo *pNfo, char *sURI) {
	int iRet=0;
	if ( sURI && sURI[0]) {
		char pWrk[80+1];
		strncpy(pWrk,sURI,80); pWrk[80]='\0';
		char *pMark = pWrk;
		char *pQ = strchr( pMark, '?');
		if ( pQ) *pQ = '\0';
		pQ = strchr( pMark, '/');
		if ( pQ) {
			pQ++;
			pMark = pQ;
			pQ = strchr( pMark, '/');
			if ( pQ) {
				pNfo->bIdx = 'T';
				*pQ = '\0';
			}
		}
		strcpy( pNfo->sPath, pMark);
	}
	return( iRet);
}

void genRpt( Nfo &aNfo) {
	pid_t pid = fork();
	if ( pid == 0) {
		char *pArgs[4];
		pArgs[0] = (char *)(malloc(80+1));
		strcpy( pArgs[0], "/home/starling/bin/rptgen");
		pArgs[1] = (char *)(malloc(80+1));
		sprintf( pArgs[1], "%lu", aNfo.lTst);
		pArgs[2] = (char *)(malloc(80+1));
		strcpy( pArgs[2], aNfo.sSFX);
		pArgs[3]=0L;
		execvp( "/home/starling/bin/rptgen", pArgs);
		free(pArgs[0]); free(pArgs[1]); free(pArgs[2]);
	} else if ( pid > 0) {
		signal( SIGCHLD, shrugDefunct); 
		if ( bDbg) {
			printf(	"Content-type: text/html\r\n\r\n"
						"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
						"<html>\n<body>\n"
						"<table align=center>\n"
						"<tr><td colspan=2><h1>Report Generation ...</h1></td></tr>\n"
						"<tr><td><strong>Test ID:</strong></td><td>%lu</td></tr>\n"
						"<tr><td><strong>CI:</strong></td><td>%s</td></tr>\n"
						"<tr><td><strong>SFX:</strong></td><td>%s</td></tr>\n"
						"</body>\n</html>\n", aNfo.lTst, aNfo.sCI, aNfo.sSFX);
		} else {
			printf(	"Content-type: text/html\r\n\r\n"
						"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
						"<html>\n<body>\n"
						"<table align=center>\n"
						"<tr><td>{\"tst\":%lu,\"ci\":\"%s\",\"sfx\":\"%s\"}</td></tr>\n"
						"</body>\n</html>\n", aNfo.lTst, aNfo.sCI, aNfo.sSFX);
		}
	}
}

void showRedirect( char *sPath) {
	printf(	"Content-type: text/html\r\n\r\n"
				"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
				"<html>\n<head>"
				"<meta http-equiv=\"refresh\" content=\"0;url='/%s/index.html'\"/>"
				"</head>\n<body><br></body>\n</html>\n", sPath);
}

void showPage( char *sURI, Nfo &aNfo, char *sQry) {
	char sLnk[40];
	strcpy( sLnk, "/");
	if ( aNfo.lTst > 0) sprintf( sLnk, "/%ld/", aNfo.lTst);
	if ( aNfo.bIdx > 0) strcat( sLnk, "index.html");
	if ( bDbg) {
		printf(	"Content-type: text/html\r\n\r\n"
					"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
					"<html>\n<body>\n"
					"<table align=center>\n"
					"<tr><td colspan=2><h1>DEBUG ...</h1></td></tr>\n"
					"<tr><td><strong>Process ID:</strong></td><td>%d</td></tr>\n"
					"<tr><td><strong>Request URI:</strong></td><td>%s</td></tr>\n"
					"<tr><td><strong>Path:</strong></td><td>%s</td></tr>\n"
					"<tr><td><strong>Query String:</strong></td><td>%s</td></tr>\n"
					"<tr><td><strong>Message :</strong></td><td>%s</td></tr>\n"
					"</body>\n</html>\n", getpid(), sURI, aNfo.sPath, sQry, sLnk);
	} else {
		const char *sMsg="If you've reached this page, its likely that your test<br>\nhas either not completed execution or has encountered<br>\na failure in report generation.<br>\nfor diagnosis of potential root-cause(s).<br>\n";
		printf(	"Content-type: text/html\r\n\r\n"
					"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
					"<html>\n<body>\n"
					"<table align=center>\n"
					"<tr><td colspan=2><h1>Test Report ...</h1></td></tr>\n"
					"<tr><td><strong></strong></td><td>%s</td></tr>\n"
					"</body>\n</html>\n", sMsg);
	}
	return;
}
