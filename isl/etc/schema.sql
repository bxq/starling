-- MySQL dump 10.19  Distrib 10.3.37-MariaDB, for debian-linux-gnu (aarch64)
--
-- Host: localhost    Database: starlingdev
-- ------------------------------------------------------
-- Server version	10.3.37-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app`
--

DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `eid` bigint(20) DEFAULT NULL,
  `axs` bigint(20) DEFAULT NULL,
  `usr` bigint(20) DEFAULT NULL,
  `tnt` varchar(40) DEFAULT 'epe',
  `name` varchar(80) DEFAULT NULL,
  `label` varchar(80) DEFAULT NULL,
  `cmdb` varchar(80) DEFAULT '',
  `ask` varchar(40) DEFAULT '',
  `fin` varchar(40) DEFAULT '',
  `job` varchar(80) DEFAULT NULL,
  `txt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal`
--

DROP TABLE IF EXISTS `cal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `chg` datetime DEFAULT NULL,
  `who` bigint(20) DEFAULT NULL,
  `pln` bigint(20) DEFAULT NULL,
  `tst` bigint(20) DEFAULT NULL,
  `wip` bigint(20) DEFAULT -1,
  `cts` varchar(80) DEFAULT NULL,
  `top` datetime DEFAULT NULL,
  `btm` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `txt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lgs`
--

DROP TABLE IF EXISTS `lgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lgs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chg` datetime DEFAULT NULL,
  `lck` varchar(80) DEFAULT NULL,
  `node` varchar(80) DEFAULT NULL,
  `job` bigint(20) DEFAULT -1,
  `nfo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lut`
--

DROP TABLE IF EXISTS `lut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lut` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ptn` bigint(20) DEFAULT 1,
  `lut` bigint(20) DEFAULT NULL,
  `oid` bigint(20) DEFAULT NULL,
  `idx` bigint(20) DEFAULT NULL,
  `ref` varchar(40) DEFAULT NULL,
  `mag` bigint(20) DEFAULT NULL,
  `tag` varchar(40) DEFAULT NULL,
  `label` varchar(80) DEFAULT NULL,
  `txt` text,
  PRIMARY KEY (`id`),
  KEY `lut` (`lut`),
  KEY `oid` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lut`
--

LOCK TABLES `lut` WRITE;
/*!40000 ALTER TABLE `lut` DISABLE KEYS */;
INSERT INTO `lut` (`id`, `ptn`, `lut`, `oid`, `idx`, `ref`, `mag`, `tag`, `label`, `txt`) VALUES (1,1,0,NULL,0,NULL,201,'org','Organization',NULL);
INSERT INTO `lut` (`id`, `ptn`, `lut`, `oid`, `idx`, `ref`, `mag`, `tag`, `label`, `txt`) VALUES (2,1,201,NULL,0,NULL,NULL,'liftoff','liftoff',NULL);
/*!40000 ALTER TABLE `lut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pln`
--

DROP TABLE IF EXISTS `pln`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pln` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `eid` bigint(20) DEFAULT NULL,
  `chg` datetime DEFAULT NULL,
  `app` bigint(20) DEFAULT NULL,
  `ngn` varchar(20) DEFAULT 'k8s',
  `frm` varchar(24) DEFAULT NULL,
  `git` varchar(255) DEFAULT NULL,
  `gbn` varchar(80) DEFAULT 'master',
  `grd` varchar(255) DEFAULT NULL,
  `tnt` varchar(40) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `lgs` bigint(20) DEFAULT 1,
  `vusr` bigint(20) DEFAULT 1,
  `doc` varchar(80) DEFAULT NULL,
  `vis` varchar(1024) DEFAULT '',
  `nfo` text DEFAULT NULL,
  `xqt` text DEFAULT NULL,
  `txt` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_permissions`
--

DROP TABLE IF EXISTS `rbac_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_permissions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Lft` int(11) NOT NULL,
  `Rght` int(11) NOT NULL,
  `Title` char(64) NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Title` (`Title`),
  KEY `Lft` (`Lft`),
  KEY `Rght` (`Rght`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_rolepermissions`
--

DROP TABLE IF EXISTS `rbac_rolepermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_rolepermissions` (
  `RoleID` int(11) NOT NULL,
  `PermissionID` int(11) NOT NULL,
  `AssignmentDate` int(11) NOT NULL,
  PRIMARY KEY (`RoleID`,`PermissionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_roles`
--

DROP TABLE IF EXISTS `rbac_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Lft` int(11) NOT NULL,
  `Rght` int(11) NOT NULL,
  `Title` varchar(128) NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Title` (`Title`),
  KEY `Lft` (`Lft`),
  KEY `Rght` (`Rght`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_userroles`
--

DROP TABLE IF EXISTS `rbac_userroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_userroles` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `AssignmentDate` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst`
--

DROP TABLE IF EXISTS `tst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `chg` datetime DEFAULT NULL,
  `eid` bigint(20) DEFAULT NULL,
  `app` bigint(20) DEFAULT NULL,
  `pln` bigint(20) DEFAULT NULL,
  `who` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `job` bigint(20) DEFAULT 0,
  `api` varchar(40) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `vusr` bigint(20) DEFAULT 1,
  `rpturl` varchar(255) DEFAULT NULL,
  `ipg` bigint(20) DEFAULT 0,
  `xpg` text DEFAULT NULL,
  `nfo` text DEFAULT NULL,
  `xqt` text DEFAULT NULL,
  `txt` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `who`
--

DROP TABLE IF EXISTS `who`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `who` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `eid` varchar(40) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `ptn` bigint(20) DEFAULT 1,
  `org` bigint(20) DEFAULT 0,
  `status` bigint(20) DEFAULT 0,
  `login` varchar(20) DEFAULT NULL,
  `label` varchar(80) DEFAULT NULL,
  `fname` varchar(40) DEFAULT NULL,
  `lname` varchar(40) DEFAULT NULL,
  `txt` text,
  PRIMARY KEY (`id`),
  KEY `login` (`login`),
  KEY `lname` (`lname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `who`
--

LOCK TABLES `who` WRITE;
/*!40000 ALTER TABLE `who` DISABLE KEYS */;
INSERT INTO `who` (`eid`,`created`,`ptn`,`org`,`status`,`login`,`label`,`fname`,`lname`,`txt`) VALUES (NULL,now(),1,0,0,'admin','starling@nowhere.com',NULL,NULL,'Administrator');
INSERT INTO `who` (`eid`,`created`,`ptn`,`org`,`status`,`login`,`label`,`fname`,`lname`,`txt`) VALUES (NULL,now(),2,0,0,'starling','starling@nowhere.com',NULL,NULL,'Starling');
/*!40000 ALTER TABLE `who` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-16 10:14:58
