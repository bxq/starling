Starling is an automation-focused platform for testing at any scale.

A minimal GUI is provided:

<img src=screenshot.png width=800>

... as long as the user is authorized, and the referenced git repository is structured correctly, tests can be executed with no further setup.

See [the wiki](https://gitlab.com/bxq/starling/-/wikis/home) for more detailed documentation.

# Deployment

## MySQL
Starling requires a MySQL database, and currently only supports an externally-managed instance, which must be seperately installed and configured.

Once the MySQL instance is available, Starling's `schema.sql` file can be used to initialize a new database.

## LDAP
By default, tenant login depends on a working OpenLDAP installation.

Active Directory can be similarly utilized by suitable changes to the `localcfg.php` file (detailed below).

## Virtualization
Starling is designed to be operated via Docker containers.

Building of the docker images is covered in a more detailed section below.

Once the images are built/downloaded, they should be available in the respective operating environments they will be executed in:

- `isl`: integrated service layer
	The ISL provides central platform context and services - only one ISL container should run per Starling installation.
	
- `vlg`: virtual load generator
	The VLG provides an agent, where:
	-	Each instantiation executes one LG for a specified test
	-	Instantiated by the ISL on (any) load generator (LG) node
	-	Tests can employ any number of LGs, constrained only by LG node resources available

## Service Management
The `rc.starling` script:
-	Delivered in the `bin` directory under the repository root
-	Crafted as a service start-up/termination command that can also be invoked from a systemd unit file
-	Provides a `--init` option that initializes local storage expected by the specified image
-	Provides a `--update` option that updates the specified image

## SSH
The ISL uses SSH for two fundamental operations:
-	Instantiating LGs on LG nodes
-	Collecting LG footprints (via `rsync`) for post-processing

These mechanics require that SSH keys be present for password-less login by the ISL on all LG nodes, which is the purpose of the `dotssh.tgz` file present in the ISL in-container configuration location.

## Storage
These locations will be populated under the `~/var/starling` directory, for the unix login(s) the docker containers will run as.

### ISL
-	`etc`: in-container configuration
-	`zoo`: in-container zookeeper storage
-	`var`: in-container operational storage
-	`pub`: in-container publish site storage

### LG
-	`vlgetc`: in-container configuration
-	`test`: in-container operational storage

## Configuration

### In-Container Files
Located in the ISL's `etc` subdirectory of its `$VAR` location:
-	`dotssh.tgz`: snapshot of a `.ssh` directory tree, for the ISL `starling` login, with `known_hosts` omitted
-	`hosts-append.txt`: optional lines to append to in-container `/etc/hosts`
-	`lgs.json`: persistent LG registry, see below for structure
-	`localcfg.php`: config file for `wwwrig`, propagated in-container at startup
-	`ssl.key` and `ssl.pem`: SSL cert for HTTPS services running in container
-	`starlingrc.sed`: variable substitution values for in-container config file population
-	`zookinirc.sed`: variable substitution values for in-container config file population

Located in the VLG's `vlgetc` subdirectory of its `$VAR` location:
-	`starlingrc.sed`: variable substitution values for in-container config file population
-	`zookinirc.sed`: variable substitution values for in-container config file population

### Config File Details

#### lgs.json
The `lgs.json` file provides some data used to inform the in-container network configuration and capabilities.

Primarily, the `dns` lists are used to populate SSH `known_hosts` file(s) so that SSH connections won't fail from host confirmation prompts.

Example:
```json
{
	"hosts": [
		{	"name": "starlingdev",
			"dns": [
				"starlingdev.yourdomain.com", "starlingdev", "rpt.yourdomain.net", "rpt"
			],
			"ipv4": "10.0.0.220"
		},
		{	"name": "anotherhost",
			"dns": [
				"anotherhost.yourdomain.net",
				"anotherhost"
			],
			"ipv4": "10.0.0.104"
		}
	]
}
```

#### starlingrc.sed
Variable substitution values for in-container config file population

Example _(the IP address should be for your ISL server)_:
```sed
s/_CFG_TGT_/dev/g
s/_CFG_P4M_/xaas/g
s/_CFG_ISL_HOST_/yourhost/g
s/_CFG_API_HOST_/yourhost:8443/g
s/_CFG_CONSOLE_HOST_/10.0.0.220/g
s/_CFG_INFLUX_HOST_/yourinfluxhost/g
s/_CFG_RPT_PROTO_/http/g
s/_CFG_RPT_HOST_/yourhost:8080/g
s/_CFG_API_MSID_/starling/g
s/_CFG_API_USER_/starling/g
s/_CFG_API_PASS_/yourstarlingpassword/g
s/_CFG_DB_HOST_/yourdbhost/g
s/_CFG_DB_NAME_/yourdbname/g
s/_CFG_DB_USER_/yourdblogin/g
s/_CFG_DB_PASS_/yourdbpassword/g
```

#### zookinirc.sed
Variable substitution values for in-container config file population

Example _(the IP address should be for your ISL server)_:
```sed
s/_CFG_RPC_HOST_/10.0.0.220:8443/g
s/_CFG_RPC_MSID_/tenantlogin/g
s/_CFG_RPC_USER_/tenantlogin/g
s/_CFG_RPC_PASS_/tenantpassword/g
s/_CFG_RPC_PFX_/starling/g
s/_CFG_BOT_ZKH_/10.0.0.220:2181/g
```

#### localcfg.php
The `localcfg.php` file is the config file for `wwwrig`, the web framework utilized by Starling.

Alot of this file will depend on specifics from your installation ... host name(s), DB credentials, logins, etc.

A reasonable example:
```php
<?php
$pCfg = array(	'db_host' => 'yourdbhost',
					'db_db' => 'yourdbname',
					'db_uid' => 'yourdblogin',
					'db_pwd' => 'yourdbpassword',
				//	'suspend' => 'Test launch suspended, resuming shortly',
					'path_www' => '/var/www/html',
					'path_app' => 'feather.php',
					'path_pwd' => '/var/www/html/etc/private/htpasswd',
					'path_img' => 'img',
					'url_host' => 'your.host.com:8443',
					'url_app' => 'https://your.host.com:8443/feather.php',
					'url_secapp' => 'https://your.host.com:8443/feather.php',
					'url_home' => 'https://your.host.com:8443',
					'url_content' => 'https://your.host.com:8443',
					'url_cal' => 'http://your.calhost.com:8080',
					'url_gfx' => 'https://your.visualizationhost.com:8443',
					'url_rpt' => 'http://your.host.com:8080',
					'jwt' => 'doesntmatter-mostlyunused',
					'rbac' => 'starlingrbac.php',
					'ecmakit' => 'etc/ecmakit.js',
					'ecma' => 'addJSCodeFeather',
					'api' => 'starling',
				//	'apitrace' => true,
					'api_class' => 'StarlingAPI',
					'api_auth' => 'true',
					'api_host' => 'https://your.host.com:8443/index.php',
					'api_main' => 'api/starling.php',
					'app_main' => 'lib/feather.php',
					'app_pfx' => 'starling',
					'app_title' => 'Starling',
					'app_slogan' => 'Powered by Starling',
					'app_site_logo' => 'logo-site.png',
					'app_scope' => 'managed',
					'zookeeper' => 'your.host.com:2181',
					'events' => 'your.host.com',
					'unix_login' => 'starling',
					'unix_path' => '/home/starling/bin',
					'ldap_host' => '10.0.10.9',
					'ldap_groups' => array('starlinguser'=>'login'),
					'ldap_udn' => 'dc=your,dc=domain,dc=net',
					'ldap_bind' => 'uid=%s,%s',
					'ldap_xia' => 'ou=person,dc=your,dc=domain,dc=net',
					'ldap_filter' => '(&(objectClass=inetOrgPerson)(uid=%s))',
					'ldap_key' => 'ou',
				//	'app_ssnuid' => 'starling_anonymous',
					'superadmins' => array(	'starling' => '2' ),
					'bots' => array( 'chatbot' => 'somepassword' ),
					'map_emails' => array(	'default' => array( 'L1' => 'EM1' ),
													'phone' => array( 'L1' => 'PH1'),
													'work' => array( 'L1' => 'EM2') ) );
//	These are temporarily here for PhpRbac ...
class CfgRBAC {
	const HOST="yourdbhost";
	const USER="yourdblogin";
	const PASS="yourdbpassword";
	const DBNAME="yourdbname";
	const ADAPTER="mysqli";
	const TABLEPFX="rbac_";
}
?>
```

### Load Generator Node Registration
The ISL utilizes an LG node registry, which is driven from a database table named `lgs`.

The table contains a list of (mostly) empty virtual LG records.  The entire list represents the virtual LG capacity, or max number of virtual LGs that can be instantiated at a time.

Example:
```
MariaDB [starlingdev]> select * from lgs limit 0,4;
+----+---------------------+--------+------+------+--------------------+
| id | chg                 | lck    | node | job  | nfo                |
+----+---------------------+--------+------+------+--------------------+
|  1 | 2023-01-17 20:17:46 | vlg201 | NULL |    0 | {"tag":"starling"} |
|  2 | 2023-01-17 20:17:46 | NULL   | NULL |    0 | NULL               |
|  3 | 2023-01-16 16:22:48 | NULL   | NULL |    0 | NULL               |
|  4 | 2023-01-15 07:21:38 | NULL   | NULL |    0 | NULL               |
+----+---------------------+--------+------+------+--------------------+
4 rows in set (0.007 sec)
```

... this example shows one node-locked record, where the host name "vlg201" will be used by the ISL to interact with the node.

Registration of an LG node involves converting one of the empty virtual LG records into a node-locked record, which then represents the presence/availability of the LG node indicated in that record.  There should be one node-locked record for each LG node available for the ISL to instantiate virtual LGs on.

Example query to convert an empty record into a node-locked record:
```sql
update lgs set chg=now(),lck='nodehostname','{"tag":"yourlgname"}' where id=2;
```

<br>
<br>
<br>

------
# Advanced Topics

## Docker Images: Construction
The build process for the docker images involves fairly advanced skills, including comprehension of the technical topics involved sufficient to find details in the build footprint and materials that may not be completely documented.

There are three Docker image definitions:
-	`starling`: core builder image shared by both ISL and VLG images
-	`isl`: integrated service layer - system and platform services
-	`vlg`: virtual load generator - load generation agent

GNU Make is used for build automation, with a `Makefile` in each image's build location.  Changing directory to each location and typing `make` will utilize standard docker CLI commands to build respective images:
-	The `starling` (core) image is built from the repository root, and must
	be built first, as the other images depend on it
-	The `isl` image is built from the `isl` subdirectory
-	The `vlg` image is built from the `vlg` subdirectory

